<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',function(){
  return view('welcome.blade.php');
});


Route::name('install')->get('install', 'ShopifyController@install_app');
Route::name('app')->get('app', 'ShopifyController@app');
Route::get('shopifycallback', 'ShopifyController@shopify_callback')->name('shopifycallback');
Route::get('offer_listing','ShopifyController@offer_listing')->name('offer_listing');
Route::get('create_offers',['uses'=>'HomesController@index','as'=>'create_offers']);
Route::get('offers','OffersController@index');
Route::get('settings','SettingsController@index')->name('settings');
Route::post('save_settings','SettingsController@store');
Route::post('store','OffersController@store');
Route::get('edit/{id}','OffersController@edit');
Route::post('update/{id}','OffersController@update');
Route::post('discount','OffersController@discountCheck');
Route::post('deleteOffer/', ['uses' => 'OffersController@delete', 'as' => 'deleteOffer']);

Route::post('discountTable','OffersController@discountTable');
Route::any('create_discount_code','OffersController@create_discount_code');


Route::get('liquid_snippet','OffersController@index');

Route::get('get_shopify_products','OffersController@shopify_products')->name('shopify_products');


Route::get('get_shopify_collections','OffersController@collections')->name('collections');

