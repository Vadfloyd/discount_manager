<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('shop_domain');
            $table->bigInteger('shop_id');
            $table->string('success_msg_color')->nullable();
            $table->string('offer_bg_color')->nullable();
            $table->string('offer_text_color')->nullable();
            $table->longText('product_page_msg')->nullable();
            $table->longText('discount_off_text')->nullable();
            $table->longText('discount_text')->nullable();
            $table->longText('currency_format')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
