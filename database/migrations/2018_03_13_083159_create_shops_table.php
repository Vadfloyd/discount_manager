<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shops', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('shop_id');
            $table->string('name');
            $table->string('email');
            $table->string('domain');
            $table->string('country');
            $table->mediumText('address');
            $table->string('zip');
            $table->string('city');
            $table->string('phone');
            $table->string('currency');
            $table->string('shop_owner');
            $table->string('access_token');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shops');
    }
}
