@extends('layouts.master')
@section('main_screen')

    <div class="page-wrapper">

        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container" style="margin-top: 0;">
            <!-- BEGIN SIDEBAR -->

            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <!-- BEGIN THEME PANEL -->

                    <!-- END THEME PANEL -->
                    <!-- BEGIN PAGE BAR -->
                    <div class="page-bar" style="margin-bottom: 30px;">
                        <ul class="page-breadcrumb">
                            <li>
                                <a href="{{route('offer_listing')}}">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>Settings</span>
                            </li>
                        </ul>
                    </div>
                    <!-- END PAGE BAR -->
                    <!-- BEGIN PAGE TITLE-->

                    <!-- END PAGE TITLE-->
                    <!-- END PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-4">
                            <h4>Notification Design</h4>
                            {{--<p>Set the tiers for your quantity breaks</p>--}}
                        </div>
                        <form class="col-md-8" action="{{ action('SettingsController@store') }}" method="POST">
                            <!-- BEGIN PORTLET-->
                            <div class="col-md-12" style="margin-top: 20px;">
                                <div class="form-group">
                                    <label for="multi-append" class="control-label">
                                        Discount Message Color
                                    </label>

                                    <div class="col-sm-12" style="padding: 0 5px;">
                                        <input style="width:12%;" value="{{ isset($shop_settings->success_msg_color)?$shop_settings->success_msg_color:"" }}" type="color" class="form-control" name="success_msg_color" />

                                    </div>
                                </div>
                            </div>
                            <div class="portlet light bordered">
                                <div class="field_wrapper">

                                </div>

                                <div class="form-group">
                                    <label class="control-label">Offer Text Color</label>
                                    <input style="width:12%;" value="{{ isset($shop_settings->offer_text_color)?$shop_settings->offer_text_color:"" }}" type="color" class="form-control" name="offer_text_color" />
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Background Color</label>
                                    <input style="width:12%;" value="{{ isset($shop_settings->offer_bg_color)?$shop_settings->offer_bg_color:"" }}" type="color" class="form-control" name="offer_bg_color" />
                                    <span class="help-block"> </span>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Product Page Message</label>
                                    <input style="width: 70%"  value="{{ isset($shop_settings->product_page_msg)?$shop_settings->product_page_msg:"" }}" type="text" class="form-control" name="product_page_msg" />

                                </div>
                                <div class="form-group">
                                    <label class="control-label">Currency Format</label>
                                    <input style="width: 70%"  value="{{ isset($shop_settings->currency_format)?$shop_settings->currency_format: "$ {{".'amount'.'}'.'}'  }}" type="text" class="form-control" name="currency_format" />

                                </div>
                                <div class="form-group">
                                    {{--<label>Group quantities by</label>--}}
                                    <div class="mt-radio-list">

                                        <div class="col-md-4">
                                            <button type="submit" class="btn btn-primary">{{ isset($shop_settings) && !empty($shop_settings)?'Update':'Save' }}</button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
        <!-- BEGIN QUICK SIDEBAR -->


    </div>
    <!-- END CONTAINER -->
    <!-- BEGIN FOOTER -->
    <div class="page-footer">
        <div class="page-footer-inner"> 2018 &copy; Bazingoinc
            <a target="_blank" href="http://bazingoinc.com/">Bazingoinc</a>
        </div>

    </div>
    <!-- END FOOTER -->
    </div>

@endsection