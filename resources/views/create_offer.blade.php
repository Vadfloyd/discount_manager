@extends('layouts.master')
@section('main_screen')

    <div class="page-wrapper">

        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"></div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container" style="margin-top: 0;">
            <!-- BEGIN SIDEBAR -->

            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <!-- BEGIN THEME PANEL -->

                    <!-- END THEME PANEL -->
                    <!-- BEGIN PAGE BAR -->
                    <div class="page-bar" style="margin-bottom: 30px;">
                        <ul class="page-breadcrumb">
                            <li>
                                <a href="{{action('ShopifyController@offer_listing')}}">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>Create Offer</span>
                            </li>
                        </ul>
                    </div>
                    <!-- END PAGE BAR -->
                    <!-- BEGIN PAGE TITLE-->

                    <!-- END PAGE TITLE-->
                    <!-- END PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-4">
                            <h4>Offer Setup</h4>
                            <p>Set the tiers for your quantity breaks</p>
                        </div>
                        <form id="createOfferForm" class="col-md-8" action="{{ action('OffersController@store') }}" method="POST">
                            <!-- BEGIN PORTLET-->
                            <div class="col-md-12" style="margin-top: 20px;">
                                <div class="form-group">
                                    <label for="multi-append" class="control-label">Select discount type</label>
                                    <div class="col-sm12" style="padding: 0 5px;">
                                        <select id="discount_type" class="form-control" name="discount_type">
                                            <option value="amount">Amount</option>
                                            <option value="quantity">Quantity</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="portlet light bordered">
                                <div class="field_wrapper">
                                    <div class="row">

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input class="form-control currency change_currency" type="text" name="qtyOrAmount[]"
                                                       onkeypress="javascript:return isNumber(event)"
                                                       value="" placeholder="Amt/Qty" required/>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <div class="col-sm-6">
                                                    <input onkeypress="javascript:return isNumber(event)"
                                                           class="form-control currency" type="text" name="discount[]" value=""
                                                           placeholder="Dscount" required/>
                                                    <input class="form-control" type="hidden" name="store_currency"
                                                           value="{{$currency}}"
                                                           placeholder=""/>
                                                </div>
                                                <div class="col-sm-6" style="padding: 0 0 0 15px;">
                                                    <select class="form-control unit" name="unit[]">
                                                        <option value="percentage">% of each</option>
                                                        <option value="fixed_amount">{{$currency}} of each</option>
                                                    </select>
                                                    <div id="error" style="color: red;"></div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12" style="margin-bottom: 15px;">
                                        <a href="javascript:void(0);"
                                           class="add_button btn btn-default dropdown-toggle" title="Add field"><i
                                                    class="fa fa-plus"></i>Add another tier</a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="form-group">
                                        <label for="multi-append" class="control-label">Select products that will have
                                            these breaks</label>
                                        <div class="input-group select2-bootstrap-append">
                                            <select data-placeholder="Select Products" id="multi-append"
                                                    class="form-control select2 products-field-input" multiple
                                                    name="products[]" >
                                                {{--@foreach ($products as $product)--}}
                                                    {{--<option value="{{$product->id}}">{{$product->title}}</option>--}}
                                                {{--@endforeach--}}
                                            </select>
                                            <span class="input-group-btn">
                                                    <button class="btn btn-default" type="button"
                                                            data-select2-open="multi-append">
                                                        <span class="glyphicon glyphicon-search"></span>
                                                    </button>
                                                </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="form-group">
                                        <label for="multi-append" class="control-label">Select Collections that will
                                            have these breaks</label>
                                        <div class="input-group select2-bootstrap-append">
                                            <select data-placeholder="Select Collections" id="multi-append"
                                                    class="collections-field-input form-control select2" multiple
                                                    name="collections[]" >
                                                @foreach ($collections as $collection)
                                                    <option value="{{$collection->id}}">{{$collection->title}}</option>
                                                @endforeach
                                            </select>
                                            <span class="input-group-btn">
                                                    <button class="btn btn-default" type="button"
                                                            data-select2-open="multi-append">
                                                        <span class="glyphicon glyphicon-search"></span>
                                                    </button>
                                                </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Success Msg</label>
                                    <input class="form-control" placeholder="You have successfully save {100}"
                                           type="text" name="success_msg" required=""
                                           value="You have successfully save @{{discount}}">
                                    <span class="help-block"> Example: Congrats you have save in bulk. </span>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Upsell Msg</label>
                                    <input class="form-control" placeholder="Buy {5} to get {1500} off" type="text"
                                           name="upsell_msg" required=""
                                           value="Buy @{{minimum}} to get @{{discount}} off">
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Offer Name(Admin purpose only)</label>
                                    <input class="form-control" placeholder="Enter text" type="text" name="offer_name"
                                           required="">
                                    <span class="help-block"> </span>
                                </div>
                                <div class="form-group">

                                    <div class="mt-radio-list">

                                        <div class="col-md-4">
                                            <button type="submit" class="btn btn-primary">Create</button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
        <!-- BEGIN QUICK SIDEBAR -->


    </div>
    <!-- END CONTAINER -->
    <!-- BEGIN FOOTER -->
    <div class="page-footer">
        <div class="page-footer-inner"> 2018 &copy; Bazingoinc
            <a target="_blank" href="http://bazingoinc.com/">Bazingoinc</a>
        </div>

    </div>
    <!-- END FOOTER -->
    </div>
    <script type="text/javascript">
        $(document).ready(function () {

            $(document).on('change','#discount_type',function(){

                if($(this).val()=='quantity'){
                    $(".change_currency").maskMoney('destroy');
                    $('.change_currency').removeClass('currency');
                    $('.currency').maskMoney();
                }else{
                    $('.change_currency').addClass('currency');
                    $('.currency').maskMoney();
                }
            });

            $(function() {
                $('.currency').maskMoney();
            })



            $('.products-field-input').select2({
                ajax: {
                    url: "{{ route('shopify_products') }}",
                    minimumInputLength: 3,
                    data: function (params) {
                        var query = {
                            search: params.term,
                            page: params.page || 1
                        }

                        // Query parameters will be ?search=[term]&page=[page]
                        return query;
                    }
                }
            });






            $("#createOfferForm").submit(function (e) {
                e.preventDefault();
                var products_val = $('.products-field-input').val();
                var collection_val = $('.collections-field-input').val();
                if (products_val ==null && collection_val==null ) {
                    ShopifyApp.flashNotice('Please select at least one product or offer');
                }else{
                    $('#createOfferForm')[0].submit();
                   // $("form").submit();
                }
            });

            var maxField = 10;
            var addButton = $('.add_button');
            var wrapper = $('.field_wrapper');

            var x = 1;
            $(addButton).click(function () {

                var fieldHTML = '<div class="row form-group repo' + x + '"><div class="col-md-3"><input type="text" required class="qtyOrAmount form-control currency change_currency" onkeypress="javascript:return isNumber(event)" name="qtyOrAmount[]" value="" placeholder="Amt/Qty"/></div><div class="col-md-5"><div class="col-sm-6"><input type="text" class="currency form-control" onkeypress="javascript:return isNumber(event)" required name="discount[]" value="" placeholder="Dscount"/></div><div class="col-sm-6"   style="padding: 0 0 0 15px;"><select class="form-control unit" name="unit[]"><option value="percentage">% of each</option><option value="fixed_amount">{{$currency}} of each</option></select></div></div><a href="javascript:void(0);" class="remove_button" title="Remove field" onclick="return removeDiv(' + x + ')">Remove</a></div></div>'; //New input field html
                if (x < maxField) {
                    x++;
                    $(wrapper).append(fieldHTML);
                    if($('#discount_type').val()=='quantity'){
                        $(".change_currency").maskMoney('destroy');
                        $('.change_currency').removeClass('currency');
                        $('.currency').maskMoney();
                    }else{
                        $('.change_currency').addClass('currency');
                        $('.currency').maskMoney();
                    }
                } else {
                    ShopifyApp.flashNotice('You can only add 10 tiers in one offer');
                }

            });

        });

        function removeDiv(id) {
            $('div.repo' + id).remove();

        }


    </script>

@endsection