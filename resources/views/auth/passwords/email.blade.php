@extends('layouts.master')

@section('content')
    <div class="user-login-5">
        <div class="login-container">
            <div class="login-content" style="display: table; width: 100%;text-align: center;min-height: 100vh;">
                <div class="inner-hlder">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2">
                                <div class="panel panel-default">
                                    <div class="panel-body midl-hlder">
                                        <h1>Reset Password</h1>
                                        @if (session('status'))
                                            {{--<div class="alert alert-success">--}}
                                                <div class="successmsg" role="alert">
                                                {{ session('status') }}
                                            </div>
                                        @endif

                                        <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                                            {{ csrf_field() }}

                                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                <label for="email" class="col-md-12 control-label">E-Mail Address</label>

                                                <div class="col-md-12">
                                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                                    @if ($errors->has('email'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('email') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <button type="submit" class="btn red">
                                                        Submit
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
