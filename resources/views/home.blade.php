@extends('layouts.master')
@section('main_screen')
    <div class="page-wrapper">

        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container" style="margin-top: 0;">
            <!-- BEGIN SIDEBAR -->

            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <!-- BEGIN THEME PANEL -->

                    <!-- END THEME PANEL -->
                    <!-- BEGIN PAGE BAR -->
                    <div class="page-bar" style="margin-bottom: 30px;">
                        <ul class="page-breadcrumb">
                            <li>
                                <span>Home</span>
                            </li>

                        </ul>

                    </div>
                    <!-- END PAGE BAR -->
                    <!-- BEGIN PAGE TITLE-->

                    <!-- END PAGE TITLE-->
                    <!-- END PAGE HEADER-->
                    <div class="row">
                        <div class="vardin_main">
                            <div class="row">
                                <div class="col-md-4">
                                    <h3>Manage Offers</h3>
                                    <a type="button" class="btn btn-primary" href="{{ route('create_offers') }}">Create Offer</a>
                                </div>
                                <div class="col-md-8">
                                @if($offers)
                                        <?php $a = 1; ?>
                                    @foreach($offers as $offer)
                                    <div class="portlet light bordered row{{$a}}" style="overflow: hidden;">
                                        <div class="portlet-title">

                                        </div>
                                        <div class="portlet-body">
                                            <div class="bootstrap-table">
                                                <div class="fixed-table-toolbar">
                                                    <div class="heading pull-left">
                                                        <h4>Quantity Breaks</h4>
                                                        <p>{{$offer->offer_name}}</p>
                                                    </div>
                                                    <div class="columns columns-right btn-group pull-right"><a class="btn btn-default" type="button"  title="Edit Offer" href="{{url('/edit/'.$offer->id)}}">Edit</a>
                                                        {{--<button class="btn btn-default" type="button" name="toggle" title="Toggle">Test Setup</button>--}}
                                                        <a class="btn btn-default" type="button"  title="Delete Offer" onclick="return deleteOffers({{$offer->id}} ,{{$a}})"><i class="fa fa-trash-o"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                                <?php $a++; ?>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                            {{ $offers->links() }}
                        </div>
                    </div>
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
    </div>
    <!-- END CONTAINER -->
    <!-- BEGIN FOOTER -->
    <div class="page-footer">
        <div class="page-footer-inner"> 2018 &copy; Bazingoinc
            <a target="_blank" href="http://bazingoinc.com/">Bazingoinc</a>
        </div>

    </div>
    <!-- END FOOTER -->
    </div>

    <script type="text/javascript">
        function deleteOffers($id,$row)
        {
           // alert($row);
            swal({
                title: "Are you sure to want to delete Offer?",
                text: "Offer will delete permanently",
                icon: "danger",
                buttons: true,
                dangerMode: true,
            }).then((value) => {
                    if (value) {
                        $.ajax({
                            url: "{{ route('deleteOffer') }}",
                            type: "POST",
                            dataType: "JSON",
                            data: {
                                'offer_id': $id
                            },
                            success: function (result) {

                                if (result) {
                                    swal(result.status, result.message, 'success');
                                    $(".row"+$row).remove();
                                }
                            }, error: function (response) {
                                swal('Warning', 'Failed to delete offer. Try again !!', 'warning');
                            }
                        });

                    } else {
                    }
                }
            );
        }
    </script>
@endsection
