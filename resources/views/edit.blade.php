@extends('layouts.master')
@section('main_screen')

    <div class="page-wrapper">

        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"></div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container" style="margin-top: 0;">
            <!-- BEGIN SIDEBAR -->

            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <!-- BEGIN THEME PANEL -->

                    <!-- END THEME PANEL -->
                    <!-- BEGIN PAGE BAR -->
                    <div class="page-bar" style="margin-bottom: 30px;">
                        <ul class="page-breadcrumb">
                            <li>
                                <a href="{{route('offer_listing')}}">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>Edit Offer</span>
                            </li>
                        </ul>
                    </div>
                    <!-- END PAGE BAR -->
                    <!-- BEGIN PAGE TITLE-->

                    <!-- END PAGE TITLE-->
                    <!-- END PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-4">
                            <h4>Offer Setup</h4>
                            <p>Set the tiers for your quantity breaks</p>
                        </div>

                        <form id="updateOfferForm" class="col-md-8" action="{{ action('OffersController@update',$id) }}" method="POST">
                            <!-- BEGIN PORTLET-->
                            <div class="col-md-12" style="margin-top: 20px;">
                                <div class="form-group">
                                    <label for="multi-append" class="control-label">Select discount type</label>

                                    <div class="col-sm12" style="padding: 0 5px;">
                                        <select id="discount_type" class="form-control" name="discount_type">
                                            <option value="quantity" @if($offer['discount_type']=='quantity'){{'selected'}}@endif>
                                                Quantity
                                            </option>
                                            <option value="amount" @if($offer['discount_type']=='amount'){{'selected'}}@endif>
                                                Amount
                                            </option>
                                        </select>
                                        <input class="form-control" type="hidden" name="store_currency"
                                               value="{{$currency}}"
                                               placeholder=""/>
                                    </div>
                                </div>
                            </div>
                            <div class="portlet light bordered">
                                <div class="field_wrapper">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input class="currency change_currency  form-control" type="text" name="qtyOrAmount[]" onkeypress="javascript:return isNumber(event)"
                                                       value="{{ isset($discounts[0]) ?$discounts[0]['amount_or_quantity']:''}}"
                                                       placeholder="Amount/Quantity" required/>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <div class="col-sm-6">
                                                    <input class="currency form-control" type="text" name="discount[]"  onkeypress="javascript:return isNumber(event)"
                                                           value="{{isset($discounts[0]) ?$discounts[0]['discount']:''}}" placeholder="Dscount"
                                                           required/>
                                                </div>
                                                <div class="col-sm-6" style="padding: 0 0 0 15px;">
                                                    <select class="form-control" name="unit[]">
                                                        <option value="percentage" {{ isset($discounts[0]) && $discounts[0]['discount_by']=='percentage'?'selected':''}}>
                                                            % of each
                                                        </option>
                                                        <option value="fixed_amount" {{ isset($discounts[0]) && $discounts[0]['discount_by']=='fixed_amount'?'selected':''}}>{{$currency}}
                                                            of each
                                                        </option>
                                                    </select></div>
                                            </div>
                                        </div>

                                    </div>
                                    <div id="field" data-field-id="{{$discounts->count()}}"></div>
                                    @if($discounts->count() > 1 )
                                        @for ($i = 1; $i < $discounts->count(); $i++)
                                            <div class="row form-group repo{{$i+1}}">

                                                <div class="col-md-3">
                                                    <input onkeypress="javascript:return isNumber(event)" type="text" class="currency change_currency form-control" name="qtyOrAmount[]"
                                                           value="{{$discounts[$i]['amount_or_quantity']}}"
                                                           required placeholder="Amount/Quantity"/>
                                                </div>
                                                <div class="col-md-5">
                                                    <div class="col-sm-6">
                                                        <input type="text" class="currency  form-control" onkeypress="javascript:return isNumber(event)" name="discount[]"
                                                               value="{{$discounts[$i]['discount']}}"
                                                               required  placeholder="Dscount"/>
                                                    </div>
                                                    <div class="col-sm-6" style="padding: 0 0 0 15px;">
                                                        <select class="form-control" name="unit[]">
                                                            <option value="percentage" {{$discounts[$i]['discount_by']=='percentage'?'selected':''}}>
                                                                % of each
                                                            </option>
                                                            <option value="fixed_amount" {{$discounts[$i]['discount_by']=='fixed_amount'?'selected':''}}>{{$currency}}
                                                                of each
                                                            </option>
                                                        </select>
                                                    </div>

                                                </div>
                                                <a href="javascript:void(0);" class="remove_button"
                                                   title="Remove field"
                                                   onclick="return removeDiv({{$i+1}})">Remove</a>
                                            </div>
                                        @endfor
                                    @endif
                                </div>
                                <div class="row">
                                    <div class="col-md-12" style="margin-bottom: 15px;">
                                        <a href="javascript:void(0);"
                                           class="add_button btn btn-default dropdown-toggle" title="Add field"><i
                                                    class="fa fa-plus"></i>Add another tier</a>
                                    </div>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="form-group">
                                    <label for="multi-append" class="control-label">Select products that will have these
                                        breaks</label>
                                    <div class="input-group select2-bootstrap-append">
                                        <select data-placeholder="Select Products" id="multi-append" class="products-field-input form-control select2" multiple
                                                name="products[]" >
                                            @if(sizeof($products)>0)
                                            @foreach ($products as $product)
                                                <option selected value="{{$product->id}}"
                                               >{{$product->title}}</option>
                                            @endforeach
                                                @endif
                                        </select>
                                        <span class="input-group-btn">
                                                    <button class="btn btn-default" type="button"
                                                            data-select2-open="multi-append">
                                                        <span class="glyphicon glyphicon-search"></span>
                                                    </button>
                                                </span>
                                    </div>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="form-group">
                                    <label for="multi-append" class="control-label">Select Collections that will have
                                        these breaks</label>
                                    <div class="input-group select2-bootstrap-append">
                                        <select data-placeholder="Select Collections" id="multi-append" class="collections-field-input form-control select2" multiple
                                                name="collections[]" >
                                            @foreach ($collections as $collection)
                                                <option value="{{$collection->id}}" @for($i = 0; $i < $offer_items->count(); $i++) @if(!empty($offer_items[$i]['collection_id'])) @if($offer_items[$i]['collection_id']==$collection->id) {{'selected'}} @endif @endif @endfor>{{$collection->title}}</option>
                                            @endforeach
                                        </select>
                                        <span class="input-group-btn">
                                                    <button class="btn btn-default" type="button"
                                                            data-select2-open="multi-append">
                                                        <span class="glyphicon glyphicon-search"></span>
                                                    </button>
                                                </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Success Msg</label>
                                <input class="form-control" placeholder="You have successfully save {100}" type="text"
                                       name="success_msg" required="" value="{{$offer['success_msg']}}">
                                <span class="help-block"> Example: Congrats you have save in bulk. </span>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Upsell Msg</label>
                                <input class="form-control" placeholder="Buy {5} to get {1500} off" type="text"
                                       name="upsell_msg" required="" value="{{$offer['upsell_msg']}}">
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Offer Name(Admin purpose only)</label>
                                <input class="form-control" placeholder="Enter text" type="text" name="offer_name"
                                       required="" value="{{$offer['offer_name']}}">
                                <span class="help-block"> </span>
                            </div>
                            <div class="form-group">
                                {{--<label>Group quantities by</label>--}}
                                <div class="mt-radio-list">

                                    <div class="col-md-4">
                                        <button type="submit" class="btn btn-primary">Update</button>
                                    </div>
                                </div>

                            </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
    <!-- BEGIN QUICK SIDEBAR -->


    </div>
    <!-- END CONTAINER -->
    <!-- BEGIN FOOTER -->
    <div class="page-footer">
        <div class="page-footer-inner"> 2018 &copy; Bazingoinc
            <a target="_blank" href="http://bazingoinc.com/">Bazingoinc</a>
        </div>

    </div>
    <!-- END FOOTER -->
    </div>

    <script type="text/javascript">
        $(document).ready(function () {

            $(document).on('change','#discount_type',function(){

                if($(this).val()=='quantity'){
                    $(".change_currency").maskMoney('destroy');
                    $('.change_currency').removeClass('currency');
                    $('.currency').maskMoney();
                }else{
                    $('.change_currency').addClass('currency');
                    $('.currency').maskMoney();
                }
            });

            $(function() {
                $('.currency').maskMoney();
            })


            $('.products-field-input').select2({
                ajax: {
                    url: "{{ route('shopify_products') }}",
                    minimumInputLength: 3,
                    data: function (params) {
                        var query = {
                            search: params.term,
                            page: params.page || 1
                        }

                        // Query parameters will be ?search=[term]&page=[page]
                        return query;
                    }
                }
            });


            $("#updateOfferForm").submit(function (e) {
                e.preventDefault();
                var products_val = $('.products-field-input').val();
                var collection_val = $('.collections-field-input').val();
                if (products_val ==null && collection_val==null ) {
                    ShopifyApp.flashNotice('Please select at least one product or offer');
                }else{
                    $('#updateOfferForm')[0].submit();
                    // $("form").submit();
                }
            });


            var maxField = 10;
            var addButton = $('.add_button');
            var wrapper = $('.field_wrapper');
            var x = $('#field').data("field-id");
            $(addButton).click(function () {
                var fieldHTML = '<div class="row form-group repo' + x + '"><div class="col-md-3"><input type="text" class="qtyOrAmount currency  change_currency form-control" required name="qtyOrAmount[]" onkeypress="javascript:return isNumber(event)" placeholder="Amt/Qty"/></div><div class="col-md-5"><div class="col-sm-6"><input type="text" class="currency form-control" required name="discount[]" onkeypress="javascript:return isNumber(event)" placeholder="Dscount"/></div><div class="col-sm-6"   style="padding: 0 0 0 15px;"><select class="form-control unit" name="unit[]"><option value="percentage">% of each</option><option value="fixed_amount">{{$currency}} of each</option></select></div></div><a href="javascript:void(0);" class="remove_button" title="Remove field" onclick="return removeDiv(' + x + ')">Remove</a></div></div>'; //New input field html
                if (x < maxField) {
                    x++;
                    $(wrapper).append(fieldHTML);
                    if($('#discount_type').val()=='quantity'){
                        $(".change_currency").maskMoney('destroy');
                        $('.change_currency').removeClass('currency');
                        $('.currency').maskMoney();
                    }else{
                        $('.change_currency').addClass('currency');
                        $('.currency').maskMoney();
                    }
                }else{
                    ShopifyApp.flashNotice( 'You can only add 10 tiers in one offer');
                }

            });



                if($('#discount_type').val()=='quantity'){
                    $(".change_currency").maskMoney('destroy');
                    $('.change_currency').removeClass('currency');
                    $('.currency').maskMoney();
                }else{
                    $('.change_currency').addClass('currency');
                    $('.currency').maskMoney();
                }



        });
        function removeDiv(id) {
            $('div.repo' + id).remove();

        }
    </script>

@endsection