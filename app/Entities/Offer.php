<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
class Offer extends Model
{
    public function shop()
    {
        return $this->belongsTo('App\Entities\Shop');
    }

    public function offer_items()
    {
        return $this->hasMany('App\Entities\offerItem');
    }
    public function discount()
    {
        return $this->hasMany('App\Entities\Discount');
    }
}