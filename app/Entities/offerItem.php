<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class OfferItem extends Model
{
    public function offer()
    {
        return $this->belongsTo('App\Entities\Offer');
    }

}
