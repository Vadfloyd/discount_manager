<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    protected $fillable = ['shop_id', 'domain', 'access_token'];
    public function offer()
    {
        return $this->hasMany('App\Entities\Offer');
    }
}
