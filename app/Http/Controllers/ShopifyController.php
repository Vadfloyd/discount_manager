<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use Session;
use Illuminate\Support\Facades\Input;
use Oseintow\Shopify\Facades\Shopify;
use App\Services\ShopifyService;
use App\Entities\Shop;
use App\Entities\User;
use Illuminate\Support\Facades\Hash;
use App\Entities\Offer;
use URL;

class ShopifyController extends Controller
{
    private $shopify_service;

    public function __construct(ShopifyService $shopify_service)
    {
        $this->shopify_service = $shopify_service;

    }

    public function install_app(Request $request)
    {

        $shopUrl = Input::get('shop');
        if ($shopUrl) {
            $scope = array('write_products', 'read_product_listings','read_products', 'write_price_rules,write_themes');
            $redirectUrl = env('SHOPIFY_REDIRECT_URL');
            $shopify = Shopify::setShopUrl($shopUrl);
            return redirect()->to($shopify->getAuthorizeUrl($scope, $redirectUrl));
        } else {
            echo "missing parameter Shop Domain";
        }

    }


    public function shopify_callback(Request $request)
    {
        $shopUrl = input::get('shop');
        if (!$shopUrl) {
            if (Session::has('shop')) {
                $shop = session('shop');
                $shopUrl = $shop['domain'];
            } else {
                return redirect('/login');
            }
        }

        if (isset($request->code)) {
            $accessToken = Shopify::setShopUrl($shopUrl)->getAccessToken($request->code);
        } elseif (Session::has('shopify_token')) {

            $accessToken = session('shopify_token');
        } else {
            $accessToken = Shop::where('domain', $shopUrl)->first();
            if ($accessToken) {
                $accessToken = $accessToken->access_token;
            }
        }
        $shop_data = Shopify::setShopUrl($shopUrl)->setAccessToken($accessToken)->get("admin/shop.json");
        session()->put('shopify_token', $accessToken);
        if ($shop_data) {
            session()->put('shop', $shop_data);
            $this->shop_data($shopUrl, $shop_data, $accessToken);
            $this->add_snippet();

        }

        return redirect()->route('offer_listing');

    }

    public function offer_listing()
    {

        $shop = session('shop');
        if ($shop) {
            $offers = Offer::where('shop_domain', $shop['domain'])
                ->where('shop_id', session('shop_id'))
                ->orderBy('id', 'DESC')->paginate(5);

            return view('home', compact('shop', 'offers'));
        }


    }


    public function app(Request $request)
    {
        return view('index');
    }

    private function shop_data($domain, $shop_data, $access_token)
    {
        $shop = Shop::firstOrNew(array('domain' => $domain));
        $shop->shop_id = $shop_data['id'];
        $shop->name = $shop_data['name'];
        $shop->email = $shop_data['email'];
        $shop->domain = $shop_data['domain'];
        $shop->country = $shop_data['country'];
        $shop->address = $shop_data['address1'];
        $shop->zip = $shop_data['zip'];
        $shop->city = $shop_data['city'];
        $shop->phone = $shop_data['phone'];
        $shop->currency = $shop_data['currency'];
        $shop->shop_owner = $shop_data['shop_owner'];
        $shop->access_token = $access_token;
        $shop->save();
        session()->put('shop_id', $shop->id);

        // verifying user

        $shop_user = User::where('email', $shop_data['email'])->first();
        if (!$shop_user) {
            $user = new User();
            $user->email = $shop_data['email'];
            $user->password = Hash::make('Bazingo');
            $user->name = 'BazingoInc';
            $user->save();
        }

    }

    private function add_snippet()
    {
        $shop_session = session('shop');
        if ($shop_session) {
            $shop = Shop::where('domain', $shop_session['domain'])->first();
            $money_format = $shop_session['money_format'];
            // Getting theme Id
            $theme_details = Shopify::setShopUrl($shop->domain)->setAccessToken($shop->access_token)->get("admin/themes.json");

            if (!is_null($theme_details)) {
                $theme_id = 0;
                foreach ($theme_details as $theme_detail):
                    if ($theme_detail->role == 'main') {
                        $theme_id = $theme_detail->id;
                        break;
                    }
                endforeach;
                if ($theme_id) {
                    $theme_id = str_replace('.0', '', $theme_id);
                    $theme_liquid_file = Shopify::setShopUrl($shop->domain)->setAccessToken($shop->access_token)->get("/admin/themes/" . $theme_id . "/assets.json", ['theme_id' => $theme_id, 'asset[key]' => 'layout/theme.liquid']);
                    if (isset($theme_liquid_file['value'])) {
                        $theme_liquid_file_value = $theme_liquid_file['value'];
                        if (strpos($theme_liquid_file_value, "{% include 'bazingo-quantity-breaks' %}") === false) {
                            $theme_liquid_file_value = str_replace('</body>', "{% include 'bazingo-quantity-breaks' %}</body>", $theme_liquid_file_value);
                            $data = ["asset" => ["key" => "layout/theme.liquid", "value" => $theme_liquid_file_value]];
                            try {
                                $update_theme_layout_liquid = Shopify::setShopUrl($shop->domain)->setAccessToken($shop->access_token)->put("/admin/themes/" . $theme_id . "/assets.json", $data);
                            } catch (\Exception $e) {
                            }
                        }
                    }

                    $data = ["asset" => ["key" => "snippets/bazingo-quantity-breaks.liquid", "src" => URL::to('/') . '/js/liquid_snippet.js']];
                    try {
                        $snippet = Shopify::setShopUrl($shop->domain)->setAccessToken($shop->access_token)->put("/admin/themes/" . $theme_id . "/assets.json", $data);
                    } catch (\Exception $e) {
                    }
                    // updating cart liquid template

                    $theme_cart_liquid_file = Shopify::setShopUrl($shop->domain)->setAccessToken($shop->access_token)->get("/admin/themes/" . $theme_id . "/assets.json", ['theme_id' => $theme_id, 'asset[key]' => 'templates/cart.liquid']);
                    if (isset($theme_cart_liquid_file['value'])) {
                        $theme_cart_liquid_file_value = $theme_cart_liquid_file['value'];
                        if (strpos($theme_cart_liquid_file_value, "{{ item.product.title }}") === false && strpos($theme_cart_liquid_file_value, "{{ item.line_price | money }}") === false) {
                            $theme_cart_template_liquid_file = Shopify::setShopUrl($shop->domain)->setAccessToken($shop->access_token)->get("/admin/themes/" . $theme_id . "/assets.json", ['theme_id' => $theme_id, 'asset[key]' => 'sections/cart-template.liquid']);

                            if (isset($theme_cart_template_liquid_file['value'])) {
                                $theme_cart_template_liquid_file = $theme_cart_template_liquid_file['value'];

                                if (strpos($theme_cart_template_liquid_file, "{{ item.product.title }}") != false && strpos($theme_cart_template_liquid_file, "{{ item.line_price | money }}") !== false) {
                                    if (strpos($theme_cart_template_liquid_file, "bazingo-cart-item-success-notes") == false) {
                                        $theme_cart_template_liquid_file = str_replace('{{ item.product.title }}', "{{ item.product.title }}" . "<span class='bazingo-cart-item-success-notes' data-key='{{item.key}}'></span><span class='bazingo-cart-item-upsell-notes' data-key='{{item.key}}'></span>", $theme_cart_template_liquid_file);
                                        $theme_cart_template_liquid_file = str_replace('{{ item.line_price | money }}',  "<span class='bazingo-cart-item-line-price' data-key='{{item.key}}'>{{ item.line_price | money }}</span>", $theme_cart_template_liquid_file);
                                        $theme_cart_template_liquid_file = str_replace('{{ cart.total_price | money }}',  "<span class='bazingo-original-cart-total'>{{ cart.total_price | money }}</span><span class='bazingo-cart-total'></span>", $theme_cart_template_liquid_file);
                                        $data = ["asset" => ["key" => "sections/cart-template.liquid", "value" => $theme_cart_template_liquid_file]];
                                        try {
                                            $update_theme_cart_layout_liquid = Shopify::setShopUrl($shop->domain)->setAccessToken($shop->access_token)->put("/admin/themes/" . $theme_id . "/assets.json", $data);
                                        } catch (\Exception $e) {
                                           // dd($e);
                                        }


                                    }
                                }
                            }

                        } else {

                            if (strpos($theme_cart_liquid_file_value, "bazingo-cart-item-success-notes") == false) {
                                $theme_cart_liquid_file_value = str_replace('{{ item.product.title }}', "{{ item.product.title }}" . "<span class='bazingo-cart-item-success-notes' data-key='{{item.key}}'></span><span class='bazingo-cart-item-upsell-notes' data-key='{{item.key}}'></span>", $theme_cart_liquid_file_value);
                                $theme_cart_liquid_file_value = str_replace('{{ item.line_price | money }}',  "<span class='bazingo-cart-item-line-price' data-key='{{item.key}}'>{{ item.line_price   | money }}</span>", $theme_cart_liquid_file_value);
                                $theme_cart_liquid_file_value = str_replace('{{ cart.total_price | money }}',  "<span class='bazingo-original-cart-total'>{{ cart.total_price | money }}</span><span class='bazingo-cart-total'></span>", $theme_cart_liquid_file_value);
                                $data = ["asset" => ["key" => "templates/cart.liquid", "value" => $theme_cart_liquid_file_value]];
                                try {
                                    $update_theme_cart_layout_liquid = Shopify::setShopUrl($shop->domain)->setAccessToken($shop->access_token)->put("/admin/themes/" . $theme_id . "/assets.json", $data);
                                } catch (\Exception $e) {
                                }
                            }

                        }

                    }

                }


            }


        }
    }

}
