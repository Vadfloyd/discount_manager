<?php

namespace App\Http\Controllers;
use Oseintow\Shopify\Facades\Shopify;
use Illuminate\Http\Request;
use App\Entities\Shop;
class HomesController extends Controller
{

    public function index(){
        $shop = session('shop');
        if($shop){
            $shopUrl = $shop['domain'];
            $currency = $shop['currency'];
            $accessToken = Shop::where('domain', $shopUrl)->first();
            $accessToken = $accessToken->access_token;

            $smartCollection = Shopify::setShopUrl($shopUrl)->setAccessToken($accessToken)->get("admin/smart_collections.json");
            $customCollection = Shopify::setShopUrl($shopUrl)->setAccessToken($accessToken)->get("admin/custom_collections.json");
            $all_collections=array_merge(json_decode($customCollection),json_decode($smartCollection));
            return view('create_offer',["currency"=>$currency,"shop"=>$shopUrl,"collections"=>$all_collections]);
        }else{
            return redirect()->route('shopifycallback');
        }

    }
}
