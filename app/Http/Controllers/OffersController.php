<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entities\Offer;
use App\Entities\offerItem;
use App\Entities\Discount;
use App\Entities\Shop;
use Oseintow\Shopify\Facades\Shopify;
use Illuminate\Support\Facades\RedirectResponse;
use Session;
use App\Entities\Setting;

class OffersController extends Controller
{
    public function index()
    {

    }

    public function edit($id)
    {

        $shop = session('shop');
        $product_ids = [];
        $products=[];
        $shopUrl = $shop['domain'];
        $currency = $shop['currency'];
        $accessToken = Shop::where('domain', $shopUrl)->first();
        $accessToken = $accessToken->access_token;

        if ($shop) {
            $singleOffer = Offer::find($id);
            $offer_items = $singleOffer->offer_items;
            $offer_discounts = $singleOffer->discount;
            if ($offer_items && sizeof($offer_items) > 0) {
                foreach ($offer_items as $offer_item):
                    if ($offer_item->product_id !== '0') {
                        $product_ids[] = $offer_item->product_id;
                    }
                endforeach;
            }
            if(sizeof($product_ids) > 0){
                $product_ids=implode(',',$product_ids);
                $products = Shopify::setShopUrl($shopUrl)->setAccessToken($accessToken)->get("admin/products.json", ['fields'=>'id,title','ids' => $product_ids]);
            }

            $smartCollection = Shopify::setShopUrl($shopUrl)->setAccessToken($accessToken)->get("admin/smart_collections.json");
            $customCollection = Shopify::setShopUrl($shopUrl)->setAccessToken($accessToken)->get("admin/custom_collections.json");
            $all_collections = array_merge(json_decode($customCollection), json_decode($smartCollection));
            return view('edit', ["id" => $id, 'offer' => $singleOffer, "offer_items" => $offer_items, "discounts" => $offer_discounts, "currency" => $currency, "shop" => $shopUrl, "collections" => $all_collections, "products" => json_decode($products)]);
        } else {
            return redirect()->route('shopifycallback');
        }
    }


    public function update(Request $request, $id)
    {
        if ($request) {
            $offer = Offer::firstOrNew(array('id' => $id));
            $offer->shop_id = session('shop_id');
            $shop = session('shop');
            if (is_null($shop)) {
                return redirect()->route('shopifycallback');
            }
            $offer->shop_domain = $shop['domain'];
            $offer->offer_name = $request->offer_name;
            $offer->discount_type = $request->discount_type;
            $offer->success_msg = $request->success_msg;
            $offer->upsell_msg = $request->upsell_msg;
            $offer->save();

            OfferItem::where('offer_id', $id)->delete();

            $counter = count($request->products);
            if ($counter != 0) {
                for ($j = 0; $j < $counter; $j++) {
                    $offerItem = new offerItem();
                    $offerItem->offer_id = $offer->id;
                    $offerItem->product_id = $request->products[$j];
                    $offerItem->collection_id = '0';
                    $offerItem->shop_id = $shop['id'];
                    $offerItem->shop_domain = $shop['domain'];
                    $offerItem->save();
                }
            }


            $counter = count($request->collections);
            if ($counter != 0) {
                for ($k = 0; $k < $counter; $k++) {
                    $offerItem = new offerItem();
                    $offerItem->offer_id = $offer->id;
                    $offerItem->product_id = '0';
                    $offerItem->collection_id = $request->collections[$k];
                    $offerItem->shop_id = $shop['id'];
                    $offerItem->shop_domain = $shop['domain'];
                    $offerItem->save();
                }
            }


            Discount::where('offer_id', $id)->delete();
            $count = count($request->discount);
            for ($i = 0; $i < $count; $i++) {
                $Discount = new Discount();
                $Discount->amount_or_quantity = str_replace(',','',$request->qtyOrAmount[$i]);
                $Discount->discount = str_replace(',','',$request->discount[$i]);
                $Discount->offer_id = $offer->id;
                $Discount->currency = $request->store_currency;

                if ($request->discount_type == 'amount') {
                    $Discount->amount_or_quantity_position = $request->amount_position[$i];
                }
                $Discount->discount_by = $request->unit[$i];
                $Discount->save();
            }
            Session::flash('message', 'Offer Updated Successfully..!');
            return redirect()->route('offer_listing');
        }
    }

    public function delete(Request $request)
    {
        $request = $request->all();
        $id = $request['offer_id'];
        // dd($id);
        $bool = Offer::where('id', $id)->delete();
        //OfferItem::where('offer_id', $id)->delete();
        //$bool=Discount::where('offer_id', $id)->delete();
        if ($bool) {
            return response()->json(array('status' => 'success', 'message' => 'Deleted successfully'));
        } else {
            return response()->json(array('status' => 'error', 'message' => 'Unable to delete offer'));
        }
//           $shopUrl = session('shop');
//           $offers=Offer::all()->sortByDesc("id");
//           return view('home',["api_key"=>env('SHOPIFY_APIKEY'),"shop"=>$shopUrl,"offers"=>$offers]);

    }

    public function store(Request $request)
    {

        if ($request) {
            $shop = session('shop');
            $offer = new Offer();
            $offer->shop_id = session('shop_id');
            $offer->shop_domain = $shop['domain'];
            $offer->offer_name = $request->offer_name;
            $offer->discount_type = $request->discount_type;
            $offer->success_msg = $request->success_msg;
            $offer->upsell_msg = $request->upsell_msg;
            $offer->save();


            $counter = count($request->products);
            if ($counter != 0) {
                for ($j = 0; $j < $counter; $j++) {
                    $offerItem = new offerItem();
                    $offerItem->offer_id = $offer->id;
                    $offerItem->product_id = $request->products[$j];
                    $offerItem->collection_id = '0';
                    $offerItem->shop_id = $shop['id'];
                    $offerItem->shop_domain = $shop['domain'];
                    $offerItem->save();
                }
            }

            $counter = count($request->collections);
            if ($counter != 0) {
                for ($k = 0; $k < $counter; $k++) {
                    $offerItem = new offerItem();
                    $offerItem->offer_id = $offer->id;
                    $offerItem->product_id = '0';
                    $offerItem->collection_id = $request->collections[$k];
                    $offerItem->shop_id = $shop['id'];
                    $offerItem->shop_domain = $shop['domain'];
                    $offerItem->save();
                }
            }
            $count = count($request->discount);
            for ($i = 0; $i < $count; $i++) {
                $Discount = new Discount();
                $Discount->amount_or_quantity = str_replace(',','',$request->qtyOrAmount[$i]);
                $Discount->discount = str_replace(',','',$request->discount[$i]);
                $Discount->offer_id = $offer->id;
                $Discount->currency = $request->store_currency;
                if ($request->discount_type == 'amount') {
                    $Discount->amount_or_quantity_position = $request->amount_position[$i];
                }
                $Discount->discount_by = $request->unit[$i];
                $Discount->save();
            }
            Session::flash('message', 'Offer Saved Successfully..!');
            return redirect()->route('offer_listing');

        }

    }


    function create_discount_codes($shopify_token, $shopUrl, $discount_val, $total_order_price)
    {

        if ($discount_val <= 0 || $total_order_price <= 0) {
            return response()->json(array('success' => false));
        }
        if ($shopify_token == "" && $shopUrl) {
            $shopobj = Shop::where('domain', $shopUrl)->first();
            if (is_null($shopobj)) {
                return response()->json(array('success' => false));
            }
            $shopify_token = $shopobj->access_token;
        }
        $discount_code = 'Bazingo_' . rand(10000, 500000);
        $price_rules = [];
        $price_rules['price_rule']['title'] = $discount_code;
        $price_rules['price_rule']['target_selection'] = 'all';
        $price_rules['price_rule']['target_type'] = 'line_item';
        $price_rules['price_rule']['allocation_method'] = 'across';
        $price_rules['price_rule']['value_type'] = 'fixed_amount';
        $price_rules['price_rule']['usage_limit'] = 1;

        $price_rules['price_rule']['value'] = "-" . "$discount_val";
        $price_rules['price_rule']['customer_selection'] = 'all';
        $price_rules['price_rule']['starts_at'] = date("d-m-Y");
        $price_rules['price_rule']['prerequisite_subtotal_range'] = ['greater_than_or_equal_to' => $total_order_price];
        $price_rule = Shopify::setShopUrl($shopUrl)->setAccessToken($shopify_token)->post("admin/price_rules.json", $price_rules);
        if (!is_null($price_rule)) {
            $price_rule_id = $price_rule['id'];

            $create_discount_code = Shopify::setShopUrl($shopUrl)->setAccessToken($shopify_token)->post("admin/price_rules/" . $price_rule_id . "/discount_codes.json", array('discount_code' => array('code' => $discount_code, 'usage_count' => 1)));
            if (!is_null($create_discount_code)) {
                return response()->json(array('success' => true, 'discount_code' => $discount_code));
            } else {
                return response()->json(array('success' => false));
            }

        } else {
            return response()->json(array('success' => false));
        }

    }

    private function product_discount_offer_table($request)
    {
        if (isset($request['product']) && isset($request['shop_domain'])) {
            $shop = $request['shop_domain'];
            $shop_settings['success_msg_color'] = '#cccccc';
            $shop_settings['offer_bg_color'] = '#000000';
            $shop_settings['offer_text_color'] = '#cccccc';
            $shop_settings['product_page_msg'] = 'Buy at discounted price';
            $currency_format = '';
            $saved_shop_settings = Setting::where('shop_domain', $shop)->first();

            if ($saved_shop_settings) {

                $shop_settings['success_msg_color'] = $saved_shop_settings->success_msg_color;
                $shop_settings['offer_bg_color'] = $saved_shop_settings->offer_bg_color;
                $shop_settings['offer_text_color'] = $saved_shop_settings->offer_text_color;
                $shop_settings['product_page_msg'] = is_null($saved_shop_settings->product_page_msg) ? 'Buy at discounted price' : $saved_shop_settings->product_page_msg;
                $currency_format = $saved_shop_settings->currency_format;
            }

            $product_id = $request['product']['id'];
            $product_collections = isset($request['product_collections'])?$request['product_collections']:[];

            $offerItem = OfferItem::where("product_id", $product_id)->where("shop_domain", $shop)->first();
            if ($offerItem) {
                $offer_id = $offerItem->offer_id;
                $offer = Offer::where("id", $offer_id)->first();
                $discount = Discount::where("offer_id", $offer_id)->orderBy('amount_or_quantity', 'ASC')->get();
                if (strpos($currency_format, '{{amount}}') !== false) {
                    foreach ($discount as $dis):
                        if (isset($offer->discount_type) && $offer->discount_type == 'amount')
                            $dis->amount_or_quantity = str_replace("{{amount}}", $dis->amount_or_quantity, $currency_format);
                        if ($dis->discount_by == 'fixed_amount')
                            $dis->discount = str_replace("{{amount}}", $dis->discount, $currency_format);
                        else {
                            $dis->discount = $dis->discount . " % ";
                        }
                    endforeach;
                } else {
                    foreach ($discount as $dis):
                        if ($dis->discount_by == 'percentage')
                            $dis->discount = $dis->discount . " % ";
                    endforeach;
                }
                $table = array('type' => 'product_page_discount', 'color_schema' => $shop_settings, 'discount_type' => $offer->discount_type, 'discounts' => $discount);
                return response()->json($table);
            } else {
                if (is_array($product_collections) && !is_null($product_collections)) {
                    foreach ($product_collections as $product_collection):
                        $offerItem = OfferItem::where("collection_id", $product_collection)
                            ->where("shop_domain", $shop)->first();
                        if ($offerItem) {
                            $offer_id = $offerItem->offer_id;
                            $offer = Offer::where("id", $offer_id)->first();
                            $discount = Discount::where("offer_id", $offer_id)->get();
                            if (strpos($currency_format, '{{amount}}') !== false) {
                                foreach ($discount as $dis):
                                    if (isset($offer->discount_type) && $offer->discount_type == 'amount')
                                        $dis->amount_or_quantity = str_replace("{{amount}}", $dis->amount_or_quantity, $currency_format);
                                    if ($dis->discount_by == 'fixed_amount')
                                        $dis->discount = str_replace("{{amount}}", $dis->discount, $currency_format);
                                    else {
                                        $dis->discount = $dis->discount . " % ";
                                    }
                                endforeach;
                            } else {
                                foreach ($discount as $dis):
                                    if ($dis->discount_by == 'percentage')
                                        $dis->discount = $dis->discount . " % ";
                                endforeach;
                            }
                            $table = array('type' => 'product_page_discount', 'color_schema' => $shop_settings, 'discount_type' => $offer->discount_type, 'discounts' => $discount);
                            return response()->json($table);
                            break;
                        }
                    endforeach;

                }
            }


        }


    }

    function create_discount_code(Request $request)
    {

        $request = $request->all();
        $msg_color = '#000000';
        $end_result = [];
        $items_array = [];
        $discounts = [];
        $discounts['original_total_price'] = 0;
        $discounts['discounted_price'] = 0;
        $discounts['discounted_price_html'] = "";
        $discounts['additional_discount_value'] = "";

        if ($request['ba_page'] == 'product') {
            return $this->product_discount_offer_table($request);
        }

        if ($request['ba_page'] == 'cart' && isset($request['action_type']) && $request['action_type'] == 'checkout') {

            $discount_obj = $request['discounts'];

            if (!is_null($discount_obj) && isset($discount_obj['original_total_price']) && isset($discount_obj['discounted_price']))
                return $this->create_discount_codes("", $request['shop_domain'], ($discount_obj['original_total_price'] - $discount_obj['discounted_price']), $discount_obj['original_total_price']);
        }

        if ($request['ba_page'] == 'cart') {
            $cart_obj = $request['cart'];
            $original_total_price = $cart_obj['original_total_price'];
            $discounts['original_total_price'] = $original_total_price / 100;
            $total_price = $cart_obj['total_price'];
            $total_weight = $cart_obj['total_weight'];
            $item_count = $cart_obj['item_count'];
            $items = $cart_obj['items'];
            $shopUrl = isset($request['shop_domain']) ? $request['shop_domain'] : '';
            if ($shopUrl) {
                $shop_settings = Setting::where('shop_domain', $shopUrl)->first();
                if ($shop_settings) {
                    $msg_color = $shop_settings->success_msg_color;
                }
            }
            if (sizeof($items) > 0) {
                foreach ($items as $item):
                    $item = (object)$item;
                    $offer_id = 0;
                    $upsell_msg = '';
                    $success_msg = '';
                    $upsell_quantity = 0;
                    $offerItem_exists = OfferItem::where("product_id", $item->product_id)
                        ->where("shop_domain", $shopUrl)
                        ->first();

                    if ($offerItem_exists) {
                        $offer_id = $offerItem_exists->offer_id;
                        $offer_details = Offer::where("id", $offer_id)->first();
                        $offer_discounts = Discount::where("offer_id", $offer_id)->orderBy('amount_or_quantity', 'ASC')->get();

                        if ($offer_details && $offer_discounts) {
                            if ($offer_details->discount_type == 'quantity') {

                                for ($j = 0; $j < count($offer_discounts); $j++) {

                                    if ($item->quantity >= $offer_discounts[$j]['amount_or_quantity']) {

                                        if ($offer_discounts[$j]['discount_by'] == 'percentage') {

                                            $success_msg = $offer_details->success_msg;
                                            if (strpos($success_msg, '{{discount}}') !== false) {
                                                $success_msg = str_replace("{{discount}}", $offer_discounts[$j]['discount'] . "%", $success_msg);
                                            }
                                            $discount_val = $offer_discounts[$j]['discount'];
                                            $discounted_price = ($discount_val / 100) * $item->original_price;
                                            $item->discounted_price = $item->original_price - $discounted_price;
                                            $discounted_price = ($discount_val / 100) * $item->original_line_price;
                                            $item->discounted_line_price = ($item->original_line_price - $discounted_price) / 100;
                                            $item->original_price_format = str_replace("{{amount}}", " " . $item->original_price / 100, $request['money_format']);
                                            $item->original_line_price_format = str_replace("{{amount}}", " " . $item->original_line_price / 100, $request['money_format']);
                                            $item->discounted_price_format = str_replace("{{amount}}", " " . $item->discounted_price, $request['money_format']);
                                            $item->discounted_line_price_format = str_replace("{{amount}}", " " . $item->discounted_line_price, $request['money_format']);

                                        } else {
                                            $success_msg = $offer_details->success_msg;
                                            if (strpos($success_msg, '{{discount}}') !== false) {
                                                $success_msg = str_replace("{{discount}}", $offer_discounts[$j]['discount'] . " " . $offer_discounts[$j]['currency'], $success_msg);
                                            }
                                            $discount_val = $offer_discounts[$j]['discount'];
                                            $item->discounted_price = ($item->original_price / 100 - $discount_val);
                                            $item->discounted_line_price = ($item->original_line_price / 100 - $discount_val);
                                            $item->original_price_format = str_replace("{{amount}}", " " . $item->original_price / 100, $request['money_format']);
                                            $item->original_line_price_format = str_replace("{{amount}}", " " . $item->original_line_price / 100, $request['money_format']);
                                            $item->discounted_price_format = str_replace("{{amount}}", " " . $item->discounted_price, $request['money_format']);
                                            $item->discounted_line_price_format = str_replace("{{amount}}", " " . $item->discounted_line_price, $request['money_format']);
                                        }
                                    }
                                    if ($item->quantity < $offer_discounts[$j]['amount_or_quantity']) {
                                        $upsell_quantity = $offer_discounts[$j]['amount_or_quantity'];
                                        if ($offer_discounts[$j]['discount_by'] == 'percentage') {
                                            $upsell_msg = $offer_details->upsell_msg;
                                            if (strpos($upsell_msg, '{{minimum}}') !== false) {
                                                $upsell_msg = str_replace("{{minimum}}", $offer_discounts[$j]['amount_or_quantity'], $upsell_msg);
                                            }
                                            if (strpos($upsell_msg, '{{discount}}') !== false) {
                                                $upsell_msg = str_replace("{{discount}}", $offer_discounts[$j]['discount'] . "%", $upsell_msg);
                                            }
                                        } else {
                                            $upsell_msg = $offer_details->upsell_msg;
                                            if (strpos($upsell_msg, '{{minimum}}') !== false) {
                                                $upsell_msg = str_replace("{{minimum}}", $offer_discounts[$j]['amount_or_quantity'], $upsell_msg);
                                            }
                                            if (strpos($upsell_msg, '{{discount}}') !== false) {
                                                $upsell_msg = str_replace("{{discount}}", $offer_discounts[$j]['discount'] . " " . $offer_discounts[$j]['currency'], $upsell_msg);
                                            }

                                        }

                                        break;
                                    }

                                }
                            } else {
                                for ($j = 0; $j < count($offer_discounts); $j++) {
                                    if (($item->original_line_price / 100) >= $offer_discounts[$j]['amount_or_quantity']) {
                                        if ($offer_discounts[$j]['discount_by'] == 'percentage') {
                                            $success_msg = $offer_details->success_msg;
                                            if (strpos($success_msg, '{{discount}}') !== false) {
                                                $success_msg = str_replace("{{discount}}", $offer_discounts[$j]['discount'] . "%", $success_msg);
                                            }
                                            $discount_val = $offer_discounts[$j]['discount'];
                                            $discounted_price = ($discount_val / 100) * $item->original_price;
                                            $item->discounted_price = $item->original_price - $discounted_price;
                                            $discounted_price = ($discount_val / 100) * $item->original_line_price;
                                            $item->discounted_line_price = ($item->original_line_price - $discounted_price) / 100;
                                            $item->original_price_format = str_replace("{{amount}}", " " . $item->original_price / 100, $request['money_format']);
                                            $item->original_line_price_format = str_replace("{{amount}}", " " . $item->original_line_price / 100, $request['money_format']);
                                            $item->discounted_price_format = str_replace("{{amount}}", " " . $item->discounted_price, $request['money_format']);
                                            $item->discounted_line_price_format = str_replace("{{amount}}", " " . $item->discounted_line_price, $request['money_format']);

                                        } else {
                                            $success_msg = $offer_details->success_msg;
                                            if (strpos($success_msg, '{{discount}}') !== false) {
                                                $success_msg = str_replace("{{discount}}", $offer_discounts[$j]['discount'] . $offer_discounts[$j]['currency'], $success_msg);
                                            }
                                            $discount_val = $offer_discounts[$j]['discount'];
                                            $item->discounted_price = ($item->original_price / 100 - $discount_val);
                                            $item->discounted_line_price = ($item->original_line_price / 100 - $discount_val);


                                            $item->original_price_format = str_replace("{{amount}}", " " . $item->original_price / 100, $request['money_format']);
                                            $item->original_line_price_format = str_replace("{{amount}}", " " . $item->original_line_price / 100, $request['money_format']);
                                            $item->discounted_price_format = str_replace("{{amount}}", " " . $item->discounted_price, $request['money_format']);
                                            $item->discounted_line_price_format = str_replace("{{amount}}", " " . $item->discounted_line_price, $request['money_format']);
                                        }
                                    }


                                    if (($item->original_line_price / 100) < $offer_discounts[$j]['amount_or_quantity']) {
                                        if ($offer_discounts[$j]['discount_by'] == 'percentage') {
                                            $upsell_msg = $offer_details->upsell_msg;
                                            if (strpos($upsell_msg, '{{minimum}}') !== false) {
                                                $upsell_msg = str_replace("{{minimum}}", $offer_discounts[$j]['amount_or_quantity'] . $offer_discounts[$j]['currency'], $upsell_msg);
                                            }
                                            if (strpos($upsell_msg, '{{discount}}') !== false) {
                                                $upsell_msg = str_replace("{{discount}}", $offer_discounts[$j]['discount'] . "%", $upsell_msg);
                                            }

                                        } else {

                                            $upsell_msg = $offer_details->upsell_msg;
                                            if (strpos($upsell_msg, '{{minimum}}') !== false) {
                                                $upsell_msg = str_replace("{{minimum}}", $offer_discounts[$j]['amount_or_quantity'] . $offer_discounts[$j]['currency'], $upsell_msg);
                                            }
                                            if (strpos($upsell_msg, '{{discount}}') !== false) {
                                                $upsell_msg = str_replace("{{discount}}", $offer_discounts[$j]['discount'] . $offer_discounts[$j]['currency'], $upsell_msg);
                                            }

                                        }
                                        break;
                                    }

                                }
                            }

                        }
                    } else {
                      //  dd( $item->collection_ids);
                        $item_collection_ids = isset($item->collection_ids)?$item->collection_ids:[];
                        if (is_array($item_collection_ids) && sizeof($item_collection_ids) > 0) {
                            foreach ($item_collection_ids as $item_collection_id):
                                $offerItem_exists = OfferItem::where("collection_id", $item_collection_id)
                                    ->where("shop_domain", $shopUrl)
                                    ->first();
                                if ($offerItem_exists) {

                                    $offer_id = $offerItem_exists->offer_id;
                                    $offer_details = Offer::where("id", $offer_id)->first();
                                    $upsell_quantity = 0;
                                    $offer_discounts = Discount::where("offer_id", $offer_id)->orderBy('amount_or_quantity', 'ASC')->get();
                                    if ($offer_details && $offer_discounts) {
                                        if ($offer_details->discount_type == 'quantity') {
                                            for ($j = 0; $j < count($offer_discounts); $j++) {

                                                if ($item->quantity >= $offer_discounts[$j]['amount_or_quantity']) {

                                                    if ($offer_discounts[$j]['discount_by'] == 'percentage') {

                                                        $success_msg = $offer_details->success_msg;
                                                        if (strpos($success_msg, '{{discount}}') !== false) {
                                                            $success_msg = str_replace("{{discount}}", $offer_discounts[$j]['discount'] . "%", $success_msg);
                                                        }
                                                        $discount_val = $offer_discounts[$j]['discount'];

                                                        $discounted_price = ($discount_val / 100) * $item->original_price;
                                                        $item->discounted_price = $item->original_price - $discounted_price;

                                                        $discounted_price = ($discount_val / 100) * $item->original_line_price;
                                                        $item->discounted_line_price = ($item->original_line_price - $discounted_price) / 100;
                                                        $item->original_price_format = str_replace("{{amount}}", " " . $item->original_price / 100, $request['money_format']);
                                                        $item->original_line_price_format = str_replace("{{amount}}", " " . $item->original_line_price / 100, $request['money_format']);
                                                        $item->discounted_price_format = str_replace("{{amount}}", " " . $item->discounted_price, $request['money_format']);
                                                        $item->discounted_line_price_format = str_replace("{{amount}}", " " . $item->discounted_line_price, $request['money_format']);

                                                    } else {

                                                        $success_msg = $offer_details->success_msg;
                                                        if (strpos($success_msg, '{{discount}}') !== false) {
                                                            $success_msg = str_replace("{{discount}}", $offer_discounts[$j]['discount'] . " " . $offer_discounts[$j]['currency'], $success_msg);
                                                        }
                                                        $discount_val = $offer_discounts[$j]['discount'];
                                                        $item->discounted_price = ($item->original_price / 100 - $discount_val);
                                                        $item->discounted_line_price = ($item->original_line_price / 100 - $discount_val);
                                                        $item->original_price_format = str_replace("{{amount}}", " " . $item->original_price / 100, $request['money_format']);
                                                        $item->original_line_price_format = str_replace("{{amount}}", " " . $item->original_line_price / 100, $request['money_format']);
                                                        $item->discounted_price_format = str_replace("{{amount}}", " " . $item->discounted_price, $request['money_format']);
                                                        $item->discounted_line_price_format = str_replace("{{amount}}", " " . $item->discounted_line_price, $request['money_format']);

                                                    }

//                                   break;
                                                }
                                                if ($item->quantity < $offer_discounts[$j]['amount_or_quantity']) {
                                                    $upsell_quantity = $offer_discounts[$j]['amount_or_quantity'];
                                                    if ($offer_discounts[$j]['discount_by'] == 'percentage') {
                                                        $upsell_msg = $offer_details->upsell_msg;
                                                        if (strpos($upsell_msg, '{{minimum}}') !== false) {
                                                            $upsell_msg = str_replace("{{minimum}}", $offer_discounts[$j]['amount_or_quantity'], $upsell_msg);
                                                        }
                                                        if (strpos($upsell_msg, '{{discount}}') !== false) {
                                                            $upsell_msg = str_replace("{{discount}}", $offer_discounts[$j]['discount'] . "%", $upsell_msg);
                                                        }
                                                    } else {
                                                        $upsell_msg = $offer_details->upsell_msg;
                                                        if (strpos($upsell_msg, '{{minimum}}') !== false) {
                                                            $upsell_msg = str_replace("{{minimum}}", $offer_discounts[$j]['amount_or_quantity'], $upsell_msg);
                                                        }
                                                        if (strpos($upsell_msg, '{{discount}}') !== false) {
                                                            $upsell_msg = str_replace("{{discount}}", $offer_discounts[$j]['discount'] . " " . $offer_discounts[$j]['currency'], $upsell_msg);
                                                        }

                                                    }
                                                    break;
                                                }

                                            }
                                        } else {

                                            for ($j = 0; $j < count($offer_discounts); $j++) {
                                                if (($item->original_line_price / 100) >= $offer_discounts[$j]['amount_or_quantity']) {
                                                    if ($offer_discounts[$j]['discount_by'] == 'percentage') {

                                                        $success_msg = $offer_details->success_msg;
                                                        if (strpos($success_msg, '{{discount}}') !== false) {
                                                            $success_msg = str_replace("{{discount}}", $offer_discounts[$j]['discount'] . "%", $success_msg);
                                                        }
                                                        $discount_val = $offer_discounts[$j]['discount'];
                                                        $discounted_price = ($discount_val / 100) * $item->original_price;
                                                        $item->discounted_price = $item->original_price - $discounted_price;
                                                        $discounted_price = ($discount_val / 100) * $item->original_line_price;
                                                        $item->discounted_line_price = ($item->original_line_price - $discounted_price) / 100;
                                                        $item->original_price_format = str_replace("{{amount}}", " " . $item->original_price / 100, $request['money_format']);
                                                        $item->original_line_price_format = str_replace("{{amount}}", " " . $item->original_line_price / 100, $request['money_format']);
                                                        $item->discounted_price_format = str_replace("{{amount}}", " " . $item->discounted_price, $request['money_format']);
                                                        $item->discounted_line_price_format = str_replace("{{amount}}", " " . $item->discounted_line_price, $request['money_format']);

                                                    } else {

                                                        $success_msg = $offer_details->success_msg;
                                                        if (strpos($success_msg, '{{discount}}') !== false) {
                                                            $success_msg = str_replace("{{discount}}", $offer_discounts[$j]['discount'] . $offer_discounts[$j]['currency'], $success_msg);
                                                        }
                                                        $discount_val = $offer_discounts[$j]['discount'];
                                                        $item->discounted_price = ($item->original_price / 100) - $discount_val;

                                                        $item->discounted_line_price = ($item->original_line_price / 100) - $discount_val;
                                                        $item->original_price_format = str_replace("{{amount}}", " " . $item->original_price / 100, $request['money_format']);
                                                        $item->original_line_price_format = str_replace("{{amount}}", " " . $item->original_line_price / 100, $request['money_format']);
                                                        $item->discounted_price_format = str_replace("{{amount}}", " " . $item->discounted_price, $request['money_format']);
                                                        $item->discounted_line_price_format = str_replace("{{amount}}", " " . $item->discounted_line_price, $request['money_format']);
                                                    }
                                                }


                                                if (($item->original_line_price / 100) < $offer_discounts[$j]['amount_or_quantity']) {

                                                    if ($offer_discounts[$j]['discount_by'] == 'percentage') {
                                                        $upsell_msg = $offer_details->upsell_msg;
                                                        if (strpos($upsell_msg, '{{minimum}}') !== false) {
                                                            $upsell_msg = str_replace("{{minimum}}", $offer_discounts[$j]['amount_or_quantity'] . $offer_discounts[$j]['currency'], $upsell_msg);
                                                        }
                                                        if (strpos($upsell_msg, '{{discount}}') !== false) {
                                                            $upsell_msg = str_replace("{{discount}}", $offer_discounts[$j]['discount'] . "%", $upsell_msg);
                                                        }

                                                    } else {

                                                        $upsell_msg = $offer_details->upsell_msg;
                                                        if (strpos($upsell_msg, '{{minimum}}') !== false) {
                                                            $upsell_msg = str_replace("{{minimum}}", $offer_discounts[$j]['amount_or_quantity'] . $offer_discounts[$j]['currency'], $upsell_msg);
                                                        }
                                                        if (strpos($upsell_msg, '{{discount}}') !== false) {
                                                            $upsell_msg = str_replace("{{discount}}", $offer_discounts[$j]['discount'] . $offer_discounts[$j]['currency'], $upsell_msg);
                                                        }

                                                    }
//
                                                    break;
                                                }

                                            }
                                        }

                                        break;
                                    }
                                }
                            endforeach;

                        }

                    }
                    $item->offer_id = $offer_id;
                    $item->success_note = $success_msg;
                    $item->upsell_note = $upsell_msg;
                    $item->upsell_quantity = $upsell_quantity;
                    $item->msg_color = $msg_color;
                    $items_array[] = $item;
                    if (isset($item->discounted_line_price) && isset($item->discounted_price)) {
                        $discounts['discounted_price'] = $discounts['discounted_price'] + $item->discounted_line_price;
                    } else {
                        $discounts['discounted_price'] = $discounts['discounted_price'] + ($item->original_line_price / 100);

                    }

                endforeach;
            }

            $discounts['show_discount_code'] = false;
            $discounts['discounted_price_html'] = str_replace("{{amount}}", $discounts['discounted_price'], $request['money_format']);
            $discounts['discount_code'] = null;
            $discounts['discount_code_value'] = 0;
            $discounts['discount_item_html'] = "";
            $discounts['summary_item_html'] = null;
            $discounts['upsell_arr'] = [];
            $discounts['cart']['items'] = $items_array;
            $end_result['notifications'] = [];
            $end_result['discounts'] = $discounts;

            return response()->json($end_result);


        }
    }


    function shopify_products(Request $request)
    {
        $shop = session('shop');
        $final_array = [];
        $request = $request->all();
        if ($shop) {
            $shopUrl = $shop['domain'];
            $accessToken = Shop::where('domain', $shopUrl)->first();
            $accessToken = $accessToken->access_token;
            if (isset($request['search']) && $request['search'] != '') {
                $title = $request['search'];
                $products = Shopify::setShopUrl($shopUrl)->setAccessToken($accessToken)->get("admin/products.json", ["title" => $title, 'fields' => 'title,id']);

            } else {
                $products = Shopify::setShopUrl($shopUrl)->setAccessToken($accessToken)->get("admin/products.json", ['limit' => 250]);
            }

            if ($products && sizeof($products) > 0) {
                foreach ($products as $key => $product):
                    $final_array[$key]['id'] = $product->id;
                    $final_array[$key]['text'] = $product->title;
                endforeach;
            }

        }
        //dd(\GuzzleHttp\json_encode(['results' => $final_array]));
        return ['results' => $final_array];
    }



    function collections(Request $request)
    {
        $shop = session('shop');
        $final_array = [];
        $request = $request->all();
        if ($shop) {
            $shopUrl = $shop['domain'];
            $accessToken = Shop::where('domain', $shopUrl)->first();
            $accessToken = $accessToken->access_token;
            if (isset($request['search']) && $request['search'] != '') {
                $title = $request['search'];

                $collections = Shopify::setShopUrl($shopUrl)->setAccessToken($accessToken)->get("admin/collections.json");

            } else {
                $collections = Shopify::setShopUrl($shopUrl)->setAccessToken($accessToken)->get("admin/collections.json");
            }

            if ($collections && sizeof($collections) > 0) {
                foreach ($collections as $key => $collection):
                    $final_array[$key]['id'] = $collection->id;
                    $final_array[$key]['text'] = $collection->title;
                endforeach;
            }

        }
        //dd(\GuzzleHttp\json_encode(['results' => $final_array]));
        return ['results' => $final_array];
    }

}
