<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entities\Setting;
use Session;
class SettingsController extends Controller
{
    public function index()
    {
        $shop = session('shop');
        $shopUrl = $shop['domain'];
        $shop_id = $shop['id'];
        $shop_settings = Setting::where('shop_id', $shop_id)->where('shop_domain', $shopUrl)->first();
        return view('settings.settings', compact('shop_settings'));
    }


    public function store(Request $request)
    {
        $request = $request->all();
        $shop = session('shop');
        $shopUrl = $shop['domain'];
        $shop_id = $shop['id'];
        $shop_settings = Setting::where('shop_id', $shop_id)->where('shop_domain', $shopUrl)->first();
        if($shop_settings){
            $shop_settings->success_msg_color=$request['success_msg_color'];
            $shop_settings->offer_text_color=$request['offer_text_color'];
            $shop_settings->offer_bg_color=$request['offer_bg_color'];
            $shop_settings->product_page_msg=$request['product_page_msg'];
            $shop_settings->currency_format=$request['currency_format'];
            $shop_settings->save();
        }else{
            $shop_settings =new Setting();
            $shop_settings->shop_id=$shop_id;
            $shop_settings->shop_domain=$shopUrl;
            $shop_settings->success_msg_color=$request['success_msg_color'];
            $shop_settings->offer_text_color=$request['offer_text_color'];
            $shop_settings->offer_bg_color=$request['offer_bg_color'];
            $shop_settings->product_page_msg=$request['product_page_msg'];
            $shop_settings->currency_format=$request['currency_format'];
            $shop_settings->save();
        }
        Session::flash('message', 'Settings Saved Successfully..!');
        return redirect()->route('offer_listing');



    }
}
