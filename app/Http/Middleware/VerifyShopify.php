<?php

namespace App\Http\Middleware;

use Closure;
use Oseintow\Shopify\Facades\Shopify;

class VerifyShopify
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        dd('hhhhhhhhhhhhhhhhhhhhhh');
//        $data = $request->getContent();
//
//        $hmacHeader = $request->server('HTTP_X_SHOPIFY_HMAC_SHA256');
//
//
//        if (Shopify::verifyWebHook($data, $hmacHeader)) {
//            logger("verification passed");
//        } else {
//            logger("verification failed");
//        }

        return $next($request);
    }
}
