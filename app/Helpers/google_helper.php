<?php

use App\GoogleSheetInfo;
use App\SyncSchedule;

function selected_sheet()
{
    return GoogleSheetInfo::first();
}

function selected_schedule_time()
{
    return SyncSchedule::first();
}


