<?php

namespace App\Services;

use Oseintow\Shopify\Facades\Shopify;
use Session;

class ShopifyService
{

    public function verify_webhooks()
    {
        if (Session::has('shop')) {
            $shop = session('shop');
            $shopUrl = $shop['domain'];
            $accessToken = session('shopify_token');
        $webhooks = Shopify::setShopUrl($shopUrl)->setAccessToken($accessToken)->get("admin/webhooks.json");

        if ($webhooks && sizeof($webhooks) > 0) {
            $create_product_webhook = false;
            $create_order_webhook = false;

            foreach ($webhooks as $webhook):
                if ($webhook->topic == 'products/create') {
                    $create_product_webhook = true;
                }
                if ($webhook->topic == 'orders/create') {
                    $create_order_webhook = true;
                }
            endforeach;
            if (!$create_product_webhook) {
                $this->create_webhook($shop, 'products/create', 'products_create_webhook');
            }
            if (!$create_order_webhook) {
                $this->create_webhook($shop, 'orders/create', 'orders_create_webhook');
            }
        } else {
            // post webhooks

//            create webhooks for products

            $webhook = $this->create_webhook($shop, 'products/create', 'products_create_webhook');

            //            create webhooks for orders

            $webhook = $this->create_webhook($shop, 'orders/create', 'orders_create_webhook');


        }
    }
    }

    private function create_webhook($shop, $type, $callbacK_url)
    {
        $shopUrl = $shop['domain'];
        $accessToken = session('shopify_token');

        $callbacK_url = env('APP_URL') . '/shopify_products_sync/public/' . $callbacK_url;

        $post_data = array('webhook' => array('topic' => $type, 'address' => $callbacK_url, 'format' => 'json'));

        $webhook = Shopify::setShopUrl($shopUrl)->setAccessToken($accessToken)->post("admin/webhooks.json", $post_data);

    }


    public function delete_webhook($webhook_topic = 'products/create')
    {

        if (Session::has('shop')) {
            $shop = session('shop');
            $shopUrl = $shop['domain'];
            $accessToken = session('shopify_token');
            $webhooks = Shopify::setShopUrl($shopUrl)->setAccessToken($accessToken)->get("admin/webhooks.json");
            if ($webhooks && sizeof($webhooks) > 0) {

                foreach ($webhooks as $webhook):
                    if ($webhook->topic == $webhook_topic) {
                        $webhook_id = $webhook->id;
                        Shopify::setShopUrl($shopUrl)->setAccessToken($accessToken)->delete("admin/webhooks/" . $webhook_id . "json");

                    }

                endforeach;

            }

        }
    }
}
