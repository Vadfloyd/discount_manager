<?php

namespace App\Services;

use Sheets;
use Google;
use Session;
use Log;
use Google\Spreadsheet\DefaultServiceRequest;
use Google\Spreadsheet\ServiceRequestFactory;
use Oseintow\Shopify\Facades\Shopify;
use App\User;
use App\Webhook;
use App\GoogleSheetInfo;
use App\Shop;

class GoogleSheet
{

    public function checkProductOnShopifyStore($handle = null, $product_array)
    {

        $shop = session('shop');
        $shopify_token = session('shopify_token');
        if ($handle && $shop && $shopify_token) {

            $shopUrl = $shop['domain'];
            $product_data = Shopify::setShopUrl($shopUrl)->setAccessToken($shopify_token)->get("admin/products.json", array('handle' => $handle));

            if ($product_data && isset($product_data[0]->id)) {
                $product_id = $product_data[0]->id;

                //updating product
                return $this->insertProductOnShopifyStore($shopify_token, $shopUrl, $product_id, $product_array);
            } else {

                //inserting product
                return $this->insertProductOnShopifyStore($shopify_token, $shopUrl, $product_id = 0, $product_array);
            }

        }
        return false;
    }

    public function insertProductOnShopifyStore($shopify_token, $shopUrl, $product_id = 0, $product_array)
    {

        $exceptions = [];
        if ($product_id) {
            //removing image updations
            if (isset($product_array['product']['images']))
                unset($product_array['product']['images']);

            try {

                $product_data = Shopify::setShopUrl($shopUrl)->setAccessToken($shopify_token)->put("admin/products/" . "$product_id" . ".json", $product_array);
//                if ($product_data) {
//                    $image_ids = array();
//                    foreach ($product_data->product->images as $key => $proi) {
//                        if (isset($product_array['product']['images'][$key]))
//                            $image_ids[$key]['src'] = $product_array['product']['images'][$key];
//                        $image_ids[$key]['image_id'] = $proi->id;
//                    }
//
//                    if (sizeof($image_ids) > 0 && isset($product_array['product']['variants']) && sizeof($product_array['product']['variants'])>0) {
//                        foreach ($image_ids as $key => $image_id) {
//
//                        }
//                    }
//
//                }


                return true;
            } catch
            (\Exception $e) {

                $exceptions['product'] = isset($product_array['product']['handle']) ? $product_array['product']['handle'] : '';
                $exceptions['code'] = $e->getCode();
                $exceptions['message'] = $e->getMessage();

                return $exceptions;
            }
        } else {
            try {
                $product_data = Shopify::setShopUrl($shopUrl)->setAccessToken($shopify_token)->post("admin/products.json", $product_array);
                return true;
            } catch
            (\Exception $e) {
                $exceptions['product'] = isset($product_array['product']['handle']) ? $product_array['product']['handle'] : '';
                $exceptions['code'] = $e->getCode();
                $exceptions['message'] = $e->getMessage();
                return $exceptions;
            }
        }


    }

// insert single product //webhook insertion
    public function insert_single_product($data, $shop_domain)
    {
        if ($data) {
            $check_webhook = Webhook::where('product_id', $data->id)->where('shop_domain', $shop_domain)->first();
            if (!$check_webhook) {
                // saving webhook records
                $obj = new Webhook();
                $obj->product_id = $data->id;
                $obj->shop_domain = $shop_domain;
                $obj->save();

                // google loged in users
                $google_users = User::all();
                if ($google_users) {
                    foreach ($google_users as $user):
                        $token = $user->access_token;
                        $serviceRequest = new DefaultServiceRequest($token);
                        ServiceRequestFactory::setInstance($serviceRequest);

                        $spreadsheetService = new Google\Spreadsheet\SpreadsheetService();
                        $spreadsheetFeed = $spreadsheetService->getSpreadsheetFeed();

                        $spreadsheet = $spreadsheetFeed->getByTitle('Shopify-Products-Spreadsheet');

                        if ($spreadsheet) {
                            $worksheetFeed = $spreadsheet->getWorksheetFeed();

                            try {
                                $worksheet = $worksheetFeed->getByTitle('StoreProducts');
                                $cellFeed = $worksheet->getCellFeed();
                                $listFeed = $worksheet->getListFeed();

                                $rows_count = $listFeed->getTotalResults();
                                $row_no_p = $rows_count + 2;
                                if ($worksheet) {

                                    try {
                                        $batchRequest = new Google\Spreadsheet\Batch\BatchRequest();

                                        // inserting  product in sheet

                                        $col_no = 2;

                                        $batchRequest->addEntry($cellFeed->createCell($row_no_p, 1, $data->handle));
                                        $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->title));
                                        $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->body_html));
                                        $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->vendor));
                                        $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->product_type));
                                        $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->tags));
                                        $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->published_scope));


                                        $option1_name = isset($data->options[0]) ? $data->options[0]->name : '';
                                        $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no, $option1_name));
                                        $col_no = $col_no + 2;

                                        $option2_name = isset($data->options[1]) ? $data->options[1]->name : '';
                                        $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no, $option2_name));
                                        $col_no = $col_no + 2;

                                        $option3_name = isset($data->options[2]) ? $data->options[2]->name : '';
                                        $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no, $option3_name));


                                        if (sizeof($data->variants) > 0) {
                                            $v_counter = 0;
                                            foreach ($data->variants as $variant):

                                                $col_no = 9;
                                                if ($v_counter) {
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, 1, $data->handle));
                                                }
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no, $variant->option1));
                                                $col_no = $col_no + 2;

                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no, $variant->option2));
                                                $col_no = $col_no + 2;
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $variant->option3));

                                                // sku val
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $variant->sku));
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $variant->grams));
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $variant->inventory_management));
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $variant->inventory_quantity));
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $variant->inventory_policy));
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $variant->fulfillment_service));
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $variant->price));
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $variant->compare_at_price));
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $variant->requires_shipping));
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $variant->taxable));
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $variant->barcode));
                                                if (sizeof($data->images)) {
                                                    foreach ($data->images as $image):
                                                        if (in_array($variant->id, $image->variant_ids)) {
                                                            $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $image->src));
                                                            $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $image->position));
                                                            $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $image->alt));
                                                            break;
                                                        }
                                                    endforeach;
                                                }

                                                $v_counter++;
                                                $row_no_p++;
                                            endforeach;

                                        }
                                        $cellFeed->insertBatch($batchRequest);


                                    } catch
                                    (\Exception $e) {
                                        Log::info(print_r($e), true);
                                    }
                                }


                            } catch
                            (\Exception $e) {
                                Log::info(print_r($e), true);
                            }


                        }

                    endforeach;

                }
            }

        }
    }

// insert single order //webhook insertion
    public function insert_single_order($data, $shop_domain)
    {
        if ($data) {
            $check_webhook = Webhook::where('order_id', $data->id)->where('shop_domain', $shop_domain)->first();
            if (!$check_webhook) {
                // saving webhook records
                $obj = new Webhook();
                $obj->order_id = $data->id;
                $obj->shop_domain = $shop_domain;
                $obj->save();

                // google loged in users
                $google_users = User::all();
                if ($google_users) {
                    foreach ($google_users as $user):
                        $token = $user->access_token;
                        $serviceRequest = new DefaultServiceRequest($token);
                        ServiceRequestFactory::setInstance($serviceRequest);

                        $spreadsheetService = new Google\Spreadsheet\SpreadsheetService();
                        $spreadsheetFeed = $spreadsheetService->getSpreadsheetFeed();


                        $spreadsheet = $spreadsheetFeed->getByTitle('Shopify-Products-Spreadsheet');

                        if ($spreadsheet) {
                            $worksheetFeed = $spreadsheet->getWorksheetFeed();

                            try {
                                $worksheet = $worksheetFeed->getByTitle('StoreOrders');
                                $cellFeed = $worksheet->getCellFeed();
                                $listFeed = $worksheet->getListFeed();

                                $rows_count = $listFeed->getTotalResults();
                                $row_no_p = $rows_count + 2;
                                if ($worksheet) {

                                    try {
                                        $batchRequest = new Google\Spreadsheet\Batch\BatchRequest();

                                        // inserting  order in sheet
                                        $col_no = 2;
                                        $batchRequest->addEntry($cellFeed->createCell($row_no_p, 1, $data->name));
                                        $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->email));
                                        $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->financial_status));
                                        $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->processed_at));
                                        $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->fulfillment_status));
                                        $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->processed_at));
                                        $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->buyer_accepts_marketing));
                                        $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->currency));
                                        $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->subtotal_price));
                                        $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, 0));
                                        $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->total_tax));
                                        $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->total_price));

                                        if (sizeof($data->discount_codes) > 0) {
                                            $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->discount_codes[0]->code));
                                            $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->discount_codes[0]->amount));
                                        } else {
                                            $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, ''));
                                            $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, ''));
                                        }

                                        if (sizeof($data->shipping_lines) > 0) {
                                            $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->shipping_lines[0]->title));

                                        } else {
                                            $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, ''));

                                        }
                                        $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->created_at));
                                        // Adding line  items
                                        $first_row = true;

                                        if (sizeof($data->line_items) > 0) {

                                            $v_counter = 0;
                                            foreach ($data->line_items as $line_item):

                                                if ($v_counter) {
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, 1, $data->name));
                                                }
                                                $col_no = 17;
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $line_item->quantity));
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $line_item->variant_title));

                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $line_item->price));

                                                // sku val
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, 0));
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $line_item->sku));
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $line_item->requires_shipping));
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $line_item->taxable));
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $line_item->fulfillment_status));
                                                if ($first_row) {
                                                    $first_row = false;
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->billing_address->name));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->billing_address->city . ' ' . $data->billing_address->province));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->billing_address->address1));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->billing_address->address2));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->billing_address->company));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->billing_address->city));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->billing_address->zip));

                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->billing_address->province));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->billing_address->country));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->billing_address->phone));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->shipping_address->name));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, ''));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->shipping_address->address1));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->shipping_address->address2));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->shipping_address->company));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->shipping_address->city));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->shipping_address->zip));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->shipping_address->province));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->shipping_address->country));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->shipping_address->phone));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->note));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, ''));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->cancelled_at));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, implode(',', $data->payment_gateway_names)));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->reference));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, 0));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, 53, $data->tags));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, 54, 'Low'));
                                                }
                                                /// next line item columns
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, 51, $line_item->vendor));
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, 52, $line_item->id));
                                                $col_no = 55;
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, ''));
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $line_item->total_discount));

                                                $v_counter++;
                                                $row_no_p++;
                                            endforeach;

                                        }

                                        $cellFeed->insertBatch($batchRequest);


                                    } catch
                                    (\Exception $e) {
                                        Log::info(print_r($e), true);
                                    }
                                }


                            } catch
                            (\Exception $e) {
                                Log::info(print_r($e), true);
                            }


                        }

                    endforeach;

                }
            }

        }
    }

    public function get_spreadsheets()
    {
        $user = session('google_info');

        if ($user) {
            $token = $user->token;
            Google::setAccessToken($token);
            return $spreadsheets = Sheets::setService(Google::make('sheets'))
                ->setDriveService(Google::make('drive'))
                ->spreadsheetList();
        }
        return false;


    }

    public
    function products_sync_via_cron_job($user = null, $spreadsheet_details = null)
    {

        if ($user) {
            $google_access_token = $user->access_token;
        } else {
            $user = User::where('is_active', 1)->first();
            $google_access_token = $user->access_token;
            $google_refresh_token = $user->refresh_token;

        }

        if ($spreadsheet_details) {
            $spreadsheet_id = $spreadsheet_details->spreadsheet_id;
            $worksheet_id = $spreadsheet_details->worksheet_id;
        } else {

            $spreadsheet_details = GoogleSheetInfo::first();
            $spreadsheet_id = $spreadsheet_details->products_spreadsheet_id;
            $worksheet_id = $spreadsheet_details->products_worksheet_id;
        }


        $response = ['status' => 'success', 'message' => ''];
        if ($google_access_token && $spreadsheet_details) {

            $google_refresh_token = Google::refreshToken($google_refresh_token);

            if (!empty($google_refresh_token) && is_array($google_refresh_token)) {

                Google::setAccessToken($google_refresh_token['access_token']);
                User::updateOrCreate(
                    [
                        'email' => $user->email,
                    ],
                    [
                        'access_token' => $google_refresh_token['access_token'],
                        'refresh_token' => $google_refresh_token['refresh_token']

                    ]);

            } else {
                Google::setAccessToken($google_access_token);
            }

            Sheets::setService(Google::make('sheets'));
            Sheets::spreadsheet($spreadsheet_id);

            $spreadsheet_title = Sheets::spreadsheet($spreadsheet_id)->spreadsheetProperties()->title;
            $workingsheet_title = Sheets::sheetById($worksheet_id)->sheetProperties()->title;

            $serviceRequest = new DefaultServiceRequest($google_access_token);
            ServiceRequestFactory::setInstance($serviceRequest);

            $spreadsheetService = new Google\Spreadsheet\SpreadsheetService();
            $spreadsheetFeed = $spreadsheetService->getSpreadsheetFeed();


            $spreadsheet = $spreadsheetFeed->getByTitle($spreadsheet_title);
            $worksheetFeed = $spreadsheet->getWorksheetFeed();

            if ($workingsheet_title) {
                try {
                    $worksheet = $worksheetFeed->getByTitle($workingsheet_title);
                    if ($worksheet) {
                        $cellFeed = $worksheet->getCellFeed();
                        $batchRequest = new Google\Spreadsheet\Batch\BatchRequest();

                        $sheet_values = Sheets::sheetById($worksheet_id)->get();
                        if ($sheet_values && sizeof($sheet_values) > 0) {
                            // checking header values start
                            $sheet_header = $sheet_values[0];
                            $csv_headers = config('constants.shopify_products_csv_header');
                            $csv_headers = explode(',', $csv_headers);
                            // matching headers
                            $matched_headers = true;
                            for ($i = 0; $i < count($csv_headers); $i++):
                                $sheet_headr = isset($sheet_header[$i]) ? $sheet_header[$i] : '';
                                if ($csv_headers[$i] !== $sheet_headr) {
                                    $matched_headers = false;
                                }
                            endfor;

                            if ($matched_headers == false) {
                                // Headers in sheets
                                $csv_headers = config('constants.shopify_products_csv_header');
                                $csv_headers = explode(',', $csv_headers);
                                $csv_column_counter = 1;

                                $cellFeed = $worksheet->getCellFeed();

                                foreach ($csv_headers as $csv_header):
                                    $batchRequest->addEntry($cellFeed->createCell(1, $csv_column_counter, $csv_header));
                                    $csv_column_counter++;
                                endforeach;
//                    insert header into sheet
                                $cellFeed->insertBatch($batchRequest);
                                //inserting rows
                            }
                        }
                        // Getting shopify products
                        $shopUrl = env('SHOPIFY_SHOP_DOMAIN');
                        $shopUrl = str_replace('https://', '', $shopUrl);

                        if ($shopUrl) {

                            $shop = Shop::where('domain', $shopUrl)->first();
                            $shop_accesss_token = $shop->access_token;

                            try {
                                $shop_data = Shopify::setShopUrl($shopUrl)->setAccessToken($shop_accesss_token)->get("admin/products.json");

                                // inserting  procts in sheets
                                if ($shop_data && sizeof($shop_data) > 0) {
                                    $row_no_p = 2;
                                    foreach ($shop_data as $data):
                                        $old_rows['first_index'] = $row_no_p;
                                        $col_no = 2;
                                        $batchRequest->addEntry($cellFeed->createCell($row_no_p, 1, $data->handle));
                                        $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->title));
                                        $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->body_html));
                                        $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->vendor));
                                        $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->product_type));
                                        $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->tags));
                                        $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->published_scope));
// adding options

                                        $option1_name = isset($data->options[0]) ? $data->options[0]->name : '';
                                        $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no, $option1_name));
                                        $col_no = $col_no + 2;

                                        $option2_name = isset($data->options[1]) ? $data->options[1]->name : '';
                                        $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no, $option2_name));
                                        $col_no = $col_no + 2;

                                        $option3_name = isset($data->options[2]) ? $data->options[2]->name : '';
                                        $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no, $option3_name));


                                        if (sizeof($data->variants) > 0) {
                                            $v_counter = 0;
                                            foreach ($data->variants as $variant):

                                                $col_no = 9;

                                                if ($v_counter) {
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, 1, $data->handle));
                                                }

                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no, $variant->option1));
                                                $col_no = $col_no + 2;

                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no, $variant->option2));
                                                $col_no = $col_no + 2;
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $variant->option3));

                                                // sku val
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $variant->sku));
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $variant->grams));
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $variant->inventory_management));
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $variant->inventory_quantity));
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $variant->inventory_policy));
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $variant->fulfillment_service));
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $variant->price));
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $variant->compare_at_price));
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $variant->requires_shipping));
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $variant->taxable));
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $variant->barcode));
                                                $exist_variant_image = false;
//                                                if (sizeof($data->images)) {
//                                                    foreach ($data->images as $image):
//                                                        if (in_array($variant->id, $image->variant_ids)) {
//                                                            $exist_variant_image = true;
//                                                            $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $image->src));
//                                                            $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $image->position));
//                                                            $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $image->alt));
//                                                            break;
//                                                        }
//                                                    endforeach;
//                                                }

                                                $v_counter++;
                                                $row_no_p++;
                                            endforeach;

                                        } else {
                                            $row_no_p++;
                                        }
                                        $old_rows['last_index'] = $row_no_p;
                                        if (sizeof($data->images) && sizeof($old_rows) > 0) {
                                            $row_no_p = $old_rows['first_index'];
                                            foreach ($data->images as $image):
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, 1, $data->handle));
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, 25, $image->src));
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, 26, $image->position));
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, 27, $image->alt));
                                                $row_no_p++;
                                            endforeach;
                                            $rows_diff = ($old_rows['last_index'] - $old_rows['first_index']) - 1;
                                            if (sizeof($data->images) > $rows_diff) {
                                                $row_no_p = $row_no_p;
                                            } else {
                                                $row_no_p = $old_rows['last_index'];
                                            }
                                        }
                                    endforeach;
                                    $cellFeed->insertBatch($batchRequest);
                                    $response = ['status' => 'success', 'message' => 'Products Synced Successfuly'];

                                } else {
                                    $response = ['status' => 'error', 'message' => 'No Products Found on Shopify Store'];


                                }

                            } catch
                            (\Exception $e) {
                                return $response = ['status' => 'error', 'message' => 'Unable to Fetch Products From Shopify Store'];


                            }

                        } else {
                            return $response = ['status' => 'error', 'message' => 'Unable to Fetch Record From Shopify'];


                        }


                    }

                } catch
                (\Exception $e) {
                    return $response = ['status' => 'error', 'message' => 'Unable to Sync Products..No Worksheet Found '];

                }


            }


        }
        return $response;
    }

    public
    function orders_sync_via_cron_job($user = null, $spreadsheet_details = null)
    {

        if ($user) {
            $google_access_token = $user->access_token;
        } else {
            $user = User::where('is_active', 1)->first();
            $google_access_token = $user->access_token;
            $google_refresh_token = $user->refresh_token;

        }

        if ($spreadsheet_details) {
            $spreadsheet_id = $spreadsheet_details->orders_spreadsheet_id;
            $worksheet_id = $spreadsheet_details->orders_worksheet_id;
        } else {

            $spreadsheet_details = GoogleSheetInfo::first();
            $spreadsheet_id = $spreadsheet_details->orders_spreadsheet_id;
            $worksheet_id = $spreadsheet_details->orders_worksheet_id;
        }

        $response = ['status' => 'success', 'message' => ''];
        if ($google_access_token && $spreadsheet_details) {


            $google_refresh_token = Google::refreshToken($google_refresh_token);

            if (!empty($google_refresh_token) && is_array($google_refresh_token)) {

                Google::setAccessToken($google_refresh_token['access_token']);
                User::updateOrCreate(
                    [
                        'email' => $user->email,
                    ],
                    [
                        'access_token' => $google_refresh_token['access_token'],
                        'refresh_token' => $google_refresh_token['refresh_token']

                    ]);

            } else {
                Google::setAccessToken($google_access_token);
            }
            Sheets::setService(Google::make('sheets'));
            Sheets::spreadsheet($spreadsheet_id);
            $spreadsheet_title = Sheets::spreadsheet($spreadsheet_id)->spreadsheetProperties()->title;
            $workingsheet_title = Sheets::sheetById($worksheet_id)->sheetProperties()->title;

            $serviceRequest = new DefaultServiceRequest($google_access_token);
            ServiceRequestFactory::setInstance($serviceRequest);

            $spreadsheetService = new Google\Spreadsheet\SpreadsheetService();
            $spreadsheetFeed = $spreadsheetService->getSpreadsheetFeed();


            $spreadsheet = $spreadsheetFeed->getByTitle($spreadsheet_title);
            $worksheetFeed = $spreadsheet->getWorksheetFeed();

            if ($workingsheet_title) {
                try {
                    $worksheet = $worksheetFeed->getByTitle($workingsheet_title);
                    if ($worksheet) {
                        $cellFeed = $worksheet->getCellFeed();
                        $batchRequest = new Google\Spreadsheet\Batch\BatchRequest();

                        $sheet_values = Sheets::sheetById($worksheet_id)->get();

                        if ($sheet_values && sizeof($sheet_values) > 0) {

                            // checking header values start
                            $sheet_header = $sheet_values[0];
                            $csv_headers = config('constants.shopify_orders_csv_header');
                            $csv_headers = explode(',', $csv_headers);
                            // matching headers

                            $matched_headers = true;
                            for ($i = 0; $i < count($csv_headers); $i++):
                                $sheet_headr = isset($sheet_header[$i]) ? $sheet_header[$i] : '';
                                if ($csv_headers[$i] !== $sheet_headr) {
                                    $matched_headers = false;
                                }
                            endfor;

                            if ($matched_headers == false) {
                                // Headers in sheets
                                $csv_headers = config('constants.shopify_orders_csv_header');
                                $csv_headers = explode(',', $csv_headers);
                                $csv_column_counter = 1;

                                $cellFeed = $worksheet->getCellFeed();

                                foreach ($csv_headers as $csv_header):
                                    $batchRequest->addEntry($cellFeed->createCell(1, $csv_column_counter, $csv_header));
                                    $csv_column_counter++;
                                endforeach;

                                //            insert header into sheet
                                $cellFeed->insertBatch($batchRequest);
                            }
                        }

                        // Getting shopify products
                        $shopUrl = env('SHOPIFY_SHOP_DOMAIN');
                        $shopUrl = str_replace('https://', '', $shopUrl);

                        if ($shopUrl) {

                            $shop = Shop::where('domain', $shopUrl)->first();
                            $shop_accesss_token = $shop->access_token;

                            try {
                                $shop_data = Shopify::setShopUrl($shopUrl)->setAccessToken($shop_accesss_token)->get("admin/orders.json?status=any");

                                // inserting  procts in sheets
                                if ($shop_data && sizeof($shop_data) > 0) {
                                    $row_no_p = 2;
                                    foreach ($shop_data as $data):

                                        $old_rows['first_index'] = $row_no_p;
                                        $col_no = 2;
                                        $batchRequest->addEntry($cellFeed->createCell($row_no_p, 1, $data->name));
                                        $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->email));
                                        $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->financial_status));
                                        $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->processed_at));
                                        $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->fulfillment_status));
                                        $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->processed_at));
                                        $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->buyer_accepts_marketing));
                                        $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->currency));
                                        $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->subtotal_price));
                                        $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, 0));
                                        $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->total_tax));
                                        $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->total_price));

                                        if (sizeof($data->discount_codes) > 0) {
                                            $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->discount_codes[0]->code));
                                            $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->discount_codes[0]->amount));
                                        } else {
                                            $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, ''));
                                            $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, ''));
                                        }

                                        if (sizeof($data->shipping_lines) > 0) {
                                            $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->shipping_lines[0]->title));

                                        } else {
                                            $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, ''));

                                        }
                                        $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->created_at));
                                        // Adding line  items
                                        $first_row = true;

                                        if (sizeof($data->line_items) > 0) {

                                            $v_counter = 0;
                                            foreach ($data->line_items as $line_item):

                                                if ($v_counter) {
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, 1, $data->name));
                                                }
                                                $col_no = 17;
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $line_item->quantity));
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $line_item->variant_title));

                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $line_item->price));

                                                // sku val
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, 0));
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $line_item->sku));
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $line_item->requires_shipping));
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $line_item->taxable));
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $line_item->fulfillment_status));
                                                if ($first_row) {
                                                    $first_row = false;
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, isset($data->billing_address) ? $data->billing_address->name : ''));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, isset($data->billing_address) ? $data->billing_address->city . ' ' . $data->billing_address->province : ''));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, isset($data->billing_address) ? $data->billing_address->address1 : ''));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, isset($data->billing_address) ? $data->billing_address->address2 : ''));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, isset($data->billing_address) ? $data->billing_address->company : ''));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, isset($data->billing_address) ? $data->billing_address->city : ''));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, isset($data->billing_address) ? $data->billing_address->zip : ''));

                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, isset($data->billing_address) ? $data->billing_address->province : ''));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, isset($data->billing_address) ? $data->billing_address->country : ''));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, isset($data->billing_address) ? $data->billing_address->phone : ''));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, isset($data->shipping_address) ? $data->shipping_address->name : ''));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, ''));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, isset($data->shipping_address) ? $data->shipping_address->address1 : ''));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, isset($data->shipping_address) ? $data->shipping_address->address2 : ''));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, isset($data->shipping_address) ? $data->shipping_address->company : ''));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, isset($data->shipping_address) ? $data->shipping_address->city : ''));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, isset($data->shipping_address) ? $data->shipping_address->zip : ''));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, isset($data->shipping_address) ? $data->shipping_address->province : ''));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, isset($data->shipping_address) ? $data->shipping_address->country : ''));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, isset($data->shipping_address) ? $data->shipping_address->phone : ''));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->note));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, ''));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->cancelled_at));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, implode(',', $data->payment_gateway_names)));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $data->reference));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, 0));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, 53, $data->tags));
                                                    $batchRequest->addEntry($cellFeed->createCell($row_no_p, 54, 'Low'));
                                                }
                                                /// next line item columns
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, 51, $line_item->vendor));
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, 52, $line_item->id));
                                                $col_no = 55;
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, ''));
                                                $batchRequest->addEntry($cellFeed->createCell($row_no_p, $col_no++, $line_item->total_discount));

                                                $v_counter++;
                                                $row_no_p++;
                                            endforeach;

                                        } else {
                                            $row_no_p++;
                                        }

                                    endforeach;
                                    $cellFeed->insertBatch($batchRequest);
                                    return $response = ['status' => 'success', 'message' => 'Orders Synced Successfuly'];

                                } else {
                                    return $response = ['status' => 'error', 'message' => 'No Order Found on Shopify Store'];


                                }


                            } catch
                            (\Exception $e) {
                                $response = ['status' => 'error', 'message' => 'Unable to Fetch Orders From Shopify Store'];


                            }

                        } else {

                            return $response = ['status' => 'error', 'message' => 'Unable to Fetch Record From Shopify'];


                        }


                    }

                } catch
                (\Exception $e) {
                    return $response = ['status' => 'error', 'message' => 'Unable to Sync Orders..No Worksheet Found '];

                }


            }


        }
        return $response;
    }


}
