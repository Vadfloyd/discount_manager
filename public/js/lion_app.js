
<style type="text/css">
.lb-vol-wrapper{
    margin-top:20px;
}
.lion-cart-item-line-price .original_price {
    display: block;
    text-decoration: line-through;
}
.lion-cart-item-price, .lion-cart-total, .lion-cart-item-line-price .discounted_price {
    display: block;
    font-weight: bold;
}

.lion-cart-item-success-notes, .lion-cart-item-upsell-notes {
    display: block;
    font-weight:bold;
    color: #4195c5;
    font-size: 100%;
    a {
        color: #4195c5;
    }
}
.lion-cart-total {
    display: block;
    font-weight: bold;
}
.lion-messages{
    display:block;
}

  #lion-discount-item{
    font-size:70%;
    padding-top: 5px;
    padding-bottom: 5px;
}

  #lion-summary-item{
    font-size:70%;
    padding-top: 5px;
    padding-bottom: 5px;
}

.summary-line-note{
    padding-right: 10px;
}

.summary-line-discount{
    color: #4195c5;
}

input#lion-discount-code{
    max-width:200px;
    display:inline-block;
}

button#apply-lion-discount{
    display:inline-block;
    max-width:200px;
}

div#lion-notification-bar{
    font-size: 110%;
    background-color: #a7ea28;
    padding: 12px;
    color: #ffff00;
    font-family: inherit;
    z-index: 9999999999999;
    display: none;
    left: 0px;
    width: 100%;
    margin: 0px;
    margin-bottom:20px;
    text-align: center;
    text-transform: none;
}

div#lion-close-notification{
    float: right;
    font-weight: bold;
    height: 0;
    overflow: visible;
    cursor: pointer;
    margin-right: 2em;
}

.lb-eqs{
    display:none;
}

.lb-product-bundle {
    clear: both;
    margin: 0 0 20px;
}
.lb-product-bundle .bundle-title {
    font-size: 20px;
    margin-bottom: 15px;
}
.lb-product-bundle .lb-product-wrapper,
.lb-product-bundle .bundle-plus,
.lb-product-bundle .bundle-total{
    display: inline-block;
    text-align: center;
    vertical-align: middle;
}
.lb-product-bundle .bundle-total {
    text-align: left;
}

    #two-product.lb-product-bundle .bundle-plus img {
    position: relative;
    margin-left: -54% !important;
    max-width: 38px !important;
}

    #three-product.lb-product-bundle .bundle-plus img {
    position: relative;
    margin-left: -94% !important;
    max-width: 38px !important;
}
.lb-product-bundle .lb-product-wrapper {
    line-height: 15px;
}
.lb-product-bundle .lb-product-wrapper img {
    width: 80%;
}
.lb-product-bundle .bundle-name {
    margin: 10px 0 5px 0;
}
.lb-product-bundle .lb-price {
    margin-bottom: 5px;
    display: inline-block;
    margin-right: 5px;
    width:100%;
}
.lb-product-bundle .buy-bundle {
    text-align: right;
    padding: 5px 0;
}
.lb-product-bundle .buy-bundle input.addtocart {
    padding: 5px 10px;
    background-color: #222;
    color: #FFF;
    border: none;
}
.lion-variants-container select {
    width: 100%;
    margin-bottom: 5px
}
.lb-product-bundle .lion-variants-container {
    border: none;
    margin: 0;
    padding: 0;
}
.lb-product-bundle .lb-price.regular {
    color: #bbb;
    text-decoration: line-through;
}
.lb-product-bundle .lb-product-wrapper {
    vertical-align: top !important;
    line-height: 15px;
    text-align:center;
}
.lb-product-bundle .lb-image {
    min-height: 125px;
    max-height: 125px;
    line-height: 125px;
    border: 1px solid #d9d9d9;
    background: #fff;
    text-align: center;
}
.lb-product-bundle .lb-image img {
    max-height: 115px;
    max-width: 100%;
    vertical-align: middle;
    height: auto;
    width: auto;
}
.lb-product-bundle .bundle-plus {
    line-height: 125px;
    max-width: 22px;
}
.lb-bundle-wrapper .lb-product-bundle button {
    display: inline-block;
    padding: 4px 10px 4px;
    margin-bottom: 0;
    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.1);
    color: #ffffff;
    text-align: center;
    vertical-align: middle;
    background-repeat: repeat-x;
    border: 1px solid #cccccc;
    border-bottom-color: #b3b3b3;
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
    -moz-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
    box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
    cursor: pointer;
    background-color: #414141;
    background-image: -moz-linear-gradient(top, #555555, #222222);
    background-image: -ms-linear-gradient(top, #555555, #222222);
    background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#555555), to(#222222));
    background-image: -webkit-linear-gradient(top, #555555, #222222);
    background-image: -o-linear-gradient(top, #555555, #222222);
    background-image: linear-gradient(top, #555555, #222222);
    background-repeat: repeat-x;
    filter: progid: DXImageTransform.Microsoft.gradient(startColorstr='#555555', endColorstr='#222222', GradientType=0);
    border-color: #222222 #222222 #000000;
    border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
    filter: progid: dximagetransform.microsoft.gradient(enabled=false);
    padding: 5px 10px;
}
.lb-product-bundle button .top-button {
    font-size: 12px;
    border-bottom: 1px solid rgba(0, 0, 0, 0.2);
    display: block;
    padding: 0 5px 2px 5px;
}
.lb-product-bundle button .bottom-button {
    font-size: 14px;
    padding: 2px 5px 0 5px;
    display: block;
    border-top: 1px solid rgba(255, 255, 255, 0.1);
}
.lb-product-bundle .lb-price {
    position: static;
    background: none;
    text-align: center;
    padding: 0;
}
.lb-product-bundle .lb-eqs {
    width: auto;
    margin: 0 10px;
}

    #two-product.lb-product-bundle .lb-product-wrapper {
    width: 46%;
}
    #two-product.lb-product-bundle .bundle-plus {
    width: 4%;
    font-size: 75px
}
    #two-product.lb-product-bundle.with-total .bundle-total {
    width: 100%;
    margin: 10px 0 0 0;
    text-align: right;
    display:inline-block;
}
    #three-product.lb-product-bundle .lb-product-wrapper {
    width: 28%;
}
    #three-product.lb-product-bundle .bundle-plus {
    width: 3%;
    font-size: 50px
}
    #four-product.lb-product-bundle .lb-product-wrapper {
    width: 21%;
}
    #four-product.lb-product-bundle .bundle-plus {
    width: 3.5%;
    font-size: 40px
}

.with-total .bundle-total button{
    white-space: normal;
}

    #two-product.lb-product-bundle.with-total .lb-product-wrapper {
    width: 30%;
}
    #two-product.lb-product-bundle.with-total .bundle-plus {
    font-size: 70px;
}
    #two-product.lb-product-bundle.with-total .bundle-total {
    font-size: 35px;
    margin: 0;
    text-align: right;
}


    #two-product.lb-product-bundle.with-total .bundle-total {
    width: 34%;
}
    #three-product.lb-product-bundle.with-total .lb-product-wrapper {
    width: 21%;
}

    #three-product.lb-product-bundle.with-total .bundle-total button{
    width: 70%;
    margin-top:0px;
}
    #three-product.lb-product-bundle.with-total .bundle-plus {
    font-size: 45px
}

    #two-product.lb-product-bundle.with-total .bundle-total {
    display:inline-block;
    width: 30%;
    font-size: 30px;
    text-align: right;
}

    #three-product.lb-product-bundle.with-total .bundle-total {
    display:inline-block;
    width: 25%;
    font-size: 30px;
    text-align: right;
}

.lb-product-bundle.with-total .bundle-total .lb-eqs {
    display: inline-block;
    text-align: center;
    width: 24px;
    padding: 0px;
    margin: 0px;
}


    #four-product.lb-product-bundle.with-total .lb-product-wrapper,
    #four-product.lb-product-bundle.with-total .bundle-total {
    width: 20%;
}
    #four-product.lb-product-bundle.with-total .bundle-plus {
    font-size: 40px
}
    #four-product.lb-product-bundle.with-total .bundle-total {
    font-size: 25px
}

    #four-product.lb-product-bundle .lb-eqs {
    display: none;
}

    #four-product.lb-product-bundle.with-total .bundle-total {
    width: 100%;
    text-align: center;
}
    #max-two {
    width: 61%;
}
    #max-two .bundle-plus {
    width: 10%
}
    #three-product.max-two .bundle-total,
    #four-product.max-two .bundle-total {
    height: 384px;
}
    #three-product.max-two .lb-eqs,
    #four-product.max-two .lb-eqs {
    position: relative;
    top: 50%;
}
    #three-product.max-two button,
    #four-product.max-two button {
    position: relative;
    top: 50%;
}
    #max-two {
    width: 60%;
    display: inline-block;
}
    #max-two .bundle-plus:nth-child(4n+4) {
    display: none;
}
.lb-product-bundle .lb-product-wrapper {
    width: 45%;
    margin-bottom: 20px;
}
.lb-product-bundle .bundle-total {
    color: #bfbfbf;
    font-size: 21px;
    font-weight: bold;
}

.with-total.lb-product-bundle .lb-eqs {
    display: inline-block;
    text-align: center;
    width: 24px;
    vertical-align: middle;
}
.lb-product-bundle .bundle-total button {
    width: 100%;
    /*max-width: 150px;*/
}
    @media screen and (max-width: 650px) {
.lb-product-bundle .lb-product-wrapper {
        width: 100% !important;
    }
.lb-product-bundle .lb-product-wrapper > a:first-child {
        width: 35% !important;
        margin-right: 20px;
        display: block;
        float: left;
    }
.lb-product-bundle .lb-product-wrapper .bundle-name {
        margin: 0;
    }
.lb-product-bundle .bundle-plus {
        width: 100% !important;
        max-width: 100%;
        line-height: 0;
        border-bottom: 1px solid #ccc;
        margin-top: -42px;
    }
.lb-product-bundle .bundle-plus img {
        margin-left: 0 !important;
        margin-bottom: -18px;
    }
.lion-variants-container select {
        width: auto;
        margin-top: 10px
    }
.lb-product-bundle .bundle-total {
        width: 100% !important;
        text-align: center;
    }
.lb-product-bundle .lb-eqs {
        width: 100%;
        border-bottom: 1px solid #ccc;
        margin: -22px 0 0 0;
        position: relative;
    }
.lb-product-bundle .lb-eqs img {
        margin-left: 0 !important;
        margin-bottom: -18px;
        display: none;
    }
        #three-product.lb-product-bundle.with-total .bundle-total button{
        margin-top: 20px;
        width: 100%;
    }


.lb-product-bundle .lb-image-container{
        width: 35%;
        display: inline-block;
        float: left;
    }

.lb-product-bundle .lb-info-wrapper{
        width: 60%;
        display: inline-block;
        float: left;
        padding-left:10px;
    }

.lb-product-bundle .lb-info-wrapper select.lb-variants{
        width:100%;
    }


}

.lb-product-bundle .lb-price {
    color: #8C0000;
}
.lb-product-bundle .bundle-total {
    color: #8C1919;
}

.lb-product-bundle .bundle-total button {
    width: 100%;
}
.lb-product-bundle button .top-button {
    border-bottom: 0px;
}

    @media (max-width: 650px) {
.bundle-name {
        display: inline-block !important;
    }
.lb-eqs {
        width: 100% !important;
    }
.bundle-name {
        padding-top: 10px !important;
    }
.bundle-total button {
        margin-left: auto !important;
        margin-right: auto !important;
        display: block;
    }
}
.bundle-total button {
    color: #fff
    margin: -10px 0 -10px;
    width: 100%;
    border: none;
    text-decoration: none;
    font-size: 13%;
    font-family: inherit;
    text-transform: uppercase;
    font-weight: 500;
    padding: 10px;
    height:100%;
}

.with-total.lb-product-bundle .bundle-total button.add-lion-bundle {
    width: 70%;
}

.lb-product-bundle button .top-button {
    border-bottom: 0px;
}

    @media (max-width: 650px) {
.bundle-name {
        display: inline-block !important;
    }
.lb-eqs {
        width: 100% !important;
    }
.bundle-name {
        padding-top: 10px !important;
    }
.bundle-total button {
        margin-left: auto !important;
        margin-right: auto !important;
        display: block;
    }
}


.dp-popup div, .dp-popup span,
.dp-popup h1, .dp-popup h2, .dp-popup h3, .dp-popup h4, .dp-popup h5, .dp-popup h6,
.dp-popup p, .dp-popup a, .dp-popup img, .dp-popup b, .dp-popup u, .dp-popup i,
.dp-popup ol, .dp-popup ul, .dp-popup li,
.dp-popup form, .dp-popup label, .dp-popup table, .dp-popup tbody, .dp-popup tfoot,
.dp-popup thead, .dp-popup tr, .dp-popup th, .dp-popup td{
    margin: 0;
    padding: 0;
    border: 0;
    font-size: 100%;
    font: inherit;
    vertical-align: baseline;
    text-transform: none;
}

.dp-popup body {
    line-height: 1;
}
.dp-popup ol, .dp-popup ul {
    list-style: none;
}
.dp-popup table {
    border-collapse: collapse;
    border-spacing: 0;
}

.blocker {
    position: fixed;
    top: 0; right: 0; bottom: 0; left: 0;
    width: 100%; height: 100%;
    overflow: auto;
    z-index: 99999999;
    padding: 20px;
    box-sizing: border-box;
    background-color: rgb(0,0,0);
    background-color: rgba(0,0,0,0.75);
    text-align: center;
}
.blocker:before{
    content: '';
    display: inline-block;
    height: 100%;
    vertical-align: middle;
    margin-right: -0.05em;
}
.blocker.behind {
    background-color: transparent;
}
.dp-popup-dpModal {
    display: inline-block;
    min-width: 400px;
    vertical-align: middle;
    position: relative;
    z-index: 99999999;
    max-width: 600px;
    background: #fff;
    padding: 30px;
    -webkit-border-radius: 8px;
    -moz-border-radius: 8px;
    -o-border-radius: 8px;
    -ms-border-radius: 8px;
    border-radius: 8px;
    -webkit-box-shadow: 0 0 10px #000;
    -moz-box-shadow: 0 0 10px #000;
    -o-box-shadow: 0 0 10px #000;
    -ms-box-shadow: 0 0 10px #000;
    box-shadow: 0 0 10px #000;
    text-align: center;
    text-transform: none;
    font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
    font-size: 14px;
    line-height: 1.42857143;
    color: #333333;
    -moz-transition: background-color 0.15s linear;
    -webkit-transition: background-color 0.15s linear;
    -o-transition: background-color 0.15s linear;
    transition: background-color 0.15s cubic-bezier(0.785, 0.135, 0.150, 0.860);
}

.dp-popup-dpModal a {
    background-color: transparent;
}
.dp-popup-dpModal a:active,
.dp-popup-dpModal a:hover {
    outline: 0;
}

.dp-popup-dpModal hr {
    height: 0;
    -webkit-box-sizing: content-box;
    -moz-box-sizing: content-box;
    box-sizing: content-box;
    height: 0;
    margin-top: 20px;
    margin-bottom: 20px;
    border: 0;
    border-top: 1px solid #eeeeee;
}

.dp-popup-dpModal button,
.dp-popup-dpModal input,
.dp-popup-dpModal optgroup,
.dp-popup-dpModal select,
.dp-popup-dpModal textarea {
    color: inherit;
    font: inherit;
    margin: 0;
    font-family: inherit;
    font-size: inherit;
    line-height: inherit;
}


.dp-popup-dpModal button {
    overflow: visible;
}
.dp-popup-dpModal button,
.dp-popup-dpModal select {
    text-transform: none;
}
.dp-popup-dpModal button {
    -webkit-appearance: button;
    cursor: pointer;
}
.dp-popup-dpModal button::-moz-focus-inner,
.dp-popup-dpModal input::-moz-focus-inner {
    border: 0;
    padding: 0;
}
.dp-popup-dpModal input {
    line-height: normal;
}
.dp-popup-dpModal input[type='number']::-webkit-inner-spin-button,
.dp-popup-dpModal input[type='number']::-webkit-outer-spin-button {
    height: auto;
}


.dp-popup-dpModal body.fadein {
    background: rgba(0, 0, 0, 0.65);
}

#dpModal-container{
    width:100%;
}

.dp-popup-dpModal #popup-dpModal-container {
    background: white;
    padding: 12px 18px 40px 18px;
}

@media only screen and (min-width:500px) {
.dp-popup-dpModal #popup-dpModal-container {
        border-radius: 5px;
        padding: 30px 40px;
    }
}

@media only screen and (min-width:992px) {
.dp-popup-dpModal #popup-dpModal-container {
        margin-top: 140px;
    }
}

.dp-popup-dpModal .fade {
    opacity: 0;
    -webkit-transition: opacity 0.15s linear;
    -o-transition: opacity 0.15s linear;
    transition: opacity 0.15s linear;
}
.dp-popup-dpModal .fade.in {
    opacity: 1;
}

/* only the stuff we need added here */

.dp-popup-dpModal h2 {
    font-size: 24px;
    font-family: inherit;
    font-weight: 500;
    line-height: 1.1;
    color: inherit;
}

.dp-popup-dpModal h3 {
    font-family: inherit;
    font-weight: normal;
    line-height: 1.1;
    color: inherit;
    font-size: 18px;
    margin-top: 10px;
    margin-bottom: 20px;
    font-weight:500;
}

.dp-popup-dpModal p.body-text {
    font-size: 20;
    margin-top: 40px;
    margin-bottom: 10px;
}

.dp-popup-dpModal .form-control {
    display: block;
    width: 100%;
    height: 34px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 5px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    -moz-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
}

.dp-popup-dpModal .input-lg {
    height: 46px;
    padding: 10px 16px;
    line-height: 1.3333333;
    border-radius: 6px;
}
.dp-popup-dpModal select.input-lg {
    height: 46px;
}

@media screen and (-webkit-min-device-pixel-ratio:0) {
.dp-popup-dpModal select:focus,
.dp-popup-dpModal textarea:focus,
.dp-popup-dpModal input:focus {
        font-size: 16px;
        background: #eee;
    }
}


.dp-popup-dpModal .form-group {
    margin-bottom: 15px;
}

.dp-popup-dpModal .btn {
    display: inline-block;
    padding: 8px 12px;
    margin-bottom: 0;
    font-size: 14px;
    line-height: 1.42857143;
    text-align: center;
    vertical-align: middle;
    letter-spacing: 1px;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 3px;
}

.dp-popup-dpModal .btn-success {
    width: 100%;
    color: #ffffff;
    background-color: #4ed14e;
}

.dp-popup-dpModal .btn-lg {
    line-height: 24px;
    font-size: 15px;
    padding:14px;
    line-height: 1.3333333;
}

.dp-popup-dpModal .close {
    -webkit-appearance: none;
    padding: 0;
    cursor: pointer;
    background: 0 0;
    border: 0;
    text-align:center;
    font-size: 21px;
    font-weight: 700;
    line-height: 1;
    color: #000;
    text-shadow: 0 1px 0 #fff;
}

.dp-popup-dpModal form{
    margin-top:10px;
}

.dp-popup-dpModal .dpModal-content .close {
    font-size: 30px;
}
.dp-popup-dpModal .dpModal-backdrop.in {
    filter: alpha(opacity=65);
    opacity: .65;
}

.dp-popup-dpModal .completed_message {
    display: none;
}
.dp-popup-dpModal .complete .completed_message {
    display: block;
}

.dp-popup-dpModal .single-variant{
    display:none;
}

.dp-popup-dpModal div.footer{
    margin-top:20px;
}

.dp-popup-dpModal div.footer p{
    color: #b3b3b3;
    font-size:12px;
}

.dp-popup-dpModal .alert {
    padding: 10px;
    margin: 20px 0px;
    border: 1px solid transparent;
    border-radius: 4px;
}

.dp-popup-dpModal .alert-success {
    color: red;
    background-color: red;
    border-color: #d6e9c6;
}

.dp-popup-dpModal .alert-danger {
    color: red;
    background-color: red;
    border-color: #ebccd1;
}

.dp-popup-dpModal div.no-thanks{
    padding-top:20px;
}

.dp-popup-dpModal div.no-thanks a{
    color: #aaa;
    font-size: 100%;
}

@media (min-width: 0px) {
.dp-popup-dpModal {
        min-width:0px;
    }

}

@media (min-width: 768px) {
.dp-popup-dpModal {
        min-width:600px;
    }

}

.dp-popup-dpModal img{
    vertical-align: middle;
    max-width: 100%;
}

.dp-popup-dpModal img.single{
    margin-right: 20px;
    margin-left: 0px;
    display: inline-block;
    padding-right: 20px;
    max-width: 100%;
    height: auto;
    margin: 0 auto;
}


  #upsell-minimized-button{
    background-color:#44c767;

    border-top-left-radius: 8px;
    border-top-right-radius: 8px;
    -moz-border-top-left-radius: 8px;
    -moz-border-top-right-radius: 8px;
    -webkit-border-top-left-radius: 8px;
    -webkit-border-top-right-radius: 8px;

    /*border:1px solid #18ab29;*/

    display:inline-block;
    cursor:pointer;

    color:#ffffff;

    /*font-family:Arial;*/

    padding:10px 16px;
    text-decoration:none;

    background: #44c767;
    color: #ffffff;
    font-size: 16px;

    -webkit-transform: rotate(90deg);
    -webkit-transform-origin: left bottom;
    -moz-transform: rotate(90deg);
    -moz-transform-origin: left bottom;
    -ms-transform: rotate(90deg);
    -ms-transform-origin: left bottom;
    -o-transform: rotate(90deg);
    -o-transform-origin: left bottom;
    transform: rotate(90deg);
    left: 0px;
    top: 100px;
    transform-origin: left bottom;

    white-space: nowrap;
    position: fixed;
}

  #upsell-minimized-button:hover {
    /*background-color:#5cbf2a;*/
}


.dp-popup-dpModal a.close-dpModal {
    position: absolute;
    top: -12.5px;
    right: -12.5px;
    display: block;
    width: 30px;
    height: 30px;
    text-indent: -9999px;
    background: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAACXBIWXMAAAsTAAALEwEAmpwYAAAABGdBTUEAANjr9RwUqgAAACBjSFJNAABtmAAAc44AAPJxAACDbAAAg7sAANTIAAAx7AAAGbyeiMU/AAAG7ElEQVR42mJkwA8YoZjBwcGB6fPnz4w/fvxg/PnzJ2N6ejoLFxcX47Rp036B5Dk4OP7z8vL+P3DgwD+o3v9QjBUABBALHguZoJhZXV2dVUNDgxNIcwEtZnn27Nl/ZmZmQRYWFmag5c90dHQY5OXl/z98+PDn1atXv79+/foPUN9fIP4HxRgOAAggRhyWMoOwqKgoq6GhIZe3t7eYrq6uHBDb8/Pz27Gysloga/jz588FYGicPn/+/OapU6deOnXq1GdgqPwCOuA31AF/0S0HCCB0xAQNBU4FBQWB0NBQublz59oADV37Hw28ePHi74MHD/6ii3/8+HEFMGQUgQ6WEhQU5AeZBTWTCdkigABC9ylIAZeMjIxQTEyMysaNG/3+/v37AGTgr1+//s2cOfOXm5vbN6Caz8jY1NT0a29v76/v37//g6q9sHfv3khjY2M5YAgJgsyEmg0PYYAAQreUk4+PT8jd3V1l1apVgUAzfoIM2rlz5x9gHH5BtxAdA9PB1zNnzvyB+R6oLxoopgC1nBPZcoAAgiFQnLIDMb+enp5iV1eXBzDeHoI0z58//xcwIX0mZCkMg9S2trb+hFk+ffr0QCkpKVmQ2VA7QHYxAgQQzLesQMwjIiIilZWVZfPu3bstMJ+SYikyBmUzkBnA9HEMyNcCYgmQHVC7mAACCJagOEBBbGdnp7lgwYJEkIavX7/+BcY1SvAaGRl9tba2xohjMTGxL8nJyT+AWQsuxsbG9vnp06e/QWYdPHiwHmiWKlBcCGQXyNcAAQSzmBuoSQqYim3u37+/EKR48uTJv5ANB+bVr7Dga2xs/AkTV1JS+gq0AJyoQIkPWU9aWtoPkPibN2/2A/l6QCwJ9TULQADB4hcY//xKXl5eHt++fbsAUmxhYYHiM1DiAsr9R7ZcVVUVbikIdHd3/0TWIyws/AWYVsByAgICdkAxRSAWAGI2gACClV7C4uLiOv7+/lEgRZ8+ffqLLd6ABck3ZMuB6uCWrlu37je29HDx4kVwQisvL88FFqkaQDERUHADBBAomBl5eHiYgQmLE1hSgQQZgIUD1lJm69atf4HR8R1YKoH5QIPAWWP9+vV/gOI/gHkeQw+wGAXTwAJJ5t+/f/BUDRBA4NIEKMDMyMjICtQIiniG379/4yza7t69+//Lly8oDrty5co/bJaCAEwcZCkwwTJDLWYCCCCwxcDgY3z16hXDnTt3voP4EhISWA0BFgZMwNqHExh3jMiG1tbWsgHjnA2bHmAeBtdWwOL1MycnJ7wAAQggBmi+kgIW/OaKiorJwOLuFShO0LMSMPF9AUYBSpz6+vqixHlOTs4P9MIEWHaDsxSwYMoE2mEGFJcG5SKAAGJCqjv/AbPUn8ePH98ACQQHB6NUmZqamkzABIgSp5s3bwbHORCA1QDLAWZkPc7OzszA8oHl5cuXVy5duvQBGIXwWgoggGA+FgO6xkBNTS28r69vDrT2+Y1cIMDyJchX6KkXVEmAshd6KB06dAic94EO3AzkBwGxPhCLg8ptgACCZyeQp9jZ2b2AmsuAefM8tnxJCk5ISPgOLTKfAdNEOVDMA2QHLDsBBBC8AAFlbmCLwlZISCg5JSVlJizeQAaQaimoWAUFK0g/sGGwHiiWCMS2yAUIQAAxI7c4gEmeFZi4OJ48ecLMzc39CRiEmgEBASxA/QzA8vYvAxEgNjaWZc2aNezAsprp2LFjp4FpZRdQ+AkQvwLij0AMSoC/AQIIXklAC3AVUBoBxmE8sPXQAiyvN8J8fuPGjR/h4eHf0eMdhkENhOPHj8OT+NGjR88BxZuBOA5kJtRseCUBEECMSI0AdmgBDooDaaDl8sASTSkyMlKzpqZGU1paGlS7MABLrX83b978A6zwwakTmE0YgIkSnHpBfGCV+gxYh98qKSk5CeTeAxVeQPwUiN8AMSjxgdLNX4AAYkRqCLBAXcMHtVwSaLkMMMHJAvOq9IQJE9R8fHxElJWV1bEF8aNHj+7t27fvLTDlXwXGLyhoH0OD+DnU0k/QYAa1QP8BBBAjWsuSFWo5LzRYxKFYAljqiAHzqxCwIBEwMTERBdZeoOYMA7Bl+RFYEbwB5oS3IA9D4/IFEL+E4nfQ6IDFLTgvAwQQI5ZmLRtSsINSuyA0uwlBUyQPMPWD20/AKo8ByP4DTJTfgRgUjB+gFoEc8R6amGDB+wu5mQsQQIxYmrdMUJ+zQTM6NzQEeKGO4UJqOzFADQMZ/A1qCSzBfQXi71ALfyM17sEAIIAY8fQiWKAYFgIwzIbWTv4HjbdfUAf8RPLhH1icojfoAQKIEU8bG9kRyF0aRiz6YP0k5C4LsmUY9TtAADEyEA+IVfufGEUAAQYABejinPr4dLEAAAAASUVORK5CYII=') no-repeat 0 0;
}


.dp-popup .just-added{
    width:100%;
    border-bottom: 1px solid #eee;
    padding-bottom: 20px;
}

.dp-popup .multiple-products-true div{
    display:block;
    float:left;
}

.dp-popup .multiple-products-false div{
    display:block;
}

.dp-popup .multiple-products-false div.product-container{
    max-width:350px;
    width:100%;
    margin-left:auto;
    margin-right:auto;
}

.dp-popup .multiple-products-false .image{
    width:100%;
}

.dp-popup .multiple-products-true .image{
    width:100px;
}

.dp-popup .multiple-products-false .image img{
    max-width: 150px;
    max-height:150px;
}

.dp-popup .multiple-products-true .image img{
    max-width: 100px;
    max-height:100px;
}

.dp-popup .multiple-products-false .details{
    width:100%;
    text-align:center;
    font-size:14px;
    padding-left: 15px;
    padding-right: 15px;
    padding-top:20px;
}

.dp-popup .multiple-products-true .details{
    width:60%;
    text-align:left;
    font-size:14px;
    padding-left: 15px;
    padding-right: 15px;
}

.dp-popup .multiple-products-false .actions{
    width:100%;
    text-align:center;
    padding-top:20px;
}

.dp-popup .multiple-products-true .actions{
    vertical-align:middle;
    max-width: 116px;
    width:100%;
}

.dp-popup .other-upsells{
    width:100%;
}

.dp-popup .product-container{
    width:100%;
    padding-bottom: 10px;
    padding-top:10px;
}

.dp-popup .product-container:not(first){
    border-top: 1px #eee solid;
}

.dp-popup .product-container select{
    width:100%;
    margin-bottom:10px;
    font-size:12px;
}

.dp-popup .product-container .variant-wrapper{
    float:none;
}

.dp-popup .no-thanks{
    text-align: center;
    width: 100%;
}

.dp-popup-dpModal .lb-image{
    width:100%;
}

.dp-popup-dpModal .upsell-total{
    width:100%;
    padding-top:10px;
}

.dp-popup-dpModal button.add-upsells{
    color: #fff;;
    font-size: 100%;
    background-color: #a1c65b;;
    display: inline-block;
    padding: 8px 12px;
    margin-bottom: 0;
    font-size: 14px;
    line-height: 1.42857143;
    text-align: center;
    vertical-align: middle;
    letter-spacing: 1px;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 3px;
    font-weight:500;
    width:100%;
}

.lb-price .lb-regular{
    width:100%;
}

.product-price .lb-sale{
    display:block;
    width:100%;
    font-weight:normal;
}

.discount-applies-true .lb-price .lb-sale{
    text-decoration: line-through;
    width:100%;
}

.discount-applies-true .product-price .lb-sale{
    display:block;
    width:100%;
    color:#8C0000;
    font-weight:normal;
    opacity:0.8;
}


.discount-applies-true .product-price .upsell-note{
    display:block;
    font-weight:normal;
    width:100%;
}

.discount-applies-false .product-price .upsell-note{
    display:none;
}

  #dpModal-container .product-title{
    font-weight:400;
    width:100%;
}

.dp-popup-dpModal .upsell-title{
    font-family: inherit;
    font-weight: normal;
    line-height: 1.1;
    color: inherit;
    font-size: 18px;
    margin-top: 10px;
    margin-bottom: 20px;
    font-weight: 500;
}


  @media (max-width: 650px) {
.dp-popup .multiple-products-true .details {
        width: 60%;
        padding-left: 20px;
    }

.dp-popup .multiple-products-true .actions {
        max-width: inherit;
        width: 100%;
        padding-top: 15px;
    }

.dp-popup .product-container{
        padding-bottom: 20px;
        padding-top:20px;
    }


}

.lb-bundle-wrapper .lb-product-bundle button.add-lion-bundle{
    cursor: pointer;
    background-color: #a1c65b;
    background-image: none;
    color: #fff;
    margin: -10px 0 -10px;
    width: 100%;
    border: none;
    text-decoration: none;
    font-size: 13px;
    font-family: inherit;
    text-transform: uppercase;
    font-weight: 500;
    padding: 10px;
}

.lb-product-bundle .bundle-total button span {
    font-size: 13px;
}


table.lb-discount-table{


}




</style>


<script type="text/javascript">
window.lion = {};

{% if customer %}
window.lion.customer_tags = "{{customer.tags | join: ','}}";
{% endif %}

{% if template contains 'product' %}
{% unless product.id == blank %}
{% assign ba_page = 'product' %}
window.lion.product = {
    id: {{product.id}},
price: {{product.price}},
};
window.lion.product_collections = []
{% for c in product.collections %}
window.lion.product_collections.push({{c.id}})
{% endfor %}
{% endunless %}
{% endif %}


{% if template contains 'cart' %}
{% assign ba_page = 'cart' %}
{% endif %}

{% if cart %}
window.lion.cart = {{cart | json}}
window.lion.cart.items = []

{% for item in cart.items %}
var lion_item = {{item | json}};
lion_item["collection_ids"] = [];
{% for c in item.product.collections %}
lion_item["collection_ids"].push({{c.id}})
{% endfor %}
window.lion.cart.items.push(lion_item);
{% endfor %}

if (typeof window.lion.cart.items == "object") {
    for (var i=0; i<window.lion.cart.items.length; i++) {
        ["sku", "grams", "vendor", "url", "image", "handle", "requires_shipping", "product_type", "product_description"].map(function(a) {
            delete window.lion.cart.items[i][a]
        })
    }
}
{% endif %}
window.lion.ba_page = "{{ba_page}}";
window.lion.discount_method = "code";


{% raw %}
window.lion.money_format = "Rs.{{amount}}";
{% endraw %}



//Handlebars

</script>

{% raw  %}
<script id="lb-discount-tiers" type="text/x-handlebars-template">
    <div class="lb-discount-tiers">
    <h4>{{{product_message}}}</h4>

<table class="lb-discount-table">
    <thead>
    <tr>
    <th>Minimum Qty</th>
<th>
Discount
</th>
</tr>
</thead>
<tbody>
{{#vol_rows}}
<tr>
<td>{{{quantity}}}+</td>
<td>{{{price.title}}}</td>
</tr>
{{/vol_rows}}
</tbody>
</table>
</div>

</script>

<script id="lb-bundle" type="text/x-handlebars-template">

    <div id="{{css_length}}-product" class="discount-applies-{{discount_applies}} lb-product-bundle with-total">
    <div class="bundle-title">{{bundle_note}}</div>
    {{#products}}
<div class="lb-product-wrapper" data-product-id="{{id}}" data-variant-id="{{variants.0.id}}" data-quantity="{{quantity}}">
        <div class='lb-image-container'>
        <div class="lb-image">
        <a href="/products/{{handle}}">
        <img src="{{{image.src}}}"/>
        </a>
        </div>
        </div>
        <div class="lb-info-wrapper">
        <a href="/products/{{handle}}">
        <div class="bundle-name">{{title}} x {{quantity}}</div>
    </a>
    <div class="lion-variants-container" style="{{{variantsStyle}}}">
        {{{variantsSelect}}}
</div>

    <div class="lb-price">

        <span class="lb-sale ">{{{variants.0.price}}}</span>
    </div>

    </div>
    </div>

    {{#unless @last}}
<div class="bundle-plus">
        <img src="{{{../ba_plus_url}}}"/>
        </div>
    {{/unless}}

        {{/products}}


        <div class="bundle-total show-savings">


            <div class="lb-eqs">
            <img src="{{{ba_eqs_url}}}"/>
            </div>


            <button class="add-lion-bundle">
            <span class='top-button'>Add Bundle</span>
            {{#if discount_applies}}
        <span class='bottom-button'>{{{save_text}}}</span>
            {{/if}}
            </button>
            </div>


            </div>

            </script>


            <script id="lb-upsell" type="text/x-handlebars-template">
                <div id='dpModal-container' style='display:none;' class='dp-popup dp-wrap dp-whModal'>
                <div id="{{css_length}}-product" class="multiple-products-{{multiple_products}} lb-product-bundle">
                <h3 class="upsell-title">{{upsell_note}}</h3>
                {{#products}}
            <div class="product-container discount-applies-{{discount_applies}}" data-product-id="{{id}}" data-variant-id="{{variants.0.id}}" data-quantity="{{qty_left}}">
                    <div class="image">
                    <a href="/products/{{handle}}" target="_blank">
                    <img src="{{{image.src}}}"/>
                    </a>
                    </div>
                    <div class="details multiple-variants">
                    <div class="product-title">
                    <a href="/products/{{handle}}" target="_blank">
                    {{title}} x {{qty_left}}
            </a>
                </div>
                <div class="product-price">
                    <span class="lb-sale">{{{variants.0.price}}}</span>
                <span class="upsell-note">{{upsell_note}}</span>
                </div>
                </div>

                <div class="actions">
                    {{{variantsSelect}}}
            <button class="add-upsells">Add to cart</button>
                </div>



                </div>
                {{/products}}
                </div>
                <div class="no-thanks">
                    <a href="#" data-offer-id="{{offer_id}}">No thanks</a>
                </div>
                </div>
                </script>

                    {% endraw %}


                <script type="text/javascript">
                        (function(window, document) {"use strict";

                            function setCookie(cname, cvalue, exdays) {
                                var d = new Date();
                                d.setTime(d.getTime() + (exdays*24*60*60*1000));
                                var expires = "expires="+ d.toUTCString();
                                document.cookie = cname + "=" + cvalue + "; " + expires + "; path=/;";
                            }

                            function getCookie(cname) {
                                var name = cname + "=";
                                var ca = document.cookie.split(';');
                                for(var i = 0; i <ca.length; i++) {
                                    var c = ca[i];
                                    while (c.charAt(0)==' ') {
                                        c = c.substring(1);
                                    }
                                    if (c.indexOf(name) == 0) {
                                        return c.substring(name.length,c.length);
                                    }
                                }
                                return "";
                            }

                            function reqJquery(onload) {
                                if(typeof jQuery === 'undefined' || (parseInt(jQuery.fn.jquery) === 1 && parseFloat(jQuery.fn.jquery.replace(/^1\./,'')) < 10)){
                                    var head = document.getElementsByTagName('head')[0];
                                    var script = document.createElement('script');
                                    script.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js';;
                                    script.type = 'text/javascript';
                                    script.onload = script.onreadystatechange = function() {
                                        if (script.readyState) {
                                            if (script.readyState === 'complete' || script.readyState === 'loaded') {
                                                script.onreadystatechange = null;
                                                onload(jQuery.noConflict(true));
                                            }
                                        }
                                        else {
                                            onload(jQuery.noConflict(true));
                                        }
                                    };
                                    head.appendChild(script);
                                }else {
                                    onload(jQuery);
                                }
                            }

                            reqJquery(function($){

                                var dpQuery=$,dpModals=[],getCurrent=function(){return dpModals.length?dpModals[dpModals.length-1]:null},selectCurrent=function(){var a,b=!1;for(a=dpModals.length-1;a>=0;a--)dpModals[a].$blocker&&(dpModals[a].$blocker.toggleClass("current",!b).toggleClass("behind",b),b=!0)};dpQuery.dpModal=function(a,b){var c,d;if(this.$body=dpQuery("body"),this.options=dpQuery.extend({},dpQuery.dpModal.defaults,b),this.options.doFade=!isNaN(parseInt(this.options.fadeDuration,10)),this.$blocker=null,this.options.closeExisting)for(;dpQuery.dpModal.isActive();)dpQuery.dpModal.close();if(dpModals.push(this),a.is("a"))if(d=a.attr("href"),/^#/.test(d)){if(this.$elm=dpQuery(d),1!==this.$elm.length)return null;this.$body.append(this.$elm),this.open()}else this.$elm=dpQuery("<div>"),this.$body.append(this.$elm),c=function(a,b){b.elm.remove()},this.showSpinner(),a.trigger(dpQuery.dpModal.AJAX_SEND),dpQuery.get(d).done(function(b){if(dpQuery.dpModal.isActive()){a.trigger(dpQuery.dpModal.AJAX_SUCCESS);var d=getCurrent();d.$elm.empty().append(b).on(dpQuery.dpModal.CLOSE,c),d.hideSpinner(),d.open(),a.trigger(dpQuery.dpModal.AJAX_COMPLETE)}}).fail(function(){a.trigger(dpQuery.dpModal.AJAX_FAIL);var b=getCurrent();b.hideSpinner(),dpModals.pop(),a.trigger(dpQuery.dpModal.AJAX_COMPLETE)});else this.$elm=a,this.$body.append(this.$elm),this.open()},dpQuery.dpModal.prototype={constructor:dpQuery.dpModal,open:function(){var a=this;this.block(),this.options.doFade?setTimeout(function(){a.show()},this.options.fadeDuration*this.options.fadeDelay):this.show(),dpQuery(document).off("keydown.dpModal").on("keydown.dpModal",function(a){var b=getCurrent();27==a.which&&b.options.escapeClose&&b.close()}),this.options.clickClose&&this.$blocker.click(function(a){a.target==this&&dpQuery.dpModal.close()})},close:function(){dpModals.pop(),this.unblock(),this.hide(),dpQuery.dpModal.isActive()||dpQuery(document).off("keydown.dpModal")},block:function(){this.$elm.trigger(dpQuery.dpModal.BEFORE_BLOCK,[this._ctx()]),this.$body.css("overflow","hidden"),this.$blocker=dpQuery('<div class="jquery-dpModal blocker current"></div>').appendTo(this.$body),selectCurrent(),this.options.doFade&&this.$blocker.css("opacity",0).animate({opacity:1},this.options.fadeDuration),this.$elm.trigger(dpQuery.dpModal.BLOCK,[this._ctx()])},unblock:function(a){!a&&this.options.doFade?this.$blocker.fadeOut(this.options.fadeDuration,this.unblock.bind(this,!0)):(this.$blocker.children().appendTo(this.$body),this.$blocker.remove(),this.$blocker=null,selectCurrent(),dpQuery.dpModal.isActive()||this.$body.css("overflow",""))},show:function(){this.$elm.trigger(dpQuery.dpModal.BEFORE_OPEN,[this._ctx()]),this.options.showClose&&(this.closeButton=dpQuery('<a href="#close-dpModal" rel="dpModal:close" class="close-dpModal '+this.options.closeClass+'">'+this.options.closeText+"</a>"),this.$elm.append(this.closeButton)),this.$elm.addClass(this.options.dpModalClass).appendTo(this.$blocker),this.options.doFade?this.$elm.css("opacity",0).show().animate({opacity:1},this.options.fadeDuration):this.$elm.show(),this.$elm.trigger(dpQuery.dpModal.OPEN,[this._ctx()])},hide:function(){this.$elm.trigger(dpQuery.dpModal.BEFORE_CLOSE,[this._ctx()]),this.closeButton&&this.closeButton.remove();var a=this;this.options.doFade?this.$elm.fadeOut(this.options.fadeDuration,function(){a.$elm.trigger(dpQuery.dpModal.AFTER_CLOSE,[a._ctx()])}):this.$elm.hide(0,function(){a.$elm.trigger(dpQuery.dpModal.AFTER_CLOSE,[a._ctx()])}),this.$elm.trigger(dpQuery.dpModal.CLOSE,[this._ctx()])},showSpinner:function(){this.options.showSpinner&&(this.spinner=this.spinner||dpQuery('<div class="'+this.options.dpModalClass+'-spinner"></div>').append(this.options.spinnerHtml),this.$body.append(this.spinner),this.spinner.show())},hideSpinner:function(){this.spinner&&this.spinner.remove()},_ctx:function(){return{elm:this.$elm,$blocker:this.$blocker,options:this.options}}},dpQuery.dpModal.close=function(a){if(dpQuery.dpModal.isActive()){a&&a.preventDefault();var b=getCurrent();return b.close(),b.$elm}},dpQuery.dpModal.isActive=function(){return dpModals.length>0},dpQuery.dpModal.defaults={closeExisting:!0,escapeClose:!0,clickClose:!0,closeText:"Close",closeClass:"",dpModalClass:"dp-popup-dpModal",spinnerHtml:null,showSpinner:!0,showClose:!0,fadeDuration:null,fadeDelay:1},dpQuery.dpModal.BEFORE_BLOCK="dpModal:before-block",dpQuery.dpModal.BLOCK="dpModal:block",dpQuery.dpModal.BEFORE_OPEN="dpModal:before-open",dpQuery.dpModal.OPEN="dpModal:open",dpQuery.dpModal.BEFORE_CLOSE="dpModal:before-close",dpQuery.dpModal.CLOSE="dpModal:close",dpQuery.dpModal.AFTER_CLOSE="dpModal:after-close",dpQuery.dpModal.AJAX_SEND="dpModal:ajax:send",dpQuery.dpModal.AJAX_SUCCESS="dpModal:ajax:success",dpQuery.dpModal.AJAX_FAIL="dpModal:ajax:fail",dpQuery.dpModal.AJAX_COMPLETE="dpModal:ajax:complete",dpQuery.fn.dpModal=function(a){return 1===this.length&&new dpQuery.dpModal(this,a),this},dpQuery(document).on("click.dpModal",'a[rel="dpModal:close"]',dpQuery.dpModal.close),dpQuery(document).on("click.dpModal",'a[rel="dpModal:open"]',function(a){a.preventDefault(),dpQuery(this).dpModal()});

                                function reloadCurrency() {
                                    if (typeof Currency == "object" && typeof Currency.moneyFormats == "object" && typeof mlvedaload == "function") {
                                        mlvedaload()
                                    }
                                }

                                var lionCookieCode = getCookie('lion_discount_' + window.lion.cart.token);
                                if(lionCookieCode){
                                    window.lion.discount_code = lionCookieCode;
                                }

                                function baDisplayCents(cents) {
                                    if (typeof cents == "undefined" || cents == null){return ""}
                                    if (typeof cents == "string" && cents.length == 0){return ""}

                                    var format = window.lion.money_format;
                                    var moneyRegex = /\{\{\s*(\w+)\s*\}\}/;
                                    if (typeof cents == "string") {
                                        cents = cents.replace(".", "")
                                    }

                                    function defOpt(opt,def){return typeof opt == "undefined" ? def : opt}

                                    function displayDelims(n,p,t,d){
                                        p = defOpt(p, 2);
                                        t = defOpt(t, ",");
                                        d = defOpt(d, ".");
                                        if (isNaN(n) || n == null){
                                            return 0
                                        }
                                        n = (n / 100).toFixed(p);
                                        var parts = n.split("."),dollars = parts[0].replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1" + t),cents = parts[1] ? d + parts[1] : "";
                                        return dollars + cents
                                    }

                                    var val = "";
                                    switch (format.match(moneyRegex)[1]) {
                                        case "amount":
                                            val = displayDelims(cents, 2);
                                            break;
                                        case "amount_no_decimals":
                                            val = displayDelims(cents, 0);
                                            break;
                                        case "amount_no_decimals_with_comma_separator":
                                            val = displayDelims(cents, 0, ".", ",");
                                            break
                                        case "amount_with_comma_separator":
                                            val = displayDelims(cents, 2, ".", ",");
                                            break;
                                    }
                                    return format.replace(moneyRegex, val)
                                }



                                function showLionBundle(ba_data){
                                    var arr = [];
                                    var products = [];
                                    ba_data.ba_bundle.items.map(function(p){
                                        arr.push($.getJSON("/products/" + p.handle + ".json", function(data) {
                                            var prod = data.product;
                                            prod.variantsStyle = "";
                                            prod.variantsSelect = "";
                                            for (var i = 0; i < prod.variants.length; i++) {
                                                if (typeof prod.variants[i].price == "string") {
                                                    prod.variants[i].price = parseFloat(prod.variants[i].price) * 100
                                                }
                                                if (typeof prod.variants[i].compare_at_price == "string") {
                                                    prod.variants[i].compare_at_price = parseFloat(prod.variants[i].compare_at_price) * 100
                                                }
                                                prod.variants[i].og_compare_at_price = prod.variants[i].compare_at_price;
                                                prod.variants[i].og_price = prod.variants[i].price;
                                                if (prod.variants[i].compare_at_price && prod.variants[i].compare_at_price > prod.variants[i].price) {
                                                    prod.variants[i].compare_at_price = prod.variants[i].compare_at_price
                                                } else {
                                                    prod.variants[i].compare_at_price = ""
                                                }
                                                prod.variants[i].price = baDisplayCents(prod.variants[i].price);
                                            }
                                            if (prod.variants.length == 1) {
                                                prod.variantsStyle = "display: none;";
                                            }else {
                                                var variantsHtml = "";
                                                prod.variants.map(function(variant){
                                                    if (typeof variant.inventory_management == "string" && variant.inventory_management == "shopify") {
                                                        if (typeof variant.inventory_policy == "string" && variant.inventory_policy == "deny" && typeof variant.inventory_quantity == "number" && variant.inventory_quantity <= 0) {
                                                            return
                                                        }
                                                    }
                                                    var baImg = "";
                                                    if (variant.image_id != null && typeof variant.image_id == "number") {
                                                        var varImgs = prod.images.filter(function(pi) {return pi.id == variant.image_id});
                                                        if (varImgs.length){baImg = varImgs[0].src}
                                                    } else {
                                                        if (typeof prod.image == "object" && prod.image && typeof prod.image.src == "string") {
                                                            baImg = prod.image.src
                                                        }
                                                    }

                                                    var n = baImg.lastIndexOf(".");
                                                    if (n >= 0) {
                                                        var s = baImg.substring(0, n) + "_medium." + baImg.substring(n + 1);
                                                        baImg = s
                                                    }
                                                    variantsHtml += "<option value='" + variant.id + "' data-img='" + baImg + "' data-price='" + variant.og_price + "' data-compare-at-price='" + variant.og_compare_at_price + "'>";
                                                    variantsHtml += variant.title + "</option>"
                                                });
                                                prod.variantsSelect = "<select class='lb-variants'>";
                                                prod.variantsSelect += variantsHtml + "</select>"
                                            }

                                            if (typeof prod.image == "object" && prod.image && typeof prod.image.src == "string") {
                                                var n = prod.image.src.lastIndexOf(".");
                                                if (n >= 0){
                                                    var s = prod.image.src.substring(0, n) + "_medium." + prod.image.src.substring(n + 1);
                                                    prod.image.src = s
                                                }
                                            } else {
                                                prod.image = {}
                                                prod.image.src = 'https://cl.ly/003c0X1M380p/Image%202018-03-05%20at%201.15.15%20PM.png'
                                            }
                                            products.push(prod)
                                        }).fail(function(){
                                            console.log('fail');
                                        }))
                                    });
                                    if (arr.length == 0) {
                                        return
                                    }
                                    $.when.apply($, arr).done(function() {
                                        if (products.length == 0) {
                                            return
                                        }
                                        var noVariants = true;
                                        for (var i = 0; i < products.length; i++) {
                                            if (products[i].variants.length > 1) {
                                                noVariants = false
                                            }
                                            products[i].quantity = 1;
                                            var curProd = ba_data.ba_bundle.items.filter(function(p) {
                                                return p.id == products[i].id
                                            });
                                            if (curProd.length == 1 && curProd[0].qty > 1) {
                                                products[i].quantity = curProd[0].qty
                                            }
                                        }
                                        if (noVariants){
                                            for (var i = 0; i < products.length; i++) {
                                                products[i].variantsStyle = "height: 0px;"
                                            }
                                        }
                                        switch (products.length) {
                                            case 2:
                                                var cssLength = 'two'
                                                break;
                                            case 3:
                                                var cssLength = 'three'
                                                break;
                                            case 4:
                                                var cssLength = 'four'
                                                break;
                                            default:
                                                var cssLength = 'two'
                                        }

                                        var source = $("#lb-bundle").html();
                                        var context = {
                                            save_text: ba_data.save_text,
                                            discount_applies: ba_data.discount_applies,
                                            ba_bundle: ba_data.ba_bundle,
                                            bundle_note: ba_data.bundle_note,
                                            products: products,
                                            css_length: cssLength,
                                            ba_plus_url: "{{ 'lb-plus.png' | asset_img_url: '38x' }}",
                                            ba_eqs_url: "{{ 'lb-eqs.png' | asset_img_url: '38x' }}"
                                        };
                                        var template = Handlebars.compile(source);
                                        var html = template(context);
                                        $('div.lb-bundle-wrapper').html(html);
                                    })
                                }



                                function showBaUpsell(ba_data){
                                    var arr = [];
                                    var products = [];
                                    var offerId = ba_data.discounts.upsell_arr[0].offer_id;
                                    var upsellNote = ba_data.discounts.upsell_arr[0].upsell_note;
                                    var discount_applies = ba_data.discounts.upsell_arr[0].discount_applies;
                                    ba_data.discounts.upsell_arr.map(function(p){
                                        arr.push($.getJSON("/products/" + p.handle + ".json", function(data) {
                                            var prod = data.product;
                                            prod.offer_id = offerId;
                                            prod.upsell_note = upsellNote;
                                            prod.discount_applies = discount_applies;
                                            prod.variantsStyle = "";
                                            prod.variantsSelect = "";
                                            for (var i = 0; i < prod.variants.length; i++) {
                                                if (typeof prod.variants[i].price == "string") {
                                                    prod.variants[i].price = parseFloat(prod.variants[i].price) * 100
                                                }
                                                if (typeof prod.variants[i].compare_at_price == "string") {
                                                    prod.variants[i].compare_at_price = parseFloat(prod.variants[i].compare_at_price) * 100
                                                }
                                                prod.variants[i].og_compare_at_price = prod.variants[i].compare_at_price;
                                                prod.variants[i].og_price = prod.variants[i].price;
                                                if (prod.variants[i].compare_at_price && prod.variants[i].compare_at_price > prod.variants[i].price) {
                                                    prod.variants[i].compare_at_price = prod.variants[i].compare_at_price
                                                } else {
                                                    prod.variants[i].compare_at_price = ""
                                                }
                                                prod.variants[i].price = baDisplayCents(prod.variants[i].price);
                                            }
                                            if (prod.variants.length == 1) {
                                                prod.variantsStyle = "display: none;";
                                            }else {
                                                var variantsHtml = "";
                                                prod.variants.map(function(variant){
                                                    if (typeof variant.inventory_management == "string" && variant.inventory_management == "shopify") {
                                                        if (typeof variant.inventory_policy == "string" && variant.inventory_policy == "deny" && typeof variant.inventory_quantity == "number" && variant.inventory_quantity <= 0) {
                                                            return
                                                        }
                                                    }
                                                    var baImg = "";
                                                    if (variant.image_id != null && typeof variant.image_id == "number") {
                                                        var varImgs = prod.images.filter(function(pi) {return pi.id == variant.image_id});
                                                        if (varImgs.length){baImg = varImgs[0].src}
                                                    } else {
                                                        if (typeof prod.image == "object" && prod.image && typeof prod.image.src == "string") {
                                                            baImg = prod.image.src
                                                        }
                                                    }

                                                    var n = baImg.lastIndexOf(".");
                                                    if (n >= 0) {
                                                        var s = baImg.substring(0, n) + "_medium." + baImg.substring(n + 1);
                                                        baImg = s
                                                    }
                                                    variantsHtml += "<option value='" + variant.id + "' data-img='" + baImg + "' data-price='" + variant.og_price + "' data-compare-at-price='" + variant.og_compare_at_price + "'>";
                                                    variantsHtml += variant.title + "</option>"
                                                });
                                                prod.variantsSelect = "<select class='lb-variants'>";
                                                prod.variantsSelect += variantsHtml + "</select>"
                                            }

                                            if (typeof prod.image == "object" && prod.image && typeof prod.image.src == "string") {
                                                var n = prod.image.src.lastIndexOf(".");
                                                if (n >= 0){
                                                    var s = prod.image.src.substring(0, n) + "_medium." + prod.image.src.substring(n + 1);
                                                    prod.image.src = s
                                                }
                                            }
                                            products.push(prod)
                                        }).fail(function(){
                                            console.log('fail');
                                        }))
                                    });
                                    if (arr.length == 0) {
                                        return
                                    }
                                    $.when.apply($, arr).done(function() {
                                        if (products.length == 0) {
                                            return
                                        }
                                        var noVariants = true;
                                        for (var i = 0; i < products.length; i++) {
                                            var offerId = products[i].offer_id;
                                            var upsellNote = products[i].upsell_note;
                                            var discount_applies = products[i].discount_applies;
                                            if (products[i].variants.length > 1) {
                                                noVariants = false
                                            }
                                            products[i].quantity = 1;
                                            var curProd = ba_data.discounts.upsell_arr.filter(function(p) {
                                                return p.id == products[i].id
                                            });
                                            if (curProd.length == 1 && curProd[0].qty > 1) {
                                                products[i].quantity = curProd[0].qty
                                            }
                                            if (curProd.length == 1 && curProd[0].qty_left) {
                                                products[i].qty_left = curProd[0].qty_left
                                            }
                                        }
                                        if (noVariants){
                                            for (var i = 0; i < products.length; i++) {
                                                products[i].variantsStyle = "height: 0px;"
                                            }
                                        }
                                        switch (products.length) {
                                            case 1:
                                                var cssLength = 'one'
                                                break;
                                            case 2:
                                                var cssLength = 'two'
                                                break;
                                            case 3:
                                                var cssLength = 'three'
                                                break;
                                            case 4:
                                                var cssLength = 'four'
                                                break;
                                            default:
                                                var cssLength = 'two'
                                        }


                                        var source = $("#lb-upsell").html();
                                        var multiple_products = products.length > 1;
                                        var context = {
                                            multiple_products: multiple_products,
                                            discount_applies: discount_applies,
                                            upsell_note: upsellNote,
                                            discount_applies: discount_applies,
                                            products: products,
                                            css_length: cssLength,
                                            ba_plus_url: "{{ 'lb-plus.png' | asset_img_url: '38x' }}",
                                            offer_id: offerId
                                        };
                                        var template = Handlebars.compile(source);
                                        var html = template(context);
                                        $('body').append(html);
                                        $('#dpModal-container').dpModal();
                                    });
                                }

                                function showVolDiscounts(data){
                                    var source = $("#lb-discount-tiers").html();
                                    var context = {
                                        product_message: "Buy at discounted prices",
                                        vol_rows: data.vol_rows
                                    };
                                    var template = Handlebars.compile(source);
                                    var html = template(context);
                                    $('div.lb-vol-wrapper').html(html);

                                }

                                function baDelegate(data) {
                                    if(data.type == 'vd'){

                                        var cartForm = $("form[action*='/cart/add']").first();
                                        if($('.lb-vol-wrapper').length == 0){
                                            cartForm.after("<div class='lb-vol-wrapper'></div>");
                                        }
                                        showVolDiscounts(data)

                                    }

                                    if(data.type == 'bundle'){
                                        var cartForm = $("form[action*='/cart/add']").first();
                                        if($('.lb-bundle-wrapper').length == 0){
                                            cartForm.after("<div class='lb-bundle-wrapper'></div>");
                                        }
                                        showLionBundle(data)
                                    }

                                    if (typeof data.discounts == "object" && typeof data.discounts.upsell_arr == "object"){
                                        if(data.discounts.upsell_arr.length > 0){
                                            showBaUpsell(data)
                                        }
                                    }

                                    if (typeof data.discounts == "object" && typeof data.notifications == "object"){
                                        if(data.notifications.length > 0 && data.discounts.upsell_arr.length == 0){
                                            showLionNotification(data.notifications)
                                        }
                                    }


                                    if (typeof data.discounts == "object" && typeof data.discounts.cart == "object" && typeof data.discounts.cart.items == "object") {
                                        showCartDiscounts(data.discounts)
                                    }
                                    reloadCurrency();
                                }

                                function showCartDiscounts(discounts) {
                                    window.lion.discounts = discounts;

                                    for (var i = 0; i < discounts.cart.items.length; i++) {
                                        var item = discounts.cart.items[i];

                                        if (item.discounted_price < item.original_price) {
                                            $(".lion-cart-item-price[data-key='" + item.key + "']").html("<span class='original_price'>" + item.original_price_format + "</span>" + "<span class='discounted_price'>" + item.discounted_price_format + "</span>");
                                            $(".lion-cart-item-line-price[data-key='" + item.key + "']").html("<span class='original_price'>" + item.original_line_price_format + "</span>" + "<span class='discounted_price'>" + item.discounted_line_price_format + "</span>")
                                        }

                                        $(".lion-cart-item-upsell-notes[data-key='" + item.key + "']").html(item.upsell_note);
                                        $(".lion-cart-item-success-notes[data-key='" + item.key + "']").html(item.success_note);
                                    };

                                    if (typeof discounts.discounted_price_html != "string") {
                                        return
                                    }

                                    $("span#bk-cart-subtotal-price").attr( "id", "");

                                    $(".lion-original-cart-total").css("text-decoration", "line-through");
                                    $(".lion-cart-total").html("<span class=''>" + discounts.discounted_price_html + "</span>");

                                    $('.subtotal .cart_savings.sale').hide();



                                    $(".lion-cart-total").prepend("<span class='lion-messages'><div id='lion-discount-item'></div><div id='lion-summary-item'></div></span>");


                                    if(discounts.summary_item_html){
                                        $('#lion-summary-item').html(discounts.summary_item_html);
                                    }

                                    var checkout_selectors = ["input[name='checkout']", "button[name='checkout']", "[href$='checkout']", "input[name='goto_pp']", "button[name='goto_pp']", "input[name='goto_gc']", "button[name='goto_gc']", ".additional-checkout-button", ".google-wallet-button-holder", ".amazon-payments-pay-button"];
                                    checkout_selectors.forEach(function(selector) {
                                        var els = document.querySelectorAll(selector);
                                        if (typeof els == "object" && els) {
                                            for (var i = 0; i < els.length; i++) {
                                                var el = els[i];
                                                if (typeof el.addEventListener != "function") {
                                                    return
                                                }
                                                el.addEventListener("click", function(ev) {
                                                    ev.preventDefault();
                                                    DiscountedPricingCheckout();
                                                }, false);

                                            }
                                        }
                                    })
                                }

                                function showLionNotification(notifications){
                                    if ((getCookie('lion_notifications_closed') != 1) && notifications != ""){
                                        var barElements = ["main", "div.content", "section.main-content"];
                                        var barHtml = "<div id='lion-notification-bar'>" + notifications + "<div id='lion-close-notification'>X</div></div>";
                                        barElements.forEach(function(el){
                                            if ($('#lion-notification-bar').length == 0){
                                                if (el == 'main'){
                                                    $(el).prepend(barHtml);
                                                } else {
                                                    $(el).before(barHtml);
                                                }
                                            }
                                        });
                                        if ($('#lion-notification-bar').length > 0){
                                            $('#lion-notification-bar').slideDown('slow');
                                        }
                                    }

                                }

                                function addLionBundle(e){
                                    e.preventDefault();
                                    var lionBundleItems = [];

                                    $(".lb-product-wrapper").each(function() {
                                        var i = {id: $(this).data("variant-id"),quantity: $(this).data("quantity")};
                                        lionBundleItems.push(i);
                                    });

                                    addLionItems(lionBundleItems, function(){
                                        setTimeout(function() {window.location.href = "/cart"}, 200)
                                    });

                                }

                                function addLionUpsells(target){
                                    var btn = $(target);
                                    var lionUpsellItems = [];

                                    var container = btn.parents(".product-container");
                                    var i = {id: container.data("variant-id"),quantity: container.data("quantity")};
                                    lionUpsellItems.push(i);


                                    addLionItems(lionUpsellItems, function(){
                                        setTimeout(function() {window.location.href = "/cart"}, 200)
                                    });
                                }


                                function addLionItems(variants, callback) {
                                    if (variants.length) {
                                        var i = variants.shift();
                                        $.ajax({
                                            url: "/cart/add.js",
                                            type: "POST",
                                            dataType: "json",
                                            data: i,
                                            success: function(data) {
                                                addLionItems(variants, callback)
                                            },
                                            error: function(data) {
                                                if (typeof data == "object" && typeof data.responseJSON == "object" && typeof data.responseJSON.description == "string") {
                                                    alert(data.responseJSON.description)
                                                }
                                                if (typeof res == "string") {
                                                    alert(data)
                                                }
                                            }
                                        })
                                    } else {
                                        if (typeof callback == "function") {
                                            return callback()
                                        }
                                        setTimeout(function() {
                                            window.location.reload()
                                        }, 100)
                                    }
                                }


                                function DiscountedPricingCheckout(){
                                    if ($("input[type='checkbox']#agree").length > 0 && $("input[type='checkbox']#agree:checked").length != jQuery("input[type='checkbox']#agree").length) {
                                        return
                                    }

                                    for (var i = 0; i < window.lion.cart.items.length; i++) {
                                        var item = window.lion.cart.items[i];
                                        var el = document.querySelectorAll("[id='updates_" + item.key + "']");
                                        if (el.length != 1) {
                                            el = document.querySelectorAll("[id='updates_" + item.variant_id + "']")
                                        }
                                        if (el.length == 1) {
                                            window.lion.cart.items[i].quantity = el[0].value
                                        }
                                    }

                                    window.lion.action_type = 'checkout';

                                    var invoiceUrlParams = [];

                                    var noteAttributes = [];
                                    $("[name^='attributes']").each(function() {
                                        var attribute = $(this);
                                        var name = $(this).attr("name");
                                        name = name.replace(/^attributes\[/i, "").replace(/\]$/i, "");
                                        var v = {
                                            name: name,
                                            value: attribute.val()
                                        };
                                        if (v.value == "") {
                                            return
                                        }
                                        switch (attribute[0].tagName.toLowerCase()) {
                                            case "input":
                                                if (attribute.attr("type") == "checkbox") {
                                                    if (attribute.is(":checked")) {
                                                        noteAttributes.push(v)
                                                    }
                                                } else {
                                                    noteAttributes.push(v)
                                                }
                                                break;
                                            default:
                                                noteAttributes.push(v)
                                        }
                                    });

                                    var orderNote = "";
                                    if ($("[name='note']").length) {
                                        orderNote = $("[name='note']")[0].value
                                    }
                                    window.lion.cart.note_attributes = noteAttributes;
                                    window.lion.cart.note = orderNote;

                                    if (orderNote.length){
                                        invoiceUrlParams.push("note=" + encodeURIComponent(orderNote))
                                    }

                                    if (noteAttributes.length){
                                        noteAttributes.map(function(a) {
                                            invoiceUrlParams.push("attributes" + encodeURIComponent("[" + a.name + "]") + "=" + encodeURIComponent(a.value))
                                        })
                                    }

                                    if(window.lion.discount_method == 'code'){
                                        $.ajax({
                                            cache: false,
                                            contentType: "application/json; charset=utf-8",
                                            dataType: "json",
                                            type: "POST",

                                            url: "/apps/lion_pricing",

                                            data: JSON.stringify(window.lion),
                                            success: function(res) {
                                                res.invoice_url = '/checkout';
                                                if (res.discount_code) {
                                                    invoiceUrlParams.push("discount=" + res.discount_code);
                                                }
                                                if (invoiceUrlParams.length) {
                                                    res.invoice_url += "?" + invoiceUrlParams.join("&")
                                                }
                                                window.location.href = res.invoice_url
                                            }
                                        })


                                    } else {

                                        if (typeof window.gaclientId) {
                                            invoiceUrlParams.push("clientId=" + window.gaclientId);
                                            invoiceUrlParams.push("_ga=" + window.gaclientId)
                                        }

                                        $.ajax({
                                            cache: false,
                                            contentType: "application/json; charset=utf-8",
                                            dataType: "json",
                                            type: "POST",

                                            url: "/apps/lion_pricing",

                                            data: JSON.stringify(window.lion),
                                            success: function(res) {
                                                if(res.invoice_url){
                                                    if (invoiceUrlParams.length) {
                                                        res.invoice_url += "?" + invoiceUrlParams.join("&")
                                                    }
                                                    setTimeout(function(){
                                                        window.location.href = res.invoice_url
                                                    }, 500);
                                                } else {
                                                    window.location.href = "/checkout"
                                                }
                                            }
                                        })

                                    }


                                }


                                if(window.lion.ba_page.length > 0){
                                    $.ajax({
                                        cache: false,
                                        type: "POST",

                                        url: "/apps/lion_pricing",

                                        data: JSON.stringify(window.lion),
                                        dataType: 'json',
                                        contentType: "application/json; charset=utf-8",
                                        success: function(data) {
                                            baDelegate(data)
                                        }
                                    });
                                }

                                setTimeout(function(){
                                    if (typeof ga == "function"){ga(function(tracker) {window.gaclientId = tracker.get("clientId")})}
                                }, 1000);

                                $(document).on('change', "input.lion-quantity, input[name='updates[]'], input[id^='updates_'], input[id^='Updates_']", function(e) {
                                    e.preventDefault();
                                    $("form[action$='cart']").submit();
                                });

                                if(window.lion.ba_page == 'cart'){
                                    $(document).ajaxSuccess(function(evt,xhr,attrs) {
                                        if (attrs.url == '/cart/change.js'){
                                            $("form[action$='cart']").submit();
                                        }
                                    });
                                }

                                $(document).on('change', 'select.lb-variants', function(){
                                    var option = $(this).find(":selected");
                                    var price  = baDisplayCents(option.data('price'));
                                    var cont = option.parents('.lb-product-wrapper');
                                    var img  = option.data('img');

                                    cont.data("variant-id", option.val());
                                    cont.find('.lb-price span.lb-sale').html(price);
                                    cont.find('img').attr('src', img);
                                    reloadCurrency();
                                    return false;
                                });

                                $(document).on('change', '#dpModal-container select.lb-variants', function(){
                                    var option = $(this).find(":selected");
                                    var price  = baDisplayCents(option.data('price'));
                                    var cont = option.parents('.product-container');
                                    var img  = option.data('img');

                                    cont.data("variant-id", option.val());
                                    cont.find('span.lb-sale').html(price);
                                    cont.find('img').attr('src', img);
                                    reloadCurrency();
                                    return false;
                                });


                            <style type="text/css">
                            .lb-vol-wrapper{
                                    margin-top:20px;
                                }
                            .lion-cart-item-line-price .original_price {
                                    display: block;
                                    text-decoration: line-through;
                                }
                            .lion-cart-item-price, .lion-cart-total, .lion-cart-item-line-price .discounted_price {
                                    display: block;
                                    font-weight: bold;
                                }

                            .lion-cart-item-success-notes, .lion-cart-item-upsell-notes {
                                    display: block;
                                    font-weight:bold;
                                    color: #4195c5;
                                    font-size: 100%;
                                    a {
                                        color: #4195c5;
                                    }
                                }
                            .lion-cart-total {
                                    display: block;
                                    font-weight: bold;
                                }
                            .lion-messages{
                                    display:block;
                                }

  #lion-discount-item{
                                    font-size:70%;
                                    padding-top: 5px;
                                    padding-bottom: 5px;
                                }

  #lion-summary-item{
                                    font-size:70%;
                                    padding-top: 5px;
                                    padding-bottom: 5px;
                                }

                            .summary-line-note{
                                    padding-right: 10px;
                                }

                            .summary-line-discount{
                                    color: #4195c5;
                                }

                                input#lion-discount-code{
                                    max-width:200px;
                                    display:inline-block;
                                }

                                button#apply-lion-discount{
                                    display:inline-block;
                                    max-width:200px;
                                }

                                div#lion-notification-bar{
                                    font-size: 110%;
                                    background-color: #a7ea28;
                                    padding: 12px;
                                    color: #ffff00;
                                    font-family: inherit;
                                    z-index: 9999999999999;
                                    display: none;
                                    left: 0px;
                                    width: 100%;
                                    margin: 0px;
                                    margin-bottom:20px;
                                    text-align: center;
                                    text-transform: none;
                                }

                                div#lion-close-notification{
                                    float: right;
                                    font-weight: bold;
                                    height: 0;
                                    overflow: visible;
                                    cursor: pointer;
                                    margin-right: 2em;
                                }

                            .lb-eqs{
                                    display:none;
                                }

                            .lb-product-bundle {
                                    clear: both;
                                    margin: 0 0 20px;
                                }
                            .lb-product-bundle .bundle-title {
                                    font-size: 20px;
                                    margin-bottom: 15px;
                                }
                            .lb-product-bundle .lb-product-wrapper,
                            .lb-product-bundle .bundle-plus,
                            .lb-product-bundle .bundle-total{
                                    display: inline-block;
                                    text-align: center;
                                    vertical-align: middle;
                                }
                            .lb-product-bundle .bundle-total {
                                    text-align: left;
                                }

    #two-product.lb-product-bundle .bundle-plus img {
                                    position: relative;
                                    margin-left: -54% !important;
                                    max-width: 38px !important;
                                }

    #three-product.lb-product-bundle .bundle-plus img {
                                    position: relative;
                                    margin-left: -94% !important;
                                    max-width: 38px !important;
                                }
                            .lb-product-bundle .lb-product-wrapper {
                                    line-height: 15px;
                                }
                            .lb-product-bundle .lb-product-wrapper img {
                                    width: 80%;
                                }
                            .lb-product-bundle .bundle-name {
                                    margin: 10px 0 5px 0;
                                }
                            .lb-product-bundle .lb-price {
                                    margin-bottom: 5px;
                                    display: inline-block;
                                    margin-right: 5px;
                                    width:100%;
                                }
                            .lb-product-bundle .buy-bundle {
                                    text-align: right;
                                    padding: 5px 0;
                                }
                            .lb-product-bundle .buy-bundle input.addtocart {
                                    padding: 5px 10px;
                                    background-color: #222;
                                    color: #FFF;
                                    border: none;
                                }
                            .lion-variants-container select {
                                    width: 100%;
                                    margin-bottom: 5px
                                }
                            .lb-product-bundle .lion-variants-container {
                                    border: none;
                                    margin: 0;
                                    padding: 0;
                                }
                            .lb-product-bundle .lb-price.regular {
                                    color: #bbb;
                                    text-decoration: line-through;
                                }
                            .lb-product-bundle .lb-product-wrapper {
                                    vertical-align: top !important;
                                    line-height: 15px;
                                    text-align:center;
                                }
                            .lb-product-bundle .lb-image {
                                    min-height: 125px;
                                    max-height: 125px;
                                    line-height: 125px;
                                    border: 1px solid #d9d9d9;
                                    background: #fff;
                                    text-align: center;
                                }
                            .lb-product-bundle .lb-image img {
                                    max-height: 115px;
                                    max-width: 100%;
                                    vertical-align: middle;
                                    height: auto;
                                    width: auto;
                                }
                            .lb-product-bundle .bundle-plus {
                                    line-height: 125px;
                                    max-width: 22px;
                                }
                            .lb-bundle-wrapper .lb-product-bundle button {
                                    display: inline-block;
                                    padding: 4px 10px 4px;
                                    margin-bottom: 0;
                                    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.1);
                                    color: #ffffff;
                                    text-align: center;
                                    vertical-align: middle;
                                    background-repeat: repeat-x;
                                    border: 1px solid #cccccc;
                                    border-bottom-color: #b3b3b3;
                                    -webkit-border-radius: 4px;
                                    -moz-border-radius: 4px;
                                    border-radius: 4px;
                                    -webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
                                    -moz-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
                                    box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
                                    cursor: pointer;
                                    background-color: #414141;
                                    background-image: -moz-linear-gradient(top, #555555, #222222);
                                    background-image: -ms-linear-gradient(top, #555555, #222222);
                                    background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#555555), to(#222222));
                                    background-image: -webkit-linear-gradient(top, #555555, #222222);
                                    background-image: -o-linear-gradient(top, #555555, #222222);
                                    background-image: linear-gradient(top, #555555, #222222);
                                    background-repeat: repeat-x;
                                    filter: progid: DXImageTransform.Microsoft.gradient(startColorstr='#555555', endColorstr='#222222', GradientType=0);
                                    border-color: #222222 #222222 #000000;
                                    border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
                                    filter: progid: dximagetransform.microsoft.gradient(enabled=false);
                                    padding: 5px 10px;
                                }
                            .lb-product-bundle button .top-button {
                                    font-size: 12px;
                                    border-bottom: 1px solid rgba(0, 0, 0, 0.2);
                                    display: block;
                                    padding: 0 5px 2px 5px;
                                }
                            .lb-product-bundle button .bottom-button {
                                    font-size: 14px;
                                    padding: 2px 5px 0 5px;
                                    display: block;
                                    border-top: 1px solid rgba(255, 255, 255, 0.1);
                                }
                            .lb-product-bundle .lb-price {
                                    position: static;
                                    background: none;
                                    text-align: center;
                                    padding: 0;
                                }
                            .lb-product-bundle .lb-eqs {
                                    width: auto;
                                    margin: 0 10px;
                                }

    #two-product.lb-product-bundle .lb-product-wrapper {
                                    width: 46%;
                                }
    #two-product.lb-product-bundle .bundle-plus {
                                    width: 4%;
                                    font-size: 75px
                                }
    #two-product.lb-product-bundle.with-total .bundle-total {
                                    width: 100%;
                                    margin: 10px 0 0 0;
                                    text-align: right;
                                    display:inline-block;
                                }
    #three-product.lb-product-bundle .lb-product-wrapper {
                                    width: 28%;
                                }
    #three-product.lb-product-bundle .bundle-plus {
                                    width: 3%;
                                    font-size: 50px
                                }
    #four-product.lb-product-bundle .lb-product-wrapper {
                                    width: 21%;
                                }
    #four-product.lb-product-bundle .bundle-plus {
                                    width: 3.5%;
                                    font-size: 40px
                                }

                            .with-total .bundle-total button{
                                    white-space: normal;
                                }

    #two-product.lb-product-bundle.with-total .lb-product-wrapper {
                                    width: 30%;
                                }
    #two-product.lb-product-bundle.with-total .bundle-plus {
                                    font-size: 70px;
                                }
    #two-product.lb-product-bundle.with-total .bundle-total {
                                    font-size: 35px;
                                    margin: 0;
                                    text-align: right;
                                }


    #two-product.lb-product-bundle.with-total .bundle-total {
                                    width: 34%;
                                }
    #three-product.lb-product-bundle.with-total .lb-product-wrapper {
                                    width: 21%;
                                }

    #three-product.lb-product-bundle.with-total .bundle-total button{
                                    width: 70%;
                                    margin-top:0px;
                                }
    #three-product.lb-product-bundle.with-total .bundle-plus {
                                    font-size: 45px
                                }

    #two-product.lb-product-bundle.with-total .bundle-total {
                                    display:inline-block;
                                    width: 30%;
                                    font-size: 30px;
                                    text-align: right;
                                }

    #three-product.lb-product-bundle.with-total .bundle-total {
                                    display:inline-block;
                                    width: 25%;
                                    font-size: 30px;
                                    text-align: right;
                                }

                            .lb-product-bundle.with-total .bundle-total .lb-eqs {
                                    display: inline-block;
                                    text-align: center;
                                    width: 24px;
                                    padding: 0px;
                                    margin: 0px;
                                }


    #four-product.lb-product-bundle.with-total .lb-product-wrapper,
    #four-product.lb-product-bundle.with-total .bundle-total {
                                    width: 20%;
                                }
    #four-product.lb-product-bundle.with-total .bundle-plus {
                                    font-size: 40px
                                }
    #four-product.lb-product-bundle.with-total .bundle-total {
                                    font-size: 25px
                                }

    #four-product.lb-product-bundle .lb-eqs {
                                    display: none;
                                }

    #four-product.lb-product-bundle.with-total .bundle-total {
                                    width: 100%;
                                    text-align: center;
                                }
    #max-two {
                                    width: 61%;
                                }
    #max-two .bundle-plus {
                                    width: 10%
                                }
    #three-product.max-two .bundle-total,
    #four-product.max-two .bundle-total {
                                    height: 384px;
                                }
    #three-product.max-two .lb-eqs,
    #four-product.max-two .lb-eqs {
                                    position: relative;
                                    top: 50%;
                                }
    #three-product.max-two button,
    #four-product.max-two button {
                                    position: relative;
                                    top: 50%;
                                }
    #max-two {
                                    width: 60%;
                                    display: inline-block;
                                }
    #max-two .bundle-plus:nth-child(4n+4) {
                                    display: none;
                                }
                            .lb-product-bundle .lb-product-wrapper {
                                    width: 45%;
                                    margin-bottom: 20px;
                                }
                            .lb-product-bundle .bundle-total {
                                    color: #bfbfbf;
                                    font-size: 21px;
                                    font-weight: bold;
                                }

                            .with-total.lb-product-bundle .lb-eqs {
                                    display: inline-block;
                                    text-align: center;
                                    width: 24px;
                                    vertical-align: middle;
                                }
                            .lb-product-bundle .bundle-total button {
                                    width: 100%;
                                    /*max-width: 150px;*/
                                }
    @media screen and (max-width: 650px) {
                                .lb-product-bundle .lb-product-wrapper {
                                        width: 100% !important;
                                    }
                                .lb-product-bundle .lb-product-wrapper > a:first-child {
                                        width: 35% !important;
                                        margin-right: 20px;
                                        display: block;
                                        float: left;
                                    }
                                .lb-product-bundle .lb-product-wrapper .bundle-name {
                                        margin: 0;
                                    }
                                .lb-product-bundle .bundle-plus {
                                        width: 100% !important;
                                        max-width: 100%;
                                        line-height: 0;
                                        border-bottom: 1px solid #ccc;
                                        margin-top: -42px;
                                    }
                                .lb-product-bundle .bundle-plus img {
                                        margin-left: 0 !important;
                                        margin-bottom: -18px;
                                    }
                                .lion-variants-container select {
                                        width: auto;
                                        margin-top: 10px
                                    }
                                .lb-product-bundle .bundle-total {
                                        width: 100% !important;
                                        text-align: center;
                                    }
                                .lb-product-bundle .lb-eqs {
                                        width: 100%;
                                        border-bottom: 1px solid #ccc;
                                        margin: -22px 0 0 0;
                                        position: relative;
                                    }
                                .lb-product-bundle .lb-eqs img {
                                        margin-left: 0 !important;
                                        margin-bottom: -18px;
                                        display: none;
                                    }
        #three-product.lb-product-bundle.with-total .bundle-total button{
                                        margin-top: 20px;
                                        width: 100%;
                                    }


                                .lb-product-bundle .lb-image-container{
                                        width: 35%;
                                        display: inline-block;
                                        float: left;
                                    }

                                .lb-product-bundle .lb-info-wrapper{
                                        width: 60%;
                                        display: inline-block;
                                        float: left;
                                        padding-left:10px;
                                    }

                                .lb-product-bundle .lb-info-wrapper select.lb-variants{
                                        width:100%;
                                    }


                                }

                            .lb-product-bundle .lb-price {
                                    color: #8C0000;
                                }
                            .lb-product-bundle .bundle-total {
                                    color: #8C1919;
                                }

                            .lb-product-bundle .bundle-total button {
                                    width: 100%;
                                }
                            .lb-product-bundle button .top-button {
                                    border-bottom: 0px;
                                }

    @media (max-width: 650px) {
                                .bundle-name {
                                        display: inline-block !important;
                                    }
                                .lb-eqs {
                                        width: 100% !important;
                                    }
                                .bundle-name {
                                        padding-top: 10px !important;
                                    }
                                .bundle-total button {
                                        margin-left: auto !important;
                                        margin-right: auto !important;
                                        display: block;
                                    }
                                }
                            .bundle-total button {
                                    color: #fff
                                    margin: -10px 0 -10px;
                                    width: 100%;
                                    border: none;
                                    text-decoration: none;
                                    font-size: 13%;
                                    font-family: inherit;
                                    text-transform: uppercase;
                                    font-weight: 500;
                                    padding: 10px;
                                    height:100%;
                                }

                            .with-total.lb-product-bundle .bundle-total button.add-lion-bundle {
                                    width: 70%;
                                }

                            .lb-product-bundle button .top-button {
                                    border-bottom: 0px;
                                }

    @media (max-width: 650px) {
                                .bundle-name {
                                        display: inline-block !important;
                                    }
                                .lb-eqs {
                                        width: 100% !important;
                                    }
                                .bundle-name {
                                        padding-top: 10px !important;
                                    }
                                .bundle-total button {
                                        margin-left: auto !important;
                                        margin-right: auto !important;
                                        display: block;
                                    }
                                }


                            .dp-popup div, .dp-popup span,
                            .dp-popup h1, .dp-popup h2, .dp-popup h3, .dp-popup h4, .dp-popup h5, .dp-popup h6,
                            .dp-popup p, .dp-popup a, .dp-popup img, .dp-popup b, .dp-popup u, .dp-popup i,
                            .dp-popup ol, .dp-popup ul, .dp-popup li,
                            .dp-popup form, .dp-popup label, .dp-popup table, .dp-popup tbody, .dp-popup tfoot,
                            .dp-popup thead, .dp-popup tr, .dp-popup th, .dp-popup td{
                                    margin: 0;
                                    padding: 0;
                                    border: 0;
                                    font-size: 100%;
                                    font: inherit;
                                    vertical-align: baseline;
                                    text-transform: none;
                                }

                            .dp-popup body {
                                    line-height: 1;
                                }
                            .dp-popup ol, .dp-popup ul {
                                    list-style: none;
                                }
                            .dp-popup table {
                                    border-collapse: collapse;
                                    border-spacing: 0;
                                }

                            .blocker {
                                    position: fixed;
                                    top: 0; right: 0; bottom: 0; left: 0;
                                    width: 100%; height: 100%;
                                    overflow: auto;
                                    z-index: 99999999;
                                    padding: 20px;
                                    box-sizing: border-box;
                                    background-color: rgb(0,0,0);
                                    background-color: rgba(0,0,0,0.75);
                                    text-align: center;
                                }
                            .blocker:before{
                                    content: '';
                                    display: inline-block;
                                    height: 100%;
                                    vertical-align: middle;
                                    margin-right: -0.05em;
                                }
                            .blocker.behind {
                                    background-color: transparent;
                                }
                            .dp-popup-dpModal {
                                    display: inline-block;
                                    min-width: 400px;
                                    vertical-align: middle;
                                    position: relative;
                                    z-index: 99999999;
                                    max-width: 600px;
                                    background: #fff;
                                    padding: 30px;
                                    -webkit-border-radius: 8px;
                                    -moz-border-radius: 8px;
                                    -o-border-radius: 8px;
                                    -ms-border-radius: 8px;
                                    border-radius: 8px;
                                    -webkit-box-shadow: 0 0 10px #000;
                                    -moz-box-shadow: 0 0 10px #000;
                                    -o-box-shadow: 0 0 10px #000;
                                    -ms-box-shadow: 0 0 10px #000;
                                    box-shadow: 0 0 10px #000;
                                    text-align: center;
                                    text-transform: none;
                                    font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
                                    font-size: 14px;
                                    line-height: 1.42857143;
                                    color: #333333;
                                    -moz-transition: background-color 0.15s linear;
                                    -webkit-transition: background-color 0.15s linear;
                                    -o-transition: background-color 0.15s linear;
                                    transition: background-color 0.15s cubic-bezier(0.785, 0.135, 0.150, 0.860);
                                }

                            .dp-popup-dpModal a {
                                    background-color: transparent;
                                }
                            .dp-popup-dpModal a:active,
                            .dp-popup-dpModal a:hover {
                                    outline: 0;
                                }

                            .dp-popup-dpModal hr {
                                    height: 0;
                                    -webkit-box-sizing: content-box;
                                    -moz-box-sizing: content-box;
                                    box-sizing: content-box;
                                    height: 0;
                                    margin-top: 20px;
                                    margin-bottom: 20px;
                                    border: 0;
                                    border-top: 1px solid #eeeeee;
                                }

                            .dp-popup-dpModal button,
                            .dp-popup-dpModal input,
                            .dp-popup-dpModal optgroup,
                            .dp-popup-dpModal select,
                            .dp-popup-dpModal textarea {
                                    color: inherit;
                                    font: inherit;
                                    margin: 0;
                                    font-family: inherit;
                                    font-size: inherit;
                                    line-height: inherit;
                                }


                            .dp-popup-dpModal button {
                                    overflow: visible;
                                }
                            .dp-popup-dpModal button,
                            .dp-popup-dpModal select {
                                    text-transform: none;
                                }
                            .dp-popup-dpModal button {
                                    -webkit-appearance: button;
                                    cursor: pointer;
                                }
                            .dp-popup-dpModal button::-moz-focus-inner,
                            .dp-popup-dpModal input::-moz-focus-inner {
                                    border: 0;
                                    padding: 0;
                                }
                            .dp-popup-dpModal input {
                                    line-height: normal;
                                }
                            .dp-popup-dpModal input[type='number']::-webkit-inner-spin-button,
                            .dp-popup-dpModal input[type='number']::-webkit-outer-spin-button {
                                    height: auto;
                                }


                            .dp-popup-dpModal body.fadein {
                                    background: rgba(0, 0, 0, 0.65);
                                }

#dpModal-container{
                                    width:100%;
                                }

                            .dp-popup-dpModal #popup-dpModal-container {
                                    background: white;
                                    padding: 12px 18px 40px 18px;
                                }

@media only screen and (min-width:500px) {
                                .dp-popup-dpModal #popup-dpModal-container {
                                        border-radius: 5px;
                                        padding: 30px 40px;
                                    }
                                }

@media only screen and (min-width:992px) {
                                .dp-popup-dpModal #popup-dpModal-container {
                                        margin-top: 140px;
                                    }
                                }

                            .dp-popup-dpModal .fade {
                                    opacity: 0;
                                    -webkit-transition: opacity 0.15s linear;
                                    -o-transition: opacity 0.15s linear;
                                    transition: opacity 0.15s linear;
                                }
                            .dp-popup-dpModal .fade.in {
                                    opacity: 1;
                                }

                                /* only the stuff we need added here */

                            .dp-popup-dpModal h2 {
                                    font-size: 24px;
                                    font-family: inherit;
                                    font-weight: 500;
                                    line-height: 1.1;
                                    color: inherit;
                                }

                            .dp-popup-dpModal h3 {
                                    font-family: inherit;
                                    font-weight: normal;
                                    line-height: 1.1;
                                    color: inherit;
                                    font-size: 18px;
                                    margin-top: 10px;
                                    margin-bottom: 20px;
                                    font-weight:500;
                                }

                            .dp-popup-dpModal p.body-text {
                                    font-size: 20;
                                    margin-top: 40px;
                                    margin-bottom: 10px;
                                }

                            .dp-popup-dpModal .form-control {
                                    display: block;
                                    width: 100%;
                                    height: 34px;
                                    padding: 6px 12px;
                                    font-size: 14px;
                                    line-height: 1.42857143;
                                    color: #555;
                                    background-color: #fff;
                                    background-image: none;
                                    border: 1px solid #ccc;
                                    border-radius: 5px;
                                    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
                                    -moz-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
                                    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
                                    -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
                                    -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
                                    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
                                }

                            .dp-popup-dpModal .input-lg {
                                    height: 46px;
                                    padding: 10px 16px;
                                    line-height: 1.3333333;
                                    border-radius: 6px;
                                }
                            .dp-popup-dpModal select.input-lg {
                                    height: 46px;
                                }

@media screen and (-webkit-min-device-pixel-ratio:0) {
                                .dp-popup-dpModal select:focus,
                                .dp-popup-dpModal textarea:focus,
                                .dp-popup-dpModal input:focus {
                                        font-size: 16px;
                                        background: #eee;
                                    }
                                }


                            .dp-popup-dpModal .form-group {
                                    margin-bottom: 15px;
                                }

                            .dp-popup-dpModal .btn {
                                    display: inline-block;
                                    padding: 8px 12px;
                                    margin-bottom: 0;
                                    font-size: 14px;
                                    line-height: 1.42857143;
                                    text-align: center;
                                    vertical-align: middle;
                                    letter-spacing: 1px;
                                    -ms-touch-action: manipulation;
                                    touch-action: manipulation;
                                    cursor: pointer;
                                    -webkit-user-select: none;
                                    -moz-user-select: none;
                                    -ms-user-select: none;
                                    user-select: none;
                                    background-image: none;
                                    border: 1px solid transparent;
                                    border-radius: 3px;
                                }

                            .dp-popup-dpModal .btn-success {
                                    width: 100%;
                                    color: #ffffff;
                                    background-color: #4ed14e;
                                }

                            .dp-popup-dpModal .btn-lg {
                                    line-height: 24px;
                                    font-size: 15px;
                                    padding:14px;
                                    line-height: 1.3333333;
                                }

                            .dp-popup-dpModal .close {
                                    -webkit-appearance: none;
                                    padding: 0;
                                    cursor: pointer;
                                    background: 0 0;
                                    border: 0;
                                    text-align:center;
                                    font-size: 21px;
                                    font-weight: 700;
                                    line-height: 1;
                                    color: #000;
                                    text-shadow: 0 1px 0 #fff;
                                }

                            .dp-popup-dpModal form{
                                    margin-top:10px;
                                }

                            .dp-popup-dpModal .dpModal-content .close {
                                    font-size: 30px;
                                }
                            .dp-popup-dpModal .dpModal-backdrop.in {
                                    filter: alpha(opacity=65);
                                    opacity: .65;
                                }

                            .dp-popup-dpModal .completed_message {
                                    display: none;
                                }
                            .dp-popup-dpModal .complete .completed_message {
                                    display: block;
                                }

                            .dp-popup-dpModal .single-variant{
                                    display:none;
                                }

                            .dp-popup-dpModal div.footer{
                                    margin-top:20px;
                                }

                            .dp-popup-dpModal div.footer p{
                                    color: #b3b3b3;
                                    font-size:12px;
                                }

                            .dp-popup-dpModal .alert {
                                    padding: 10px;
                                    margin: 20px 0px;
                                    border: 1px solid transparent;
                                    border-radius: 4px;
                                }

                            .dp-popup-dpModal .alert-success {
                                    color: red;
                                    background-color: red;
                                    border-color: #d6e9c6;
                                }

                            .dp-popup-dpModal .alert-danger {
                                    color: red;
                                    background-color: red;
                                    border-color: #ebccd1;
                                }

                            .dp-popup-dpModal div.no-thanks{
                                    padding-top:20px;
                                }

                            .dp-popup-dpModal div.no-thanks a{
                                    color: #aaa;
                                    font-size: 100%;
                                }

@media (min-width: 0px) {
                                .dp-popup-dpModal {
                                        min-width:0px;
                                    }

                                }

@media (min-width: 768px) {
                                .dp-popup-dpModal {
                                        min-width:600px;
                                    }

                                }

                            .dp-popup-dpModal img{
                                    vertical-align: middle;
                                    max-width: 100%;
                                }

                            .dp-popup-dpModal img.single{
                                    margin-right: 20px;
                                    margin-left: 0px;
                                    display: inline-block;
                                    padding-right: 20px;
                                    max-width: 100%;
                                    height: auto;
                                    margin: 0 auto;
                                }


  #upsell-minimized-button{
                                    background-color:#44c767;

                                    border-top-left-radius: 8px;
                                    border-top-right-radius: 8px;
                                    -moz-border-top-left-radius: 8px;
                                    -moz-border-top-right-radius: 8px;
                                    -webkit-border-top-left-radius: 8px;
                                    -webkit-border-top-right-radius: 8px;

                                    /*border:1px solid #18ab29;*/

                                    display:inline-block;
                                    cursor:pointer;

                                    color:#ffffff;

                                    /*font-family:Arial;*/

                                    padding:10px 16px;
                                    text-decoration:none;

                                    background: #44c767;
                                    color: #ffffff;
                                    font-size: 16px;

                                    -webkit-transform: rotate(90deg);
                                    -webkit-transform-origin: left bottom;
                                    -moz-transform: rotate(90deg);
                                    -moz-transform-origin: left bottom;
                                    -ms-transform: rotate(90deg);
                                    -ms-transform-origin: left bottom;
                                    -o-transform: rotate(90deg);
                                    -o-transform-origin: left bottom;
                                    transform: rotate(90deg);
                                    left: 0px;
                                    top: 100px;
                                    transform-origin: left bottom;

                                    white-space: nowrap;
                                    position: fixed;
                                }

  #upsell-minimized-button:hover {
                                    /*background-color:#5cbf2a;*/
                                }


                            .dp-popup-dpModal a.close-dpModal {
                                    position: absolute;
                                    top: -12.5px;
                                    right: -12.5px;
                                    display: block;
                                    width: 30px;
                                    height: 30px;
                                    text-indent: -9999px;
                                    background: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAACXBIWXMAAAsTAAALEwEAmpwYAAAABGdBTUEAANjr9RwUqgAAACBjSFJNAABtmAAAc44AAPJxAACDbAAAg7sAANTIAAAx7AAAGbyeiMU/AAAG7ElEQVR42mJkwA8YoZjBwcGB6fPnz4w/fvxg/PnzJ2N6ejoLFxcX47Rp036B5Dk4OP7z8vL+P3DgwD+o3v9QjBUABBALHguZoJhZXV2dVUNDgxNIcwEtZnn27Nl/ZmZmQRYWFmag5c90dHQY5OXl/z98+PDn1atXv79+/foPUN9fIP4HxRgOAAggRhyWMoOwqKgoq6GhIZe3t7eYrq6uHBDb8/Pz27Gysloga/jz588FYGicPn/+/OapU6deOnXq1GdgqPwCOuA31AF/0S0HCCB0xAQNBU4FBQWB0NBQublz59oADV37Hw28ePHi74MHD/6ii3/8+HEFMGQUgQ6WEhQU5AeZBTWTCdkigABC9ylIAZeMjIxQTEyMysaNG/3+/v37AGTgr1+//s2cOfOXm5vbN6Caz8jY1NT0a29v76/v37//g6q9sHfv3khjY2M5YAgJgsyEmg0PYYAAQreUk4+PT8jd3V1l1apVgUAzfoIM2rlz5x9gHH5BtxAdA9PB1zNnzvyB+R6oLxoopgC1nBPZcoAAgiFQnLIDMb+enp5iV1eXBzDeHoI0z58//xcwIX0mZCkMg9S2trb+hFk+ffr0QCkpKVmQ2VA7QHYxAgQQzLesQMwjIiIilZWVZfPu3bstMJ+SYikyBmUzkBnA9HEMyNcCYgmQHVC7mAACCJagOEBBbGdnp7lgwYJEkIavX7/+BcY1SvAaGRl9tba2xohjMTGxL8nJyT+AWQsuxsbG9vnp06e/QWYdPHiwHmiWKlBcCGQXyNcAAQSzmBuoSQqYim3u37+/EKR48uTJv5ANB+bVr7Dga2xs/AkTV1JS+gq0AJyoQIkPWU9aWtoPkPibN2/2A/l6QCwJ9TULQADB4hcY//xKXl5eHt++fbsAUmxhYYHiM1DiAsr9R7ZcVVUVbikIdHd3/0TWIyws/AWYVsByAgICdkAxRSAWAGI2gACClV7C4uLiOv7+/lEgRZ8+ffqLLd6ABck3ZMuB6uCWrlu37je29HDx4kVwQisvL88FFqkaQDERUHADBBAomBl5eHiYgQmLE1hSgQQZgIUD1lJm69atf4HR8R1YKoH5QIPAWWP9+vV/gOI/gHkeQw+wGAXTwAJJ5t+/f/BUDRBA4NIEKMDMyMjICtQIiniG379/4yza7t69+//Lly8oDrty5co/bJaCAEwcZCkwwTJDLWYCCCCwxcDgY3z16hXDnTt3voP4EhISWA0BFgZMwNqHExh3jMiG1tbWsgHjnA2bHmAeBtdWwOL1MycnJ7wAAQggBmi+kgIW/OaKiorJwOLuFShO0LMSMPF9AUYBSpz6+vqixHlOTs4P9MIEWHaDsxSwYMoE2mEGFJcG5SKAAGJCqjv/AbPUn8ePH98ACQQHB6NUmZqamkzABIgSp5s3bwbHORCA1QDLAWZkPc7OzszA8oHl5cuXVy5duvQBGIXwWgoggGA+FgO6xkBNTS28r69vDrT2+Y1cIMDyJchX6KkXVEmAshd6KB06dAic94EO3AzkBwGxPhCLg8ptgACCZyeQp9jZ2b2AmsuAefM8tnxJCk5ISPgOLTKfAdNEOVDMA2QHLDsBBBC8AAFlbmCLwlZISCg5JSVlJizeQAaQaimoWAUFK0g/sGGwHiiWCMS2yAUIQAAxI7c4gEmeFZi4OJ48ecLMzc39CRiEmgEBASxA/QzA8vYvAxEgNjaWZc2aNezAsprp2LFjp4FpZRdQ+AkQvwLij0AMSoC/AQIIXklAC3AVUBoBxmE8sPXQAiyvN8J8fuPGjR/h4eHf0eMdhkENhOPHj8OT+NGjR88BxZuBOA5kJtRseCUBEECMSI0AdmgBDooDaaDl8sASTSkyMlKzpqZGU1paGlS7MABLrX83b978A6zwwakTmE0YgIkSnHpBfGCV+gxYh98qKSk5CeTeAxVeQPwUiN8AMSjxgdLNX4AAYkRqCLBAXcMHtVwSaLkMMMHJAvOq9IQJE9R8fHxElJWV1bEF8aNHj+7t27fvLTDlXwXGLyhoH0OD+DnU0k/QYAa1QP8BBBAjWsuSFWo5LzRYxKFYAljqiAHzqxCwIBEwMTERBdZeoOYMA7Bl+RFYEbwB5oS3IA9D4/IFEL+E4nfQ6IDFLTgvAwQQI5ZmLRtSsINSuyA0uwlBUyQPMPWD20/AKo8ByP4DTJTfgRgUjB+gFoEc8R6amGDB+wu5mQsQQIxYmrdMUJ+zQTM6NzQEeKGO4UJqOzFADQMZ/A1qCSzBfQXi71ALfyM17sEAIIAY8fQiWKAYFgIwzIbWTv4HjbdfUAf8RPLhH1icojfoAQKIEU8bG9kRyF0aRiz6YP0k5C4LsmUY9TtAADEyEA+IVfufGEUAAQYABejinPr4dLEAAAAASUVORK5CYII=') no-repeat 0 0;
                                }


                            .dp-popup .just-added{
                                    width:100%;
                                    border-bottom: 1px solid #eee;
                                    padding-bottom: 20px;
                                }

                            .dp-popup .multiple-products-true div{
                                    display:block;
                                    float:left;
                                }

                            .dp-popup .multiple-products-false div{
                                    display:block;
                                }

                            .dp-popup .multiple-products-false div.product-container{
                                    max-width:350px;
                                    width:100%;
                                    margin-left:auto;
                                    margin-right:auto;
                                }

                            .dp-popup .multiple-products-false .image{
                                    width:100%;
                                }

                            .dp-popup .multiple-products-true .image{
                                    width:100px;
                                }

                            .dp-popup .multiple-products-false .image img{
                                    max-width: 150px;
                                    max-height:150px;
                                }

                            .dp-popup .multiple-products-true .image img{
                                    max-width: 100px;
                                    max-height:100px;
                                }

                            .dp-popup .multiple-products-false .details{
                                    width:100%;
                                    text-align:center;
                                    font-size:14px;
                                    padding-left: 15px;
                                    padding-right: 15px;
                                    padding-top:20px;
                                }

                            .dp-popup .multiple-products-true .details{
                                    width:60%;
                                    text-align:left;
                                    font-size:14px;
                                    padding-left: 15px;
                                    padding-right: 15px;
                                }

                            .dp-popup .multiple-products-false .actions{
                                    width:100%;
                                    text-align:center;
                                    padding-top:20px;
                                }

                            .dp-popup .multiple-products-true .actions{
                                    vertical-align:middle;
                                    max-width: 116px;
                                    width:100%;
                                }

                            .dp-popup .other-upsells{
                                    width:100%;
                                }

                            .dp-popup .product-container{
                                    width:100%;
                                    padding-bottom: 10px;
                                    padding-top:10px;
                                }

                            .dp-popup .product-container:not(first){
                                    border-top: 1px #eee solid;
                                }

                            .dp-popup .product-container select{
                                    width:100%;
                                    margin-bottom:10px;
                                    font-size:12px;
                                }

                            .dp-popup .product-container .variant-wrapper{
                                    float:none;
                                }

                            .dp-popup .no-thanks{
                                    text-align: center;
                                    width: 100%;
                                }

                            .dp-popup-dpModal .lb-image{
                                    width:100%;
                                }

                            .dp-popup-dpModal .upsell-total{
                                    width:100%;
                                    padding-top:10px;
                                }

                            .dp-popup-dpModal button.add-upsells{
                                    color: #fff;;
                                    font-size: 100%;
                                    background-color: #a1c65b;;
                                    display: inline-block;
                                    padding: 8px 12px;
                                    margin-bottom: 0;
                                    font-size: 14px;
                                    line-height: 1.42857143;
                                    text-align: center;
                                    vertical-align: middle;
                                    letter-spacing: 1px;
                                    -ms-touch-action: manipulation;
                                    touch-action: manipulation;
                                    cursor: pointer;
                                    -webkit-user-select: none;
                                    -moz-user-select: none;
                                    -ms-user-select: none;
                                    user-select: none;
                                    background-image: none;
                                    border: 1px solid transparent;
                                    border-radius: 3px;
                                    font-weight:500;
                                    width:100%;
                                }

                            .lb-price .lb-regular{
                                    width:100%;
                                }

                            .product-price .lb-sale{
                                    display:block;
                                    width:100%;
                                    font-weight:normal;
                                }

                            .discount-applies-true .lb-price .lb-sale{
                                    text-decoration: line-through;
                                    width:100%;
                                }

                            .discount-applies-true .product-price .lb-sale{
                                    display:block;
                                    width:100%;
                                    color:#8C0000;
                                    font-weight:normal;
                                    opacity:0.8;
                                }


                            .discount-applies-true .product-price .upsell-note{
                                    display:block;
                                    font-weight:normal;
                                    width:100%;
                                }

                            .discount-applies-false .product-price .upsell-note{
                                    display:none;
                                }

  #dpModal-container .product-title{
                                    font-weight:400;
                                    width:100%;
                                }

                            .dp-popup-dpModal .upsell-title{
                                    font-family: inherit;
                                    font-weight: normal;
                                    line-height: 1.1;
                                    color: inherit;
                                    font-size: 18px;
                                    margin-top: 10px;
                                    margin-bottom: 20px;
                                    font-weight: 500;
                                }


  @media (max-width: 650px) {
                                .dp-popup .multiple-products-true .details {
                                        width: 60%;
                                        padding-left: 20px;
                                    }

                                .dp-popup .multiple-products-true .actions {
                                        max-width: inherit;
                                        width: 100%;
                                        padding-top: 15px;
                                    }

                                .dp-popup .product-container{
                                        padding-bottom: 20px;
                                        padding-top:20px;
                                    }


                                }

                            .lb-bundle-wrapper .lb-product-bundle button.add-lion-bundle{
                                    cursor: pointer;
                                    background-color: #a1c65b;
                                    background-image: none;
                                    color: #fff;
                                    margin: -10px 0 -10px;
                                    width: 100%;
                                    border: none;
                                    text-decoration: none;
                                    font-size: 13px;
                                    font-family: inherit;
                                    text-transform: uppercase;
                                    font-weight: 500;
                                    padding: 10px;
                                }

                            .lb-product-bundle .bundle-total button span {
                                    font-size: 13px;
                                }


                                table.lb-discount-table{


                                }




                            </style>


                                <script type="text/javascript">
                                window.lion = {};

                                {% if customer %}
                                window.lion.customer_tags = "{{customer.tags | join: ','}}";
                                {% endif %}

                                {% if template contains 'product' %}
                                {% unless product.id == blank %}
                                {% assign ba_page = 'product' %}
                                window.lion.product = {
                                    id: {{product.id}},
                                price: {{product.price}},
                            };
                                window.lion.product_collections = []
                                {% for c in product.collections %}
                                window.lion.product_collections.push({{c.id}})
                                {% endfor %}
                                {% endunless %}
                                {% endif %}


                                {% if template contains 'cart' %}
                                {% assign ba_page = 'cart' %}
                                {% endif %}

                                {% if cart %}
                                window.lion.cart = {{cart | json}}
                                window.lion.cart.items = []

                                {% for item in cart.items %}
                                var lion_item = {{item | json}};
                                lion_item["collection_ids"] = [];
                                {% for c in item.product.collections %}
                                lion_item["collection_ids"].push({{c.id}})
                                {% endfor %}
                                window.lion.cart.items.push(lion_item);
                                {% endfor %}

                                if (typeof window.lion.cart.items == "object") {
                                    for (var i=0; i<window.lion.cart.items.length; i++) {
                                        ["sku", "grams", "vendor", "url", "image", "handle", "requires_shipping", "product_type", "product_description"].map(function(a) {
                                            delete window.lion.cart.items[i][a]
                                        })
                                    }
                                }
                                {% endif %}
                                window.lion.ba_page = "{{ba_page}}";
                                window.lion.discount_method = "code";


                                {% raw %}
                                window.lion.money_format = "Rs.{{amount}}";
                                {% endraw %}



//Handlebars

                            </script>

                                {% raw  %}
                            <script id="lb-discount-tiers" type="text/x-handlebars-template">
                                    <div class="lb-discount-tiers">
                                    <h4>{{{product_message}}}</h4>

                                <table class="lb-discount-table">
                                    <thead>
                                    <tr>
                                    <th>Minimum Qty</th>
                                <th>
                                Discount
                                </th>
                                </tr>
                                </thead>
                                <tbody>
                                {{#vol_rows}}
                            <tr>
                                <td>{{{quantity}}}+</td>
                                <td>{{{price.title}}}</td>
                                </tr>
                                {{/vol_rows}}
                                </tbody>
                                </table>
                                </div>

                                </script>

                                <script id="lb-bundle" type="text/x-handlebars-template">

                                    <div id="{{css_length}}-product" class="discount-applies-{{discount_applies}} lb-product-bundle with-total">
                                    <div class="bundle-title">{{bundle_note}}</div>
                                    {{#products}}
                                <div class="lb-product-wrapper" data-product-id="{{id}}" data-variant-id="{{variants.0.id}}" data-quantity="{{quantity}}">
                                        <div class='lb-image-container'>
                                        <div class="lb-image">
                                        <a href="/products/{{handle}}">
                                        <img src="{{{image.src}}}"/>
                                        </a>
                                        </div>
                                        </div>
                                        <div class="lb-info-wrapper">
                                        <a href="/products/{{handle}}">
                                        <div class="bundle-name">{{title}} x {{quantity}}</div>
                                    </a>
                                    <div class="lion-variants-container" style="{{{variantsStyle}}}">
                                        {{{variantsSelect}}}
                                </div>

                                    <div class="lb-price">

                                        <span class="lb-sale ">{{{variants.0.price}}}</span>
                                    </div>

                                    </div>
                                    </div>

                                    {{#unless @last}}
                                <div class="bundle-plus">
                                        <img src="{{{../ba_plus_url}}}"/>
                                        </div>
                                    {{/unless}}

                                        {{/products}}


                                        <div class="bundle-total show-savings">


                                            <div class="lb-eqs">
                                            <img src="{{{ba_eqs_url}}}"/>
                                            </div>


                                            <button class="add-lion-bundle">
                                            <span class='top-button'>Add Bundle</span>
                                            {{#if discount_applies}}
                                        <span class='bottom-button'>{{{save_text}}}</span>
                                            {{/if}}
                                            </button>
                                            </div>


                                            </div>

                                            </script>


                                            <script id="lb-upsell" type="text/x-handlebars-template">
                                                <div id='dpModal-container' style='display:none;' class='dp-popup dp-wrap dp-whModal'>
                                                <div id="{{css_length}}-product" class="multiple-products-{{multiple_products}} lb-product-bundle">
                                                <h3 class="upsell-title">{{upsell_note}}</h3>
                                                {{#products}}
                                            <div class="product-container discount-applies-{{discount_applies}}" data-product-id="{{id}}" data-variant-id="{{variants.0.id}}" data-quantity="{{qty_left}}">
                                                    <div class="image">
                                                    <a href="/products/{{handle}}" target="_blank">
                                                    <img src="{{{image.src}}}"/>
                                                    </a>
                                                    </div>
                                                    <div class="details multiple-variants">
                                                    <div class="product-title">
                                                    <a href="/products/{{handle}}" target="_blank">
                                                    {{title}} x {{qty_left}}
                                            </a>
                                                </div>
                                                <div class="product-price">
                                                    <span class="lb-sale">{{{variants.0.price}}}</span>
                                                <span class="upsell-note">{{upsell_note}}</span>
                                                </div>
                                                </div>

                                                <div class="actions">
                                                    {{{variantsSelect}}}
                                            <button class="add-upsells">Add to cart</button>
                                                </div>



                                                </div>
                                                {{/products}}
                                                </div>
                                                <div class="no-thanks">
                                                    <a href="#" data-offer-id="{{offer_id}}">No thanks</a>
                                                </div>
                                                </div>
                                                </script>

                                                    {% endraw %}


                                                <script type="text/javascript">
                                                        (function(window, document) {"use strict";

                                                            function setCookie(cname, cvalue, exdays) {
                                                                var d = new Date();
                                                                d.setTime(d.getTime() + (exdays*24*60*60*1000));
                                                                var expires = "expires="+ d.toUTCString();
                                                                document.cookie = cname + "=" + cvalue + "; " + expires + "; path=/;";
                                                            }

                                                            function getCookie(cname) {
                                                                var name = cname + "=";
                                                                var ca = document.cookie.split(';');
                                                                for(var i = 0; i <ca.length; i++) {
                                                                    var c = ca[i];
                                                                    while (c.charAt(0)==' ') {
                                                                        c = c.substring(1);
                                                                    }
                                                                    if (c.indexOf(name) == 0) {
                                                                        return c.substring(name.length,c.length);
                                                                    }
                                                                }
                                                                return "";
                                                            }

                                                            function reqJquery(onload) {
                                                                if(typeof jQuery === 'undefined' || (parseInt(jQuery.fn.jquery) === 1 && parseFloat(jQuery.fn.jquery.replace(/^1\./,'')) < 10)){
                                                                    var head = document.getElementsByTagName('head')[0];
                                                                    var script = document.createElement('script');
                                                                    script.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js';;
                                                                    script.type = 'text/javascript';
                                                                    script.onload = script.onreadystatechange = function() {
                                                                        if (script.readyState) {
                                                                            if (script.readyState === 'complete' || script.readyState === 'loaded') {
                                                                                script.onreadystatechange = null;
                                                                                onload(jQuery.noConflict(true));
                                                                            }
                                                                        }
                                                                        else {
                                                                            onload(jQuery.noConflict(true));
                                                                        }
                                                                    };
                                                                    head.appendChild(script);
                                                                }else {
                                                                    onload(jQuery);
                                                                }
                                                            }

                                                            reqJquery(function($){

                                                                var dpQuery=$,dpModals=[],getCurrent=function(){return dpModals.length?dpModals[dpModals.length-1]:null},selectCurrent=function(){var a,b=!1;for(a=dpModals.length-1;a>=0;a--)dpModals[a].$blocker&&(dpModals[a].$blocker.toggleClass("current",!b).toggleClass("behind",b),b=!0)};dpQuery.dpModal=function(a,b){var c,d;if(this.$body=dpQuery("body"),this.options=dpQuery.extend({},dpQuery.dpModal.defaults,b),this.options.doFade=!isNaN(parseInt(this.options.fadeDuration,10)),this.$blocker=null,this.options.closeExisting)for(;dpQuery.dpModal.isActive();)dpQuery.dpModal.close();if(dpModals.push(this),a.is("a"))if(d=a.attr("href"),/^#/.test(d)){if(this.$elm=dpQuery(d),1!==this.$elm.length)return null;this.$body.append(this.$elm),this.open()}else this.$elm=dpQuery("<div>"),this.$body.append(this.$elm),c=function(a,b){b.elm.remove()},this.showSpinner(),a.trigger(dpQuery.dpModal.AJAX_SEND),dpQuery.get(d).done(function(b){if(dpQuery.dpModal.isActive()){a.trigger(dpQuery.dpModal.AJAX_SUCCESS);var d=getCurrent();d.$elm.empty().append(b).on(dpQuery.dpModal.CLOSE,c),d.hideSpinner(),d.open(),a.trigger(dpQuery.dpModal.AJAX_COMPLETE)}}).fail(function(){a.trigger(dpQuery.dpModal.AJAX_FAIL);var b=getCurrent();b.hideSpinner(),dpModals.pop(),a.trigger(dpQuery.dpModal.AJAX_COMPLETE)});else this.$elm=a,this.$body.append(this.$elm),this.open()},dpQuery.dpModal.prototype={constructor:dpQuery.dpModal,open:function(){var a=this;this.block(),this.options.doFade?setTimeout(function(){a.show()},this.options.fadeDuration*this.options.fadeDelay):this.show(),dpQuery(document).off("keydown.dpModal").on("keydown.dpModal",function(a){var b=getCurrent();27==a.which&&b.options.escapeClose&&b.close()}),this.options.clickClose&&this.$blocker.click(function(a){a.target==this&&dpQuery.dpModal.close()})},close:function(){dpModals.pop(),this.unblock(),this.hide(),dpQuery.dpModal.isActive()||dpQuery(document).off("keydown.dpModal")},block:function(){this.$elm.trigger(dpQuery.dpModal.BEFORE_BLOCK,[this._ctx()]),this.$body.css("overflow","hidden"),this.$blocker=dpQuery('<div class="jquery-dpModal blocker current"></div>').appendTo(this.$body),selectCurrent(),this.options.doFade&&this.$blocker.css("opacity",0).animate({opacity:1},this.options.fadeDuration),this.$elm.trigger(dpQuery.dpModal.BLOCK,[this._ctx()])},unblock:function(a){!a&&this.options.doFade?this.$blocker.fadeOut(this.options.fadeDuration,this.unblock.bind(this,!0)):(this.$blocker.children().appendTo(this.$body),this.$blocker.remove(),this.$blocker=null,selectCurrent(),dpQuery.dpModal.isActive()||this.$body.css("overflow",""))},show:function(){this.$elm.trigger(dpQuery.dpModal.BEFORE_OPEN,[this._ctx()]),this.options.showClose&&(this.closeButton=dpQuery('<a href="#close-dpModal" rel="dpModal:close" class="close-dpModal '+this.options.closeClass+'">'+this.options.closeText+"</a>"),this.$elm.append(this.closeButton)),this.$elm.addClass(this.options.dpModalClass).appendTo(this.$blocker),this.options.doFade?this.$elm.css("opacity",0).show().animate({opacity:1},this.options.fadeDuration):this.$elm.show(),this.$elm.trigger(dpQuery.dpModal.OPEN,[this._ctx()])},hide:function(){this.$elm.trigger(dpQuery.dpModal.BEFORE_CLOSE,[this._ctx()]),this.closeButton&&this.closeButton.remove();var a=this;this.options.doFade?this.$elm.fadeOut(this.options.fadeDuration,function(){a.$elm.trigger(dpQuery.dpModal.AFTER_CLOSE,[a._ctx()])}):this.$elm.hide(0,function(){a.$elm.trigger(dpQuery.dpModal.AFTER_CLOSE,[a._ctx()])}),this.$elm.trigger(dpQuery.dpModal.CLOSE,[this._ctx()])},showSpinner:function(){this.options.showSpinner&&(this.spinner=this.spinner||dpQuery('<div class="'+this.options.dpModalClass+'-spinner"></div>').append(this.options.spinnerHtml),this.$body.append(this.spinner),this.spinner.show())},hideSpinner:function(){this.spinner&&this.spinner.remove()},_ctx:function(){return{elm:this.$elm,$blocker:this.$blocker,options:this.options}}},dpQuery.dpModal.close=function(a){if(dpQuery.dpModal.isActive()){a&&a.preventDefault();var b=getCurrent();return b.close(),b.$elm}},dpQuery.dpModal.isActive=function(){return dpModals.length>0},dpQuery.dpModal.defaults={closeExisting:!0,escapeClose:!0,clickClose:!0,closeText:"Close",closeClass:"",dpModalClass:"dp-popup-dpModal",spinnerHtml:null,showSpinner:!0,showClose:!0,fadeDuration:null,fadeDelay:1},dpQuery.dpModal.BEFORE_BLOCK="dpModal:before-block",dpQuery.dpModal.BLOCK="dpModal:block",dpQuery.dpModal.BEFORE_OPEN="dpModal:before-open",dpQuery.dpModal.OPEN="dpModal:open",dpQuery.dpModal.BEFORE_CLOSE="dpModal:before-close",dpQuery.dpModal.CLOSE="dpModal:close",dpQuery.dpModal.AFTER_CLOSE="dpModal:after-close",dpQuery.dpModal.AJAX_SEND="dpModal:ajax:send",dpQuery.dpModal.AJAX_SUCCESS="dpModal:ajax:success",dpQuery.dpModal.AJAX_FAIL="dpModal:ajax:fail",dpQuery.dpModal.AJAX_COMPLETE="dpModal:ajax:complete",dpQuery.fn.dpModal=function(a){return 1===this.length&&new dpQuery.dpModal(this,a),this},dpQuery(document).on("click.dpModal",'a[rel="dpModal:close"]',dpQuery.dpModal.close),dpQuery(document).on("click.dpModal",'a[rel="dpModal:open"]',function(a){a.preventDefault(),dpQuery(this).dpModal()});

                                                                function reloadCurrency() {
                                                                    if (typeof Currency == "object" && typeof Currency.moneyFormats == "object" && typeof mlvedaload == "function") {
                                                                        mlvedaload()
                                                                    }
                                                                }

                                                                var lionCookieCode = getCookie('lion_discount_' + window.lion.cart.token);
                                                                if(lionCookieCode){
                                                                    window.lion.discount_code = lionCookieCode;
                                                                }

                                                                function baDisplayCents(cents) {
                                                                    if (typeof cents == "undefined" || cents == null){return ""}
                                                                    if (typeof cents == "string" && cents.length == 0){return ""}

                                                                    var format = window.lion.money_format;
                                                                    var moneyRegex = /\{\{\s*(\w+)\s*\}\}/;
                                                                    if (typeof cents == "string") {
                                                                        cents = cents.replace(".", "")
                                                                    }

                                                                    function defOpt(opt,def){return typeof opt == "undefined" ? def : opt}

                                                                    function displayDelims(n,p,t,d){
                                                                        p = defOpt(p, 2);
                                                                        t = defOpt(t, ",");
                                                                        d = defOpt(d, ".");
                                                                        if (isNaN(n) || n == null){
                                                                            return 0
                                                                        }
                                                                        n = (n / 100).toFixed(p);
                                                                        var parts = n.split("."),dollars = parts[0].replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1" + t),cents = parts[1] ? d + parts[1] : "";
                                                                        return dollars + cents
                                                                    }

                                                                    var val = "";
                                                                    switch (format.match(moneyRegex)[1]) {
                                                                        case "amount":
                                                                            val = displayDelims(cents, 2);
                                                                            break;
                                                                        case "amount_no_decimals":
                                                                            val = displayDelims(cents, 0);
                                                                            break;
                                                                        case "amount_no_decimals_with_comma_separator":
                                                                            val = displayDelims(cents, 0, ".", ",");
                                                                            break
                                                                        case "amount_with_comma_separator":
                                                                            val = displayDelims(cents, 2, ".", ",");
                                                                            break;
                                                                    }
                                                                    return format.replace(moneyRegex, val)
                                                                }



                                                                function showLionBundle(ba_data){
                                                                    var arr = [];
                                                                    var products = [];
                                                                    ba_data.ba_bundle.items.map(function(p){
                                                                        arr.push($.getJSON("/products/" + p.handle + ".json", function(data) {
                                                                            var prod = data.product;
                                                                            prod.variantsStyle = "";
                                                                            prod.variantsSelect = "";
                                                                            for (var i = 0; i < prod.variants.length; i++) {
                                                                                if (typeof prod.variants[i].price == "string") {
                                                                                    prod.variants[i].price = parseFloat(prod.variants[i].price) * 100
                                                                                }
                                                                                if (typeof prod.variants[i].compare_at_price == "string") {
                                                                                    prod.variants[i].compare_at_price = parseFloat(prod.variants[i].compare_at_price) * 100
                                                                                }
                                                                                prod.variants[i].og_compare_at_price = prod.variants[i].compare_at_price;
                                                                                prod.variants[i].og_price = prod.variants[i].price;
                                                                                if (prod.variants[i].compare_at_price && prod.variants[i].compare_at_price > prod.variants[i].price) {
                                                                                    prod.variants[i].compare_at_price = prod.variants[i].compare_at_price
                                                                                } else {
                                                                                    prod.variants[i].compare_at_price = ""
                                                                                }
                                                                                prod.variants[i].price = baDisplayCents(prod.variants[i].price);
                                                                            }
                                                                            if (prod.variants.length == 1) {
                                                                                prod.variantsStyle = "display: none;";
                                                                            }else {
                                                                                var variantsHtml = "";
                                                                                prod.variants.map(function(variant){
                                                                                    if (typeof variant.inventory_management == "string" && variant.inventory_management == "shopify") {
                                                                                        if (typeof variant.inventory_policy == "string" && variant.inventory_policy == "deny" && typeof variant.inventory_quantity == "number" && variant.inventory_quantity <= 0) {
                                                                                            return
                                                                                        }
                                                                                    }
                                                                                    var baImg = "";
                                                                                    if (variant.image_id != null && typeof variant.image_id == "number") {
                                                                                        var varImgs = prod.images.filter(function(pi) {return pi.id == variant.image_id});
                                                                                        if (varImgs.length){baImg = varImgs[0].src}
                                                                                    } else {
                                                                                        if (typeof prod.image == "object" && prod.image && typeof prod.image.src == "string") {
                                                                                            baImg = prod.image.src
                                                                                        }
                                                                                    }

                                                                                    var n = baImg.lastIndexOf(".");
                                                                                    if (n >= 0) {
                                                                                        var s = baImg.substring(0, n) + "_medium." + baImg.substring(n + 1);
                                                                                        baImg = s
                                                                                    }
                                                                                    variantsHtml += "<option value='" + variant.id + "' data-img='" + baImg + "' data-price='" + variant.og_price + "' data-compare-at-price='" + variant.og_compare_at_price + "'>";
                                                                                    variantsHtml += variant.title + "</option>"
                                                                                });
                                                                                prod.variantsSelect = "<select class='lb-variants'>";
                                                                                prod.variantsSelect += variantsHtml + "</select>"
                                                                            }

                                                                            if (typeof prod.image == "object" && prod.image && typeof prod.image.src == "string") {
                                                                                var n = prod.image.src.lastIndexOf(".");
                                                                                if (n >= 0){
                                                                                    var s = prod.image.src.substring(0, n) + "_medium." + prod.image.src.substring(n + 1);
                                                                                    prod.image.src = s
                                                                                }
                                                                            } else {
                                                                                prod.image = {}
                                                                                prod.image.src = 'https://cl.ly/003c0X1M380p/Image%202018-03-05%20at%201.15.15%20PM.png'
                                                                            }
                                                                            products.push(prod)
                                                                        }).fail(function(){
                                                                            console.log('fail');
                                                                        }))
                                                                    });
                                                                    if (arr.length == 0) {
                                                                        return
                                                                    }
                                                                    $.when.apply($, arr).done(function() {
                                                                        if (products.length == 0) {
                                                                            return
                                                                        }
                                                                        var noVariants = true;
                                                                        for (var i = 0; i < products.length; i++) {
                                                                            if (products[i].variants.length > 1) {
                                                                                noVariants = false
                                                                            }
                                                                            products[i].quantity = 1;
                                                                            var curProd = ba_data.ba_bundle.items.filter(function(p) {
                                                                                return p.id == products[i].id
                                                                            });
                                                                            if (curProd.length == 1 && curProd[0].qty > 1) {
                                                                                products[i].quantity = curProd[0].qty
                                                                            }
                                                                        }
                                                                        if (noVariants){
                                                                            for (var i = 0; i < products.length; i++) {
                                                                                products[i].variantsStyle = "height: 0px;"
                                                                            }
                                                                        }
                                                                        switch (products.length) {
                                                                            case 2:
                                                                                var cssLength = 'two'
                                                                                break;
                                                                            case 3:
                                                                                var cssLength = 'three'
                                                                                break;
                                                                            case 4:
                                                                                var cssLength = 'four'
                                                                                break;
                                                                            default:
                                                                                var cssLength = 'two'
                                                                        }

                                                                        var source = $("#lb-bundle").html();
                                                                        var context = {
                                                                            save_text: ba_data.save_text,
                                                                            discount_applies: ba_data.discount_applies,
                                                                            ba_bundle: ba_data.ba_bundle,
                                                                            bundle_note: ba_data.bundle_note,
                                                                            products: products,
                                                                            css_length: cssLength,
                                                                            ba_plus_url: "{{ 'lb-plus.png' | asset_img_url: '38x' }}",
                                                                            ba_eqs_url: "{{ 'lb-eqs.png' | asset_img_url: '38x' }}"
                                                                        };
                                                                        var template = Handlebars.compile(source);
                                                                        var html = template(context);
                                                                        $('div.lb-bundle-wrapper').html(html);
                                                                    })
                                                                }



                                                                function showBaUpsell(ba_data){
                                                                    var arr = [];
                                                                    var products = [];
                                                                    var offerId = ba_data.discounts.upsell_arr[0].offer_id;
                                                                    var upsellNote = ba_data.discounts.upsell_arr[0].upsell_note;
                                                                    var discount_applies = ba_data.discounts.upsell_arr[0].discount_applies;
                                                                    ba_data.discounts.upsell_arr.map(function(p){
                                                                        arr.push($.getJSON("/products/" + p.handle + ".json", function(data) {
                                                                            var prod = data.product;
                                                                            prod.offer_id = offerId;
                                                                            prod.upsell_note = upsellNote;
                                                                            prod.discount_applies = discount_applies;
                                                                            prod.variantsStyle = "";
                                                                            prod.variantsSelect = "";
                                                                            for (var i = 0; i < prod.variants.length; i++) {
                                                                                if (typeof prod.variants[i].price == "string") {
                                                                                    prod.variants[i].price = parseFloat(prod.variants[i].price) * 100
                                                                                }
                                                                                if (typeof prod.variants[i].compare_at_price == "string") {
                                                                                    prod.variants[i].compare_at_price = parseFloat(prod.variants[i].compare_at_price) * 100
                                                                                }
                                                                                prod.variants[i].og_compare_at_price = prod.variants[i].compare_at_price;
                                                                                prod.variants[i].og_price = prod.variants[i].price;
                                                                                if (prod.variants[i].compare_at_price && prod.variants[i].compare_at_price > prod.variants[i].price) {
                                                                                    prod.variants[i].compare_at_price = prod.variants[i].compare_at_price
                                                                                } else {
                                                                                    prod.variants[i].compare_at_price = ""
                                                                                }
                                                                                prod.variants[i].price = baDisplayCents(prod.variants[i].price);
                                                                            }
                                                                            if (prod.variants.length == 1) {
                                                                                prod.variantsStyle = "display: none;";
                                                                            }else {
                                                                                var variantsHtml = "";
                                                                                prod.variants.map(function(variant){
                                                                                    if (typeof variant.inventory_management == "string" && variant.inventory_management == "shopify") {
                                                                                        if (typeof variant.inventory_policy == "string" && variant.inventory_policy == "deny" && typeof variant.inventory_quantity == "number" && variant.inventory_quantity <= 0) {
                                                                                            return
                                                                                        }
                                                                                    }
                                                                                    var baImg = "";
                                                                                    if (variant.image_id != null && typeof variant.image_id == "number") {
                                                                                        var varImgs = prod.images.filter(function(pi) {return pi.id == variant.image_id});
                                                                                        if (varImgs.length){baImg = varImgs[0].src}
                                                                                    } else {
                                                                                        if (typeof prod.image == "object" && prod.image && typeof prod.image.src == "string") {
                                                                                            baImg = prod.image.src
                                                                                        }
                                                                                    }

                                                                                    var n = baImg.lastIndexOf(".");
                                                                                    if (n >= 0) {
                                                                                        var s = baImg.substring(0, n) + "_medium." + baImg.substring(n + 1);
                                                                                        baImg = s
                                                                                    }
                                                                                    variantsHtml += "<option value='" + variant.id + "' data-img='" + baImg + "' data-price='" + variant.og_price + "' data-compare-at-price='" + variant.og_compare_at_price + "'>";
                                                                                    variantsHtml += variant.title + "</option>"
                                                                                });
                                                                                prod.variantsSelect = "<select class='lb-variants'>";
                                                                                prod.variantsSelect += variantsHtml + "</select>"
                                                                            }

                                                                            if (typeof prod.image == "object" && prod.image && typeof prod.image.src == "string") {
                                                                                var n = prod.image.src.lastIndexOf(".");
                                                                                if (n >= 0){
                                                                                    var s = prod.image.src.substring(0, n) + "_medium." + prod.image.src.substring(n + 1);
                                                                                    prod.image.src = s
                                                                                }
                                                                            }
                                                                            products.push(prod)
                                                                        }).fail(function(){
                                                                            console.log('fail');
                                                                        }))
                                                                    });
                                                                    if (arr.length == 0) {
                                                                        return
                                                                    }
                                                                    $.when.apply($, arr).done(function() {
                                                                        if (products.length == 0) {
                                                                            return
                                                                        }
                                                                        var noVariants = true;
                                                                        for (var i = 0; i < products.length; i++) {
                                                                            var offerId = products[i].offer_id;
                                                                            var upsellNote = products[i].upsell_note;
                                                                            var discount_applies = products[i].discount_applies;
                                                                            if (products[i].variants.length > 1) {
                                                                                noVariants = false
                                                                            }
                                                                            products[i].quantity = 1;
                                                                            var curProd = ba_data.discounts.upsell_arr.filter(function(p) {
                                                                                return p.id == products[i].id
                                                                            });
                                                                            if (curProd.length == 1 && curProd[0].qty > 1) {
                                                                                products[i].quantity = curProd[0].qty
                                                                            }
                                                                            if (curProd.length == 1 && curProd[0].qty_left) {
                                                                                products[i].qty_left = curProd[0].qty_left
                                                                            }
                                                                        }
                                                                        if (noVariants){
                                                                            for (var i = 0; i < products.length; i++) {
                                                                                products[i].variantsStyle = "height: 0px;"
                                                                            }
                                                                        }
                                                                        switch (products.length) {
                                                                            case 1:
                                                                                var cssLength = 'one'
                                                                                break;
                                                                            case 2:
                                                                                var cssLength = 'two'
                                                                                break;
                                                                            case 3:
                                                                                var cssLength = 'three'
                                                                                break;
                                                                            case 4:
                                                                                var cssLength = 'four'
                                                                                break;
                                                                            default:
                                                                                var cssLength = 'two'
                                                                        }


                                                                        var source = $("#lb-upsell").html();
                                                                        var multiple_products = products.length > 1;
                                                                        var context = {
                                                                            multiple_products: multiple_products,
                                                                            discount_applies: discount_applies,
                                                                            upsell_note: upsellNote,
                                                                            discount_applies: discount_applies,
                                                                            products: products,
                                                                            css_length: cssLength,
                                                                            ba_plus_url: "{{ 'lb-plus.png' | asset_img_url: '38x' }}",
                                                                            offer_id: offerId
                                                                        };
                                                                        var template = Handlebars.compile(source);
                                                                        var html = template(context);
                                                                        $('body').append(html);
                                                                        $('#dpModal-container').dpModal();
                                                                    });
                                                                }

                                                                function showVolDiscounts(data){
                                                                    var source = $("#lb-discount-tiers").html();
                                                                    var context = {
                                                                        product_message: "Buy at discounted prices",
                                                                        vol_rows: data.vol_rows
                                                                    };
                                                                    var template = Handlebars.compile(source);
                                                                    var html = template(context);
                                                                    $('div.lb-vol-wrapper').html(html);

                                                                }

                                                                function baDelegate(data) {
                                                                    if(data.type == 'vd'){

                                                                        var cartForm = $("form[action*='/cart/add']").first();
                                                                        if($('.lb-vol-wrapper').length == 0){
                                                                            cartForm.after("<div class='lb-vol-wrapper'></div>");
                                                                        }
                                                                        showVolDiscounts(data)

                                                                    }

                                                                    if(data.type == 'bundle'){
                                                                        var cartForm = $("form[action*='/cart/add']").first();
                                                                        if($('.lb-bundle-wrapper').length == 0){
                                                                            cartForm.after("<div class='lb-bundle-wrapper'></div>");
                                                                        }
                                                                        showLionBundle(data)
                                                                    }

                                                                    if (typeof data.discounts == "object" && typeof data.discounts.upsell_arr == "object"){
                                                                        if(data.discounts.upsell_arr.length > 0){
                                                                            showBaUpsell(data)
                                                                        }
                                                                    }

                                                                    if (typeof data.discounts == "object" && typeof data.notifications == "object"){
                                                                        if(data.notifications.length > 0 && data.discounts.upsell_arr.length == 0){
                                                                            showLionNotification(data.notifications)
                                                                        }
                                                                    }


                                                                    if (typeof data.discounts == "object" && typeof data.discounts.cart == "object" && typeof data.discounts.cart.items == "object") {
                                                                        showCartDiscounts(data.discounts)
                                                                    }
                                                                    reloadCurrency();
                                                                }

                                                                function showCartDiscounts(discounts) {
                                                                    window.lion.discounts = discounts;

                                                                    for (var i = 0; i < discounts.cart.items.length; i++) {
                                                                        var item = discounts.cart.items[i];

                                                                        if (item.discounted_price < item.original_price) {
                                                                            $(".lion-cart-item-price[data-key='" + item.key + "']").html("<span class='original_price'>" + item.original_price_format + "</span>" + "<span class='discounted_price'>" + item.discounted_price_format + "</span>");
                                                                            $(".lion-cart-item-line-price[data-key='" + item.key + "']").html("<span class='original_price'>" + item.original_line_price_format + "</span>" + "<span class='discounted_price'>" + item.discounted_line_price_format + "</span>")
                                                                        }

                                                                        $(".lion-cart-item-upsell-notes[data-key='" + item.key + "']").html(item.upsell_note);
                                                                        $(".lion-cart-item-success-notes[data-key='" + item.key + "']").html(item.success_note);
                                                                    };

                                                                    if (typeof discounts.discounted_price_html != "string") {
                                                                        return
                                                                    }

                                                                    $("span#bk-cart-subtotal-price").attr( "id", "");

                                                                    $(".lion-original-cart-total").css("text-decoration", "line-through");
                                                                    $(".lion-cart-total").html("<span class=''>" + discounts.discounted_price_html + "</span>");

                                                                    $('.subtotal .cart_savings.sale').hide();



                                                                    $(".lion-cart-total").prepend("<span class='lion-messages'><div id='lion-discount-item'></div><div id='lion-summary-item'></div></span>");


                                                                    if(discounts.summary_item_html){
                                                                        $('#lion-summary-item').html(discounts.summary_item_html);
                                                                    }

                                                                    var checkout_selectors = ["input[name='checkout']", "button[name='checkout']", "[href$='checkout']", "input[name='goto_pp']", "button[name='goto_pp']", "input[name='goto_gc']", "button[name='goto_gc']", ".additional-checkout-button", ".google-wallet-button-holder", ".amazon-payments-pay-button"];
                                                                    checkout_selectors.forEach(function(selector) {
                                                                        var els = document.querySelectorAll(selector);
                                                                        if (typeof els == "object" && els) {
                                                                            for (var i = 0; i < els.length; i++) {
                                                                                var el = els[i];
                                                                                if (typeof el.addEventListener != "function") {
                                                                                    return
                                                                                }
                                                                                el.addEventListener("click", function(ev) {
                                                                                    ev.preventDefault();
                                                                                    DiscountedPricingCheckout();
                                                                                }, false);

                                                                            }
                                                                        }
                                                                    })
                                                                }

                                                                function showLionNotification(notifications){
                                                                    if ((getCookie('lion_notifications_closed') != 1) && notifications != ""){
                                                                        var barElements = ["main", "div.content", "section.main-content"];
                                                                        var barHtml = "<div id='lion-notification-bar'>" + notifications + "<div id='lion-close-notification'>X</div></div>";
                                                                        barElements.forEach(function(el){
                                                                            if ($('#lion-notification-bar').length == 0){
                                                                                if (el == 'main'){
                                                                                    $(el).prepend(barHtml);
                                                                                } else {
                                                                                    $(el).before(barHtml);
                                                                                }
                                                                            }
                                                                        });
                                                                        if ($('#lion-notification-bar').length > 0){
                                                                            $('#lion-notification-bar').slideDown('slow');
                                                                        }
                                                                    }

                                                                }

                                                                function addLionBundle(e){
                                                                    e.preventDefault();
                                                                    var lionBundleItems = [];

                                                                    $(".lb-product-wrapper").each(function() {
                                                                        var i = {id: $(this).data("variant-id"),quantity: $(this).data("quantity")};
                                                                        lionBundleItems.push(i);
                                                                    });

                                                                    addLionItems(lionBundleItems, function(){
                                                                        setTimeout(function() {window.location.href = "/cart"}, 200)
                                                                    });

                                                                }

                                                                function addLionUpsells(target){
                                                                    var btn = $(target);
                                                                    var lionUpsellItems = [];

                                                                    var container = btn.parents(".product-container");
                                                                    var i = {id: container.data("variant-id"),quantity: container.data("quantity")};
                                                                    lionUpsellItems.push(i);


                                                                    addLionItems(lionUpsellItems, function(){
                                                                        setTimeout(function() {window.location.href = "/cart"}, 200)
                                                                    });
                                                                }


                                                                function addLionItems(variants, callback) {
                                                                    if (variants.length) {
                                                                        var i = variants.shift();
                                                                        $.ajax({
                                                                            url: "/cart/add.js",
                                                                            type: "POST",
                                                                            dataType: "json",
                                                                            data: i,
                                                                            success: function(data) {
                                                                                addLionItems(variants, callback)
                                                                            },
                                                                            error: function(data) {
                                                                                if (typeof data == "object" && typeof data.responseJSON == "object" && typeof data.responseJSON.description == "string") {
                                                                                    alert(data.responseJSON.description)
                                                                                }
                                                                                if (typeof res == "string") {
                                                                                    alert(data)
                                                                                }
                                                                            }
                                                                        })
                                                                    } else {
                                                                        if (typeof callback == "function") {
                                                                            return callback()
                                                                        }
                                                                        setTimeout(function() {
                                                                            window.location.reload()
                                                                        }, 100)
                                                                    }
                                                                }


                                                                function DiscountedPricingCheckout(){
                                                                    if ($("input[type='checkbox']#agree").length > 0 && $("input[type='checkbox']#agree:checked").length != jQuery("input[type='checkbox']#agree").length) {
                                                                        return
                                                                    }

                                                                    for (var i = 0; i < window.lion.cart.items.length; i++) {
                                                                        var item = window.lion.cart.items[i];
                                                                        var el = document.querySelectorAll("[id='updates_" + item.key + "']");
                                                                        if (el.length != 1) {
                                                                            el = document.querySelectorAll("[id='updates_" + item.variant_id + "']")
                                                                        }
                                                                        if (el.length == 1) {
                                                                            window.lion.cart.items[i].quantity = el[0].value
                                                                        }
                                                                    }

                                                                    window.lion.action_type = 'checkout';

                                                                    var invoiceUrlParams = [];

                                                                    var noteAttributes = [];
                                                                    $("[name^='attributes']").each(function() {
                                                                        var attribute = $(this);
                                                                        var name = $(this).attr("name");
                                                                        name = name.replace(/^attributes\[/i, "").replace(/\]$/i, "");
                                                                        var v = {
                                                                            name: name,
                                                                            value: attribute.val()
                                                                        };
                                                                        if (v.value == "") {
                                                                            return
                                                                        }
                                                                        switch (attribute[0].tagName.toLowerCase()) {
                                                                            case "input":
                                                                                if (attribute.attr("type") == "checkbox") {
                                                                                    if (attribute.is(":checked")) {
                                                                                        noteAttributes.push(v)
                                                                                    }
                                                                                } else {
                                                                                    noteAttributes.push(v)
                                                                                }
                                                                                break;
                                                                            default:
                                                                                noteAttributes.push(v)
                                                                        }
                                                                    });

                                                                    var orderNote = "";
                                                                    if ($("[name='note']").length) {
                                                                        orderNote = $("[name='note']")[0].value
                                                                    }
                                                                    window.lion.cart.note_attributes = noteAttributes;
                                                                    window.lion.cart.note = orderNote;

                                                                    if (orderNote.length){
                                                                        invoiceUrlParams.push("note=" + encodeURIComponent(orderNote))
                                                                    }

                                                                    if (noteAttributes.length){
                                                                        noteAttributes.map(function(a) {
                                                                            invoiceUrlParams.push("attributes" + encodeURIComponent("[" + a.name + "]") + "=" + encodeURIComponent(a.value))
                                                                        })
                                                                    }

                                                                    if(window.lion.discount_method == 'code'){
                                                                        $.ajax({
                                                                            cache: false,
                                                                            contentType: "application/json; charset=utf-8",
                                                                            dataType: "json",
                                                                            type: "POST",

                                                                            url: "/apps/lion_pricing",

                                                                            data: JSON.stringify(window.lion),
                                                                            success: function(res) {
                                                                                res.invoice_url = '/checkout';
                                                                                if (res.discount_code) {
                                                                                    invoiceUrlParams.push("discount=" + res.discount_code);
                                                                                }
                                                                                if (invoiceUrlParams.length) {
                                                                                    res.invoice_url += "?" + invoiceUrlParams.join("&")
                                                                                }
                                                                                window.location.href = res.invoice_url
                                                                            }
                                                                        })


                                                                    } else {

                                                                        if (typeof window.gaclientId) {
                                                                            invoiceUrlParams.push("clientId=" + window.gaclientId);
                                                                            invoiceUrlParams.push("_ga=" + window.gaclientId)
                                                                        }

                                                                        $.ajax({
                                                                            cache: false,
                                                                            contentType: "application/json; charset=utf-8",
                                                                            dataType: "json",
                                                                            type: "POST",

                                                                            url: "/apps/lion_pricing",

                                                                            data: JSON.stringify(window.lion),
                                                                            success: function(res) {
                                                                                if(res.invoice_url){
                                                                                    if (invoiceUrlParams.length) {
                                                                                        res.invoice_url += "?" + invoiceUrlParams.join("&")
                                                                                    }
                                                                                    setTimeout(function(){
                                                                                        window.location.href = res.invoice_url
                                                                                    }, 500);
                                                                                } else {
                                                                                    window.location.href = "/checkout"
                                                                                }
                                                                            }
                                                                        })

                                                                    }


                                                                }


                                                                if(window.lion.ba_page.length > 0){
                                                                    $.ajax({
                                                                        cache: false,
                                                                        type: "POST",

                                                                        url: "/apps/lion_pricing",

                                                                        data: JSON.stringify(window.lion),
                                                                        dataType: 'json',
                                                                        contentType: "application/json; charset=utf-8",
                                                                        success: function(data) {
                                                                            baDelegate(data)
                                                                        }
                                                                    });
                                                                }

                                                                setTimeout(function(){
                                                                    if (typeof ga == "function"){ga(function(tracker) {window.gaclientId = tracker.get("clientId")})}
                                                                }, 1000);

                                                                $(document).on('change', "input.lion-quantity, input[name='updates[]'], input[id^='updates_'], input[id^='Updates_']", function(e) {
                                                                    e.preventDefault();
                                                                    $("form[action$='cart']").submit();
                                                                });

                                                                if(window.lion.ba_page == 'cart'){
                                                                    $(document).ajaxSuccess(function(evt,xhr,attrs) {
                                                                        if (attrs.url == '/cart/change.js'){
                                                                            $("form[action$='cart']").submit();
                                                                        }
                                                                    });
                                                                }

                                                                $(document).on('change', 'select.lb-variants', function(){
                                                                    var option = $(this).find(":selected");
                                                                    var price  = baDisplayCents(option.data('price'));
                                                                    var cont = option.parents('.lb-product-wrapper');
                                                                    var img  = option.data('img');

                                                                    cont.data("variant-id", option.val());
                                                                    cont.find('.lb-price span.lb-sale').html(price);
                                                                    cont.find('img').attr('src', img);
                                                                    reloadCurrency();
                                                                    return false;
                                                                });

                                                                $(document).on('change', '#dpModal-container select.lb-variants', function(){
                                                                    var option = $(this).find(":selected");
                                                                    var price  = baDisplayCents(option.data('price'));
                                                                    var cont = option.parents('.product-container');
                                                                    var img  = option.data('img');

                                                                    cont.data("variant-id", option.val());
                                                                    cont.find('span.lb-sale').html(price);
                                                                    cont.find('img').attr('src', img);
                                                                    reloadCurrency();
                                                                    return false;
                                                                });


                                                                $(document).on('click', ".add-lion-bundle", function(e) {
                                                                    e.preventDefault();
                                                                    addLionBundle(e);
                                                                });

                                                                $(document).on('click', ".add-upsells", function(e) {
                                                                    e.preventDefault();
                                                                    addLionUpsells(e.target);
                                                                });

                                                                $(document).on('click', '.no-thanks a', function(){
                                                                    var link = $(this);
                                                                    var offerId = link.data('offer-id')
                                                                    var currentSkipIds = getCookie('lb-skip-ids');
                                                                    var allSkipIds = currentSkipIds + ',' + offerId;
                                                                    setCookie('lb-skip-ids', allSkipIds);
                                                                    $.dpModal.close()
                                                                    return false;
                                                                });

                                                                $(document).on('click', "button#apply-lion-discount", function(e) {
                                                                    e.preventDefault();
                                                                    setCookie('lion_discount_' + window.lion.cart.token, $('#lion-discount-code').val().trim())
                                                                    window.location.reload();
                                                                });

                                                                $(document).on('click', "div#lion-close-notification", function(e) {
                                                                    e.preventDefault();
                                                                    //var closedNotifications = getCookie('lion_notifications_closed')
                                                                    setCookie('lion_notifications_closed', 1, 0.01);
                                                                    $('#lion-notification-bar').slideUp('slow');
                                                                });


                                                            })
                                                        }(window, document));
                                                </script>


                                                    $(document).on('click', ".add-lion-bundle", function(e) {
                                    e.preventDefault();
                                    addLionBundle(e);
                                });

                                $(document).on('click', ".add-upsells", function(e) {
                                    e.preventDefault();
                                    addLionUpsells(e.target);
                                });

                                $(document).on('click', '.no-thanks a', function(){
                                    var link = $(this);
                                    var offerId = link.data('offer-id')
                                    var currentSkipIds = getCookie('lb-skip-ids');
                                    var allSkipIds = currentSkipIds + ',' + offerId;
                                    setCookie('lb-skip-ids', allSkipIds);
                                    $.dpModal.close()
                                    return false;
                                });

                                $(document).on('click', "button#apply-lion-discount", function(e) {
                                    e.preventDefault();
                                    setCookie('lion_discount_' + window.lion.cart.token, $('#lion-discount-code').val().trim())
                                    window.location.reload();
                                });

                                $(document).on('click', "div#lion-close-notification", function(e) {
                                    e.preventDefault();
                                    //var closedNotifications = getCookie('lion_notifications_closed')
                                    setCookie('lion_notifications_closed', 1, 0.01);
                                    $('#lion-notification-bar').slideUp('slow');
                                });


                            })
                        }(window, document));
                </script>

