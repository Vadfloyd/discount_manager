
<style type="text/css">

.bazingo-cart-item-line-price .original_price {
    display: block;
    text-decoration: line-through;
}
.bazingo-cart-item-price, .bazingo-cart-total, .bazingo-cart-item-line-price .discounted_price {
    display: block;
    font-weight: bold;
}

.bazingo-cart-item-success-notes, .bazingo-cart-item-upsell-notes {
    display: block;
    font-weight:bold;
    color: #0078bd;
    font-size: 100%;
    a {
        color: #0078bd;
    }
}
.bazingo-cart-total {
    display: block;
    font-weight: bold;
}
.bazingo-messages{
    display:block;
}

  #bazingo-discount-item{
    font-size:70%;
    padding-top: 5px;
    padding-bottom: 5px;
}

  #bazingo-summary-item{
    font-size:70%;
    padding-top: 5px;
    padding-bottom: 5px;
}

.summary-line-note{
    padding-right: 10px;
}

.summary-line-discount{
    color: #0078bd;
}

input#bazingo-discount-code{
    max-width:200px;
    display:inline-block;
}

button#apply-bazingo-discount{
    display:inline-block;
    max-width:200px;
}

.product-price .lb-sale{
    display:block;
    width:100%;
    font-weight:normal;
}

.discount-applies-true .lb-price .lb-sale{
    text-decoration: line-through;
    width:100%;
}

.discount-applies-true .product-price .lb-sale{
    display:block;
    width:100%;
    color:#8C0000;
    font-weight:normal;
    opacity:0.8;
}


.discount-applies-true .product-price .upsell-note{
    display:block;
    font-weight:normal;
    width:100%;
}

.discount-applies-false .product-price .upsell-note{
    display:none;
}


}


</style>


<script type="text/javascript">
window.bazingo = {};

{% if customer %}
window.bazingo.customer_tags = "{{customer.tags | join: ','}}";
{% endif %}

{% if template contains 'product' %}
{% unless product.id == blank %}
{% assign ba_page = 'product' %}
window.bazingo.product = {
    id: {{product.id}},
price: {{product.price}},
};
window.bazingo.product_collections = []
{% for c in product.collections %}
window.bazingo.product_collections.push({{c.id}})
{% endfor %}
{% endunless %}
{% endif %}


{% if template contains 'cart' %}
{% assign ba_page = 'cart' %}

{% assign ba_currency = shop.money_format %}
{% endif %}

{% if cart %}
window.bazingo.cart = {{cart | json}}


window.bazingo.shop_domain='{{ shop.domain }}';
window.bazingo.cart.items = []

{% for item in cart.items %}
var bazingo_item = {{item | json}};
bazingo_item["collection_ids"] = [];
{% for c in item.product.collections %}
bazingo_item["collection_ids"].push({{c.id}})
{% endfor %}
window.bazingo.cart.items.push(bazingo_item);
{% endfor %}

if (typeof window.bazingo.cart.items == "object") {
    for (var i=0; i<window.bazingo.cart.items.length; i++) {
        ["sku", "grams", "vendor", "url", "image", "handle", "requires_shipping", "product_type", "product_description"].map(function(a) {
            delete window.bazingo.cart.items[i][a]
        })
    }
}
{% endif %}
window.bazingo.ba_page = "{{ba_page}}";
window.bazingo.money_format = "{{ ba_currency }}";
window.bazingo.discount_method = "code";


{% raw %}


{% endraw %}





</script>



<script type="text/javascript">

    (function(window, document) {"use strict";

        function setCookie(cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays*24*60*60*1000));
            var expires = "expires="+ d.toUTCString();
            document.cookie = cname + "=" + cvalue + "; " + expires + "; path=/;";
        }

        function getCookie(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for(var i = 0; i <ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0)==' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length,c.length);
                }
            }
            return "";
        }

        function reqJquery(onload) {
            if(typeof jQuery === 'undefined' || (parseInt(jQuery.fn.jquery) === 1 && parseFloat(jQuery.fn.jquery.replace(/^1\./,'')) < 10)){
                var head = document.getElementsByTagName('head')[0];
                var script = document.createElement('script');
                script.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js';;
                script.type = 'text/javascript';
                script.onload = script.onreadystatechange = function() {
                    if (script.readyState) {
                        if (script.readyState === 'complete' || script.readyState === 'loaded') {
                            script.onreadystatechange = null;
                            onload(jQuery.noConflict(true));
                        }
                    }
                    else {
                        onload(jQuery.noConflict(true));
                    }
                };
                head.appendChild(script);
            }else {
                onload(jQuery);
            }
        }

        reqJquery(function($){



            function bazingoreloadCurrency() {

                if (typeof Currency == "object" && typeof Currency.moneyFormats == "object" && typeof mlvedaload == "function") {
                    mlvedaload()
                }
            }

            var bazingoCookieCode = getCookie('bazingo_discount_' + window.bazingo.cart.token);
            if(bazingoCookieCode){
                window.bazingo.discount_code = bazingoCookieCode;
            }

            function bazingoDisplayCents(cents) {
                if (typeof cents == "undefined" || cents == null){return ""}
                if (typeof cents == "string" && cents.length == 0){return ""}

                var format = window.bazingo.money_format;
                var moneyRegex = /\{\{\s*(\w+)\s*\}\}/;
                if (typeof cents == "string") {
                    cents = cents.replace(".", "")
                }

                function defOpt(opt,def){return typeof opt == "undefined" ? def : opt}

                function displayDelims(n,p,t,d){
                    p = defOpt(p, 2);
                    t = defOpt(t, ",");
                    d = defOpt(d, ".");
                    if (isNaN(n) || n == null){
                        return 0
                    }
                    n = (n / 100).toFixed(p);
                    var parts = n.split("."),dollars = parts[0].replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1" + t),cents = parts[1] ? d + parts[1] : "";
                    return dollars + cents
                }

                var val = "";
                switch (format.match(moneyRegex)[1]) {
                    case "amount":
                        val = displayDelims(cents, 2);
                        break;
                    case "amount_no_decimals":
                        val = displayDelims(cents, 0);
                        break;
                    case "amount_no_decimals_with_comma_separator":
                        val = displayDelims(cents, 0, ".", ",");
                        break
                    case "amount_with_comma_separator":
                        val = displayDelims(cents, 2, ".", ",");
                        break;
                }
                return format.replace(moneyRegex, val)
            }







            function baDelegate(data) {

                var response=data;
                if(data.type == 'product_page_discount'){

                    var cartForm = $("form[action*='/cart/add']").first();


                    var html='<p style="color: '+ response.color_schema.offer_text_color +'">'+  ( response.color_schema.product_page_msg !==' ' ? response.color_schema.product_page_msg:"")+'</p></br>';
                    if(response.discount_type=='amount') {
                        html = html + '<table style="background-color: '+ response.color_schema.offer_bg_color +'"><tr><th>Minimum Amount</th><th>Discount</th></tr>';
                    }else{
                        html = html + '<table style="background-color: '+ response.color_schema.offer_bg_color +'"><tr><th>Minimum Qty</th><th>Discount</th></tr>';
                    }

                    for (var i = 0; i < response.discounts.length; i++) {

                        html = html + '<tr style="color: '+ response.color_schema.offer_text_color +'"><th>'+response.discounts[i]['amount_or_quantity']+'</th><th>'+response.discounts[i].discount+ '</th></tr>';



                    }

                    $(html).insertAfter(cartForm);


                }



                if (typeof data.discounts == "object" && typeof data.discounts.upsell_arr == "object"){
                    if(data.discounts.upsell_arr.length > 0){
                        showBaUpsell(data)
                    }
                }



                if (typeof data.discounts == "object" && typeof data.discounts.cart == "object" && typeof data.discounts.cart.items == "object") {


                    bazingoCartDiscounts(data.discounts);

                }
                bazingoreloadCurrency();
            }

            function bazingoCartDiscounts(discounts) {

                window.bazingo.discounts = discounts;

                for (var i = 0; i < discounts.cart.items.length; i++) {
                    var item = discounts.cart.items[i];

                    if (item.discounted_price < item.original_price) {
                        $(".bazingo-cart-item-price[data-key='" + item.key + "']").html("<span class='original_price'>" + item.original_price_format + "</span>" + "<span class='discounted_price'>" + item.discounted_price_format + "</span>");
                        $(".bazingo-cart-item-line-price[data-key='" + item.key + "']").html("<span class='original_price'>" + item.original_line_price_format + "</span>" + "<span class='discounted_price'>" + item.discounted_line_price_format + "</span>")
                    }

                    $(".bazingo-cart-item-upsell-notes[data-key='" + item.key + "']").html(item.upsell_note).attr('upsell_quantity',item.upsell_quantity).css('color',item.msg_color);
                    $(".bazingo-cart-item-success-notes[data-key='" + item.key + "']").html(item.success_note).css('color',item.msg_color);

                };

                if (typeof discounts.discounted_price_html != "string") {
                    return
                }

                $("span#bk-cart-subtotal-price").attr( "id", "");

                if(discounts.original_total_price > discounts.discounted_price){
                    $(".bazingo-original-cart-total").css("text-decoration", "line-through");
                    $(".bazingo-cart-total").html("<span class=''>" + discounts.discounted_price_html + "</span>");
                }


                $('.subtotal .cart_savings.sale').hide();



                $(".bazingo-cart-total").prepend("<span class='bazingo-messages'><div id='bazingo-discount-item'></div><div id='bazingo-summary-item'></div></span>");


                if(discounts.summary_item_html){
                    $('#bazingo-summary-item').html(discounts.summary_item_html);
                }

                var checkout_selectors = ["input[name='checkout']", "button[name='checkout']", "[href$='checkout']", "input[name='goto_pp']", "button[name='goto_pp']", "input[name='goto_gc']", "button[name='goto_gc']", ".additional-checkout-button", ".google-wallet-button-holder", ".amazon-payments-pay-button"];
                checkout_selectors.forEach(function(selector) {
                    var els = document.querySelectorAll(selector);
                    if (typeof els == "object" && els) {
                        for (var i = 0; i < els.length; i++) {
                            var el = els[i];
                            if (typeof el.addEventListener != "function") {
                                return
                            }
                            el.addEventListener("click", function(ev) {
                                ev.preventDefault();
                                BazingoDiscountedPricingCheckout();
                            }, false);

                        }
                    }
                })
            }


            function BazingoDiscountedPricingCheckout(){

                if ($("input[type='checkbox']#agree").length > 0 && $("input[type='checkbox']#agree:checked").length != jQuery("input[type='checkbox']#agree").length) {
                    return
                }

                for (var i = 0; i < window.bazingo.cart.items.length; i++) {
                    var item = window.bazingo.cart.items[i];
                    var el = document.querySelectorAll("[id='updates_" + item.key + "']");
                    if (el.length != 1) {
                        el = document.querySelectorAll("[id='updates_" + item.variant_id + "']")
                    }
                    if (el.length == 1) {
                        window.bazingo.cart.items[i].quantity = el[0].value
                    }
                }

                window.bazingo.action_type = 'checkout';

                var invoiceUrlParams = [];


                if(window.bazingo.discount_method == 'code'){


                    $.ajax({
                        cache: false,

                        dataType: "json",
                        type: "POST",

                        url: "https://3b069742.ngrok.io/vadim/public/create_discount_code",

                        data: window.bazingo,
                        success: function(res) {

                            res.invoice_url = '/checkout';
                            if (res.discount_code) {
                                invoiceUrlParams.push("discount=" + res.discount_code);
                            }
                            if (invoiceUrlParams.length) {
                                res.invoice_url += "?" + invoiceUrlParams.join("&")
                            }

                            window.location.href = res.invoice_url
                        }
                    })
                } else {
                    $.ajax({
                        cache: false,
                        dataType: "json",
                        type: "POST",
                        url: "https://3b069742.ngrok.io/vadim/public/create_discount_code",

                        data: window.bazingo,
                        success: function(res) {
                            if(res.invoice_url){
                                if (invoiceUrlParams.length) {
                                    res.invoice_url += "?" + invoiceUrlParams.join("&")
                                }
                                setTimeout(function(){
                                    window.location.href = res.invoice_url
                                }, 500);
                            } else {
                                window.location.href = "/checkout"
                            }
                        }
                    })

                }
            }
            $('a').click(function(event) {
                event.preventDefault();
                var target = event.target || event.srcElement;
                target =  target.innerHTML;
                if( target.indexOf('<span class="bazingo-cart-item-upsell-notes"') >= 0){
                    setInterval(function(){
                        if(typeof event.target.href !=='undefined'){
                            window.location.href=event.target.href;
                        }

                    },2000);

                }else{
                    if(typeof event.target.href !=='undefined'){
                        window.location.href=event.target.href;
                    }

                }

            });
            if(window.bazingo.ba_page.length > 0){

                $.ajax({

                    type: "POST",

                    url: "https://3b069742.ngrok.io/vadim/public/create_discount_code",

                    data:window.bazingo,
                    dataType: 'JSON',

                    success: function(data) {

                        baDelegate(data)
                    },
                    fail:function(data) {
                        console.log(data);return;

                    }
                });
            }

            setTimeout(function(){
                if (typeof ga == "function"){ga(function(tracker) {window.gaclientId = tracker.get("clientId")})}
            }, 1000);

            $(document).on('change', "input.bazingo-quantity, input[name='updates[]'], input[id^='updates_'], input[id^='Updates_']", function(e) {
                e.preventDefault();
                $("form[action$='cart']").submit();
            });




            $(document).on('click','.bazingo-cart-item-upsell-notes',function(){

                var bazingo_upsell_quantity=$(this).attr('upsell_quantity');
                if(bazingo_upsell_quantity !=null && bazingo_upsell_quantity!=0){
                    var  data_key_line_item=$(this).attr('data-key');

                    data_key_line_item = data_key_line_item.substring(0, data_key_line_item.indexOf(':'));

                    $.post(
                        '/cart/change.js',{quantity: bazingo_upsell_quantity,id:  data_key_line_item},
                        null,
                        "json"
                    ).fail(function(xhr, status, error) {

                    }).done(function(data,  error) {

                        location.reload();
                    });

                }
            });



            if(window.bazingo.ba_page == 'cart'){
                $(document).ajaxSuccess(function(evt,xhr,attrs) {
                    if (attrs.url == '/cart/change.js'){
                        //  window.setInterval(function(){
                        // $("form[action$='cart']").submit(); },5000);

                    }
                });
            }

            $(document).on('change', 'select.lb-variants', function(){
                var option = $(this).find(":selected");
                var price  = bazingoDisplayCents(option.data('price'));
                var cont = option.parents('.lb-product-wrapper');
                var img  = option.data('img');

                cont.data("variant-id", option.val());
                cont.find('.lb-price span.lb-sale').html(price);
                cont.find('img').attr('src', img);
                bazingoreloadCurrency();
                return false;
            });
            $(document).on('click', '.no-thanks a', function(){
                var link = $(this);
                var offerId = link.data('offer-id')
                var currentSkipIds = getCookie('bazingo-skip-ids');
                var allSkipIds = currentSkipIds + ',' + offerId;
                setCookie('bazingo-skip-ids', allSkipIds);
                $.dpModal.close()
                return false;
            });

            $(document).on('click', "button#apply-bazingo-discount", function(e) {
                e.preventDefault();
                setCookie('bazingo_discount_' + window.bazingo.cart.token, $('#bazingo-discount-code').val().trim())
                window.location.reload();
            });

        })
    }(window, document));
</script>

